# [Le Biniou](https://biniou.net)

[![pipeline status](https://gitlab.com/lebiniou/lebiniou/badges/master/pipeline.svg)](https://gitlab.com/lebiniou/lebiniou/commits/master)

# Pre-built packages

Packages exist for various GNU/Linux distributions:

 * [Debian](https://tracker.debian.org/pkg/lebiniou), [Ubuntu](https://packages.ubuntu.com/search?keywords=lebiniou&searchon=names&suite=all&section=all)

# Building from sources

## GNU/Linux-based systems

  1. Install required dependencies

### Debian-based distributions

  ```sh
  sudo apt-get -qq update
  sudo apt-get -qq install autoconf pkg-config gcc make libglib2.0-dev libfftw3-dev libfreetype6-dev libswscale-dev libsdl2-ttf-dev libcaca-dev libjack-dev libsndfile1-dev libmagickwand-dev libjansson-dev libulfius-dev
  ```

  2. Configure, compile and install

  The configure script has several [build options](BUILD.md).

  ```sh
  autoreconf -fi
  ./configure
  make
  sudo make install
  ```

  3. Follow the same steps for [lebiniou-data](https://gitlab.com/lebiniou/lebiniou-data) package, then

  4. Run

  ```sh
  lebiniou
  ```

  5. Get more options
  ```sh
  lebiniou --help
  man lebiniou
  ```

## Arch Linux

Use the PKGBUILDs from the AUR [lebiniou](https://aur.archlinux.org/packages/lebiniou)/[lebiniou-data](https://aur.archlinux.org/packages/lebiniou-data/) or build it manually:

Fetch dependencies and build tools:
```sh
pacman --needed -S base-devel fftw libmagick6 ffmpeg sdl2_ttf libcaca
```

Build and install lebiniou-data:
```sh 
autoreconf -i
./configure
make
sudo make install
```

Build and install lebiniou:
```sh
autoreconf -i
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/lib/imagemagick6/pkgconfig"
./configure
make
sudo make install
```

## BSD-based systems

1. Fetch dependencies

* FreeBSD (12.0)
  ```sh
  pkg install autoconf automake pkgconf glib fftw3 ffmpeg sdl2_ttf libcaca jackit ImageMagick
  ```

* NetBSD (8.0)
  ```sh
  pkg_add autoconf automake pkg-config glib2 fftw ffmpeg4 SDL2_ttf libcaca jack ImageMagick
  ```

* OpenBSD (6.5)
  ```sh
  pkg_add gcc-8.3.0 glib2 fftw3 ffmpeg sdl2-ttf libcaca jack ImageMagick
  ```

2. Configure, compile and install
  ```sh
  autoreconf -fi
  ./configure
  # for OpenBSD
  CC=/usr/local/bin/egcc ./configure
  make
  make install
  ```

# Running a local build

Make sure you have the data files at least downloaded !

* if they are installed
```sh
LD_LIBRARY_PATH=$PWD/src ./src/lebiniou -b plugins
```
* else
```sh
LD_LIBRARY_PATH=$PWD/src ./src/lebiniou -b plugins -d /path/to/datafiles
```

# Support

Join us on [IRC](irc://freenode/biniou) for general questions.

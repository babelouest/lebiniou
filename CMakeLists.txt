#
# Lebiniou
#
# CMake file used to build program
#

cmake_minimum_required(VERSION 3.5)

project(biniou C)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Werror")
set(LIBS "-rdynamic -Wl,-z,now -lm -ldl")

# library info

set(VERSION_MAJOR "3")
set(VERSION_MINOR "50")
set(VERSION_PATCH "1")
set(LIBRARY_VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
set(LIBRARY_SOVERSION "${VERSION_MAJOR}.${VERSION_MINOR}")

set(PROJECT_NAME "lebiniou")
set(PROJECT_DESCRIPTION "A thingy to make stuff")
set(PROJECT_BUGREPORT_PATH "https://gitlab.com/lebiniou/lebiniou/-/issues")
set(ORCANIA_VERSION_REQUIRED "2.1.1")
set(YDER_VERSION_REQUIRED "1.4.11")
set(ULFIUS_VERSION_REQUIRED "2.6.9")
set(JANSSON_VERSION_REQUIRED "2.11")

set(SRC_PATH "${CMAKE_CURRENT_SOURCE_DIR}/src/")
include_directories(${SRC_PATH})

include(GNUInstallDirs)
include(CheckSymbolExists)

# cmake modules

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake-modules)

include(FindJansson)
find_package(Jansson ${JANSSON_VERSION_REQUIRED} REQUIRED)
if (JANSSON_FOUND)
	set(LIBS ${LIBS} ${JANSSON_LIBRARIES})
	include_directories(${JANSSON_INCLUDE_DIRS})
endif ()

include(FindGLIB2)
find_package(GLIB2 REQUIRED)
if (GLIB2_FOUND)
	set(LIBS ${LIBS} ${GLIB2_LIBRARIES})
	include_directories(${GLIB2_INCLUDE_DIR})
endif ()

include(FindImageMagick)
find_package(ImageMagick COMPONENTS MagickWand REQUIRED)
if (ImageMagick_FOUND)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fopenmp -DMAGICKCORE_HDRI_ENABLE=0 -DMAGICKCORE_QUANTUM_DEPTH=16") # TODO: use "pkg-config --cflags-only-other ImageMagick" output if possible
	set(LIBS ${LIBS} ${ImageMagick_LIBRARIES})
	include_directories(${ImageMagick_INCLUDE_DIRS} ${ImageMagick_MagickWand_INCLUDE_DIRS})
endif ()

include(FindZLIB)
find_package(ZLIB REQUIRED)
if (ZLIB_FOUND)
	set(LIBS ${LIBS} ${ZLIB_LIBRARIES})
	include_directories(${ZLIB_INCLUDE_DIRS})
endif ()

set(FFTW_FIND_QUIETLY 1)
include(FindFFTW)
find_package(FFTW REQUIRED)
if (FFTW3_FOUND)
	set(LIBS ${LIBS} ${FFTW3_LIBRARIES})
	include_directories(${FFTW3_INCLUDE_DIRS})
endif ()

include(FindFreetype)
find_package(Freetype REQUIRED)
if (FREETYPE_FOUND)
	set(LIBS ${LIBS} ${FREETYPE_LIBRARIES})
	include_directories(${FREETYPE_INCLUDE_DIRS})
endif ()

include(FindLibswscale)
set(LIBSWSCALE_MIN_VERSION 4.0.0)
find_package(Libswscale ${LIBSWSCALE_MIN_VERSION} REQUIRED)
if (LIBSWSCALE_FOUND)
    set(LIBS ${LIBS} ${LIBSWSCALE_LIBRARIES})
endif ()

include(FindOrcania)
find_package(Orcania ${ORCANIA_VERSION_REQUIRED} REQUIRED)
if (ORCANIA_FOUND)
  set(LIBS ${LIBS} ${ORCANIA_LIBRARIES})
	include_directories(${orcania_SOURCE_DIR}/include)
endif ()

include(FindYder)
find_package(Yder ${YDER_VERSION_REQUIRED} REQUIRED)
if (YDER_FOUND)
  set(LIBS ${LIBS} ${ORCANIA_LIBRARIES})
	include_directories(${yder_SOURCE_DIR}/include)
endif ()

include(FindUlfius)
find_package(Ulfius ${ULFIUS_VERSION_REQUIRED} REQUIRED)
if (ULFIUS_FOUND)
	include_directories(${ulfius_SOURCE_DIR}/include)
endif ()
set(LIBS ${LIBS} ${ULFIUS_LIBRARIES})

message(STATUS "Generating defaults.h")
set(BINIOU_VERSION ${LIBRARY_VERSION})
set(LEBINIOU_DATADIR "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATAROOTDIR}/lebiniou")
set(LEBINIOU_PLUGINSDIR "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}/lebiniou/plugins")
set(LEBINIOU_WWWDIR "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATAROOTDIR}/lebiniou/www")
set(LEBINIOU_SCHEMES_FILE "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATAROOTDIR}/lebiniou/etc/schemes.json")
set(INPUT_PLUGIN "oss, alsa, jackaudio, pulseaudio, sndfile, twip, random, NULL") # hardcoded
set(OUTPUT_PLUGINS "SDL2, caca, RTMP, mp4, NULL") # hardcoded
set(DEFAULT_INPUT_PLUGIN "alsa") # hardcoded
set(DEFAULT_OUTPUT_PLUGIN "SDL2") # hardcoded
configure_file(${SRC_PATH}/defaults.h.in defaults.h @ONLY)

message(STATUS "Generating commands.h")
set(COMMANDS_H "${CMAKE_CURRENT_BINARY_DIR}/commands.h")
file(READ ${SRC_PATH}/commands.h.head COMMANDS_H_HEAD)
file(READ ${SRC_PATH}/commands.h.tail COMMANDS_H_TAIL)
find_program(AWK awk mawk gawk)
execute_process(COMMAND "${AWK}" -f ${SRC_PATH}/commands_enum.awk ${SRC_PATH}/commands.c.in
                OUTPUT_VARIABLE COMMANDS_H_CONTENT)
file(WRITE ${COMMANDS_H} "${COMMANDS_H_HEAD}")
file(APPEND ${COMMANDS_H} "${COMMANDS_H_CONTENT}")
file(APPEND ${COMMANDS_H} "${COMMANDS_H_TAIL}")

message(STATUS "Generating commands.c")
execute_process(COMMAND "${AWK}" -f ${SRC_PATH}/gen.awk ${SRC_PATH}/commands.c.in
                OUTPUT_FILE commands.c)

message(STATUS "Generating schemes_str2option.c")
execute_process(COMMAND "${AWK}" -f ${SRC_PATH}/schemes_str2option.awk ${SRC_PATH}/options.h.in
                OUTPUT_FILE schemes_str2option.c)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

# build lib

set(LIB_SRC
  ${CMAKE_CURRENT_BINARY_DIR}/defaults.h
  ${CMAKE_CURRENT_BINARY_DIR}/commands.h
  ${CMAKE_CURRENT_BINARY_DIR}/commands.c 
  ${CMAKE_CURRENT_BINARY_DIR}/schemes_str2option.c
  ${SRC_PATH}/alarm.c
  ${SRC_PATH}/alarm.h 
  ${SRC_PATH}/brandom.c
  ${SRC_PATH}/brandom.h 
  ${SRC_PATH}/btimer.c
  ${SRC_PATH}/btimer.h 
  ${SRC_PATH}/buffer_8bits.c
  ${SRC_PATH}/buffer_8bits.h 
  ${SRC_PATH}/cmap_8bits.c
  ${SRC_PATH}/cmap_8bits.h 
  ${SRC_PATH}/cmapfader.c
  ${SRC_PATH}/cmapfader.h	
  ${SRC_PATH}/colormaps.c
  ${SRC_PATH}/colormaps.h 
  ${SRC_PATH}/commands_key.h
  ${SRC_PATH}/context.c
  ${SRC_PATH}/context_commands.c
  ${SRC_PATH}/context_export.c
  ${SRC_PATH}/context.h 
  ${SRC_PATH}/fader.c
  ${SRC_PATH}/fader.h 
  ${SRC_PATH}/image_filter.c
  ${SRC_PATH}/image_filter.h 
  ${SRC_PATH}/images.c
  ${SRC_PATH}/images.h 
  ${SRC_PATH}/input.c
  ${SRC_PATH}/input.h 
  ${SRC_PATH}/globals.c
  ${SRC_PATH}/globals.h 
  ${SRC_PATH}/layer.c
  ${SRC_PATH}/layer.h 
  ${SRC_PATH}/params3d.c
  ${SRC_PATH}/params3d.h 
  ${SRC_PATH}/particles.c
  ${SRC_PATH}/particles.h 
  ${SRC_PATH}/paths.c
  ${SRC_PATH}/paths.h 
  ${SRC_PATH}/plugin.h
  ${SRC_PATH}/plugin_parameters.c 
  ${SRC_PATH}/oscillo.c
  ${SRC_PATH}/oscillo.h 
  ${SRC_PATH}/sequence.c
  ${SRC_PATH}/sequence.h 
  ${SRC_PATH}/sequencemanager.c
  ${SRC_PATH}/sequencemanager.h 
  ${SRC_PATH}/shuffler.c
  ${SRC_PATH}/shuffler.h
  ${SRC_PATH}/shuffler_modes.h 
  ${SRC_PATH}/spline.c
  ${SRC_PATH}/spline.h 
  ${SRC_PATH}/translation.c
  ${SRC_PATH}/translation.h 
  ${SRC_PATH}/utils.c
  ${SRC_PATH}/utils.h
)

add_library(liblebiniou SHARED ${LIB_SRC})
set_target_properties(liblebiniou PROPERTIES
    COMPILE_OPTIONS -Wextra
    PUBLIC_HEADER "${INC_DIR}/biniou.h"
    VERSION "${LIBRARY_VERSION}"
    SOVERSION "${LIBRARY_SOVERSION}")
target_link_libraries(liblebiniou ${LIBS})

# build exec

add_executable(biniou
  ${CMAKE_CURRENT_BINARY_DIR}/defaults.h
  ${CMAKE_CURRENT_BINARY_DIR}/commands.h 
  ${CMAKE_CURRENT_BINARY_DIR}/commands.c
  ${CMAKE_CURRENT_BINARY_DIR}/schemes_str2option.c
  ${SRC_PATH}/main.c
  ${SRC_PATH}/commands_key.h
  ${SRC_PATH}/cmdline.c
  ${SRC_PATH}/signals.c
  ${SRC_PATH}/main.h
  ${SRC_PATH}/buffer_RGBA.c
  ${SRC_PATH}/buffer_RGBA.h 
  ${SRC_PATH}/context.c
  ${SRC_PATH}/context.h 
  ${SRC_PATH}/context_banks.c 
  ${SRC_PATH}/context_playlist.c
  ${SRC_PATH}/context_png.c 
  ${SRC_PATH}/context_replay.c
  ${SRC_PATH}/context_shortcuts.c 
  ${SRC_PATH}/image_8bits.c
  ${SRC_PATH}/image_8bits.h 
  ${SRC_PATH}/imagefader.c
  ${SRC_PATH}/imagefader.h 
  ${SRC_PATH}/imagefader_command.c 
  ${SRC_PATH}/image_filter.h 
  ${SRC_PATH}/options.h 
  ${SRC_PATH}/plugin.c 
  ${SRC_PATH}/plugins.c
  ${SRC_PATH}/plugins.h 
  ${SRC_PATH}/plugins_command.c 
  ${SRC_PATH}/pnglite.c
  ${SRC_PATH}/pnglite.h 
  ${SRC_PATH}/screenshot.c 
  ${SRC_PATH}/sequence2json.c 
  ${SRC_PATH}/sequence_copy.c 
  ${SRC_PATH}/sequences.c
  ${SRC_PATH}/sequences.h 
  ${SRC_PATH}/sequencemanager_command.c 
  ${SRC_PATH}/sequence_load.c
  ${SRC_PATH}/sequence_save.c 
  ${SRC_PATH}/biniou.c
  ${SRC_PATH}/biniou.h 
  ${SRC_PATH}/circle.c
  ${SRC_PATH}/circle.h 
  ${SRC_PATH}/cmapfader_command.c 
  ${SRC_PATH}/constants.h 
  ${SRC_PATH}/context_run.c 
  ${SRC_PATH}/context_set.c 
  ${SRC_PATH}/includes.h 
  ${SRC_PATH}/keyfile.c 
  ${SRC_PATH}/keys.h 
  ${SRC_PATH}/point2d.h
  ${SRC_PATH}/point3d.h 
  ${SRC_PATH}/rgba.h 
  ${SRC_PATH}/schemes.c
  ${SRC_PATH}/schemes.h 
  ${SRC_PATH}/schemes_random.c
)

target_link_libraries(biniou ${LIBS} liblebiniou)
add_dependencies(biniou liblebiniou)

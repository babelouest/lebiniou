/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "parameters.h"
#include "shuffler_modes.h"


uint32_t version = 0;
uint32_t options = BO_GFX|BO_FIRST|BO_NORANDOM;
char desc[] = "Video player";
char dname[] = "Video";

#define FFMPEG_CHECK "ffmpeg -h >/dev/null 2>&1"
#define VIDEO_CMD    "ffmpeg -loglevel quiet -i \"%s\"" \
  " -vf \"scale=%d:%d:force_original_aspect_ratio=decrease,pad=%d:%d:(ow-iw)/2:(oh-ih)/2:black,setsar=1,vflip\"" \
  " -pix_fmt gray -vcodec rawvideo -f image2pipe -r %d -vsync cfr -"

static FILE *video = NULL;
extern uint8_t max_fps;
static Shuffler_t *shuffler = NULL;

static void open_video(const char *);
static void close_video();
static void next_video();

/* parameters */
static json_t *playlist = NULL;
static enum ShufflerMode mode = BS_CYCLE;
#define BPP_FREEZE_AUTO_CHANGES "freeze_auto_changes"
static int freeze_auto = 0;
#define BPP_TRIGGER_AUTO_CHANGE "trigger_auto_change"
static int trigger_auto = 0;

static uint8_t played = 0;


json_t *
get_parameters()
{
  json_t *params = json_object();

  plugin_parameters_add_string_list(params, BPP_MODE, BS_NB, shuffler_modes, mode, BS_NB - 1, "Order in which the videos are played");
  plugin_parameters_add_playlist(params, BPP_PLAYLIST, playlist, "Select one or more videos to play");
  plugin_parameters_add_boolean(params, BPP_FREEZE_AUTO_CHANGES, freeze_auto, "Freeze auto changes until playlist is done");
  plugin_parameters_add_boolean(params, BPP_TRIGGER_AUTO_CHANGE, trigger_auto, "Trigger a change when playlist is done, if auto schemes or sequences was active");

  return params;
}


static void
next_video()
{
  uint16_t idx = Shuffler_get(shuffler);
  json_t *file_j = json_array_get(playlist, idx);
  const char *file = NULL;

  assert(json_is_string(file_j));
  file = json_string_value(file_j);
#ifdef DEBUG_VIDEO
  printf("[i] %s: %s: playing %s\n", __FILE__, __func__, file);
#endif
  close_video();
  open_video(file);
}


void
set_parameters(Context_t *ctx, const json_t *in_parameters)
{
  int8_t changed = plugin_parameter_parse_playlist(in_parameters, BPP_PLAYLIST, &playlist) & PLUGIN_PARAMETER_CHANGED;

  changed |= plugin_parameter_parse_string_list_as_int_range(in_parameters, BPP_MODE, BS_NB, shuffler_modes, (int *)&mode) & PLUGIN_PARAMETER_CHANGED;

  if (changed && json_array_size(playlist)) {
    Shuffler_delete(shuffler);
    shuffler = Shuffler_new(json_array_size(playlist));
    Shuffler_set_mode(shuffler, mode);
    next_video();
  }

  plugin_parameter_parse_boolean(in_parameters, BPP_FREEZE_AUTO_CHANGES, &freeze_auto);
  ctx->allow_random_changes = !freeze_auto;
  plugin_parameter_parse_boolean(in_parameters, BPP_TRIGGER_AUTO_CHANGE, &trigger_auto);
}


json_t *
parameters(Context_t *ctx, const json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


static void
open_video(const char *file)
{
  const gchar *home_dir = g_get_home_dir();
  char *path = g_strdup_printf("%s/." PACKAGE_NAME "/videos/%s", home_dir, file);
  char *cmd = g_strdup_printf(VIDEO_CMD, path, WIDTH, HEIGHT, WIDTH, HEIGHT, max_fps);

#ifdef DEBUG_VIDEO
  VERBOSE(printf("[i] %s: cmd= %s\n", __FILE__, cmd));
#endif
  if (NULL == (video = popen(cmd, "r"))) {
    xperror("popen");
  } else {
#ifdef DEBUG_VIDEO
    VERBOSE(printf("[i] %s: opened stream from %s\n", __FILE__, file));
#endif
  }
  g_free(path);
  g_free(cmd);
}


static void
close_video()
{
  if (NULL != video)
    if (-1 == pclose(video)) {
      xperror("pclose");
    }
  video = NULL;
}


int8_t
create(Context_t *ctx)
{
  if (check_command(FFMPEG_CHECK) == -1) {
    VERBOSE(printf("[!] %s: ffmpeg binary not found, plugin disabled\n", __FILE__));

    return 0;
  } else {
    playlist = json_array();

    return 1;
  }
}


void
on_switch_on(Context_t *ctx)
{
  if (NULL != shuffler) {
    Shuffler_reinit(shuffler);
    next_video();
  }
  if (freeze_auto) {
    ctx->allow_random_changes = 0;
  }
  played = 0;
}


void
on_switch_off(Context_t *ctx)
{
  close_video();

  if (freeze_auto) {
    // re-enable random changes
    ctx->allow_random_changes = 1;
  }
}


void
destroy(Context_t *ctx)
{
  close_video();
  json_decref(playlist);
  Shuffler_delete(shuffler);
}


void
run(Context_t *ctx)
{
  if (NULL != video) {
    void *dst = passive_buffer(ctx)->buffer;
    size_t res = fread(dst, sizeof(Pixel_t), BUFFSIZE, video);

    if (res == BUFFSIZE) {
#ifdef DEBUG_VIDEO
      VERBOSE(printf("%s: read %ld bytes\n", __FILE__, res));
#endif
    } else {
#ifdef DEBUG_VIDEO
      VERBOSE(printf("%s: short count: %ld\n", __FILE__, res));
#endif
      if (feof(video)) {
#ifdef DEBUG_VIDEO
        VERBOSE(printf("%s: end of file reached\n", __FILE__));
#endif
        played++;
#ifdef DEBUG_FREEZE
        xdebug("%s: played= %d\n", __func__, played);
#endif
        if (played == json_array_size(playlist)) {
#ifdef DEBUG_FREEZE
          xdebug("%s: END OF PLAYLIST\n", __func__);
#endif
          // all files played
#ifdef DEBUG_FREEZE
          xdebug("%s:%d freeze_auto= %d, trigger_auto= %d\n", __func__, __LINE__, freeze_auto, trigger_auto);
#endif
          if (freeze_auto) {
            // re-enable auto changes
            ctx->allow_random_changes = 1;
#ifdef DEBUG_FREEZE
            xdebug("%s:%d RESTORED freeze_auto= %d, trigger_auto= %d\n", __func__, __LINE__, freeze_auto, trigger_auto);
#endif
          }
          if (trigger_auto) {
            // force a random change
#ifdef DEBUG_FREEZE
            xdebug("%s: TRIGGER CHANGE\n", __func__);
#endif
            Alarm_trigger(ctx->a_random);
          }
          played = 0;
        }
#ifdef DEBUG_FREEZE
        xdebug("%s:%d next_video()\n", __func__, __LINE__);
#endif
        next_video();
      } else if (ferror(video)) {
        xerror("%s: an error occurred\n", __FILE__);
      }
    }
  }
}

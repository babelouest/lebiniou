/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 0;
uint32_t options = BO_GFX | BO_NORANDOM;
char desc[] = "Poincare effect";
char dname[] = "Poincare";

void
run(Context_t *ctx)
{
  const uint16_t a = 10, b = 0;
  uint16_t delta = b;
  short y = 0, j, d;
  uint16_t e;
  const Buffer8_t *src = NULL;
  Buffer8_t *dst = NULL;

  src = active_buffer(ctx);
  dst = passive_buffer(ctx);

  for (j = MAXY; j >= MINY; j--) {
    for (d = delta; d < MAXX; d++) {
      set_pixel_nc(dst, d + 1, j, get_pixel_nc(src, d - delta + 1, j));
    }

    for (e = 0; e < delta; e++) {
      set_pixel_nc(dst, e + 1, j, get_pixel_nc(src, MAXX - delta + e + 1, j));
    }

    if (!(y % a)) {
      delta++;
    }

    y++;
  }
}

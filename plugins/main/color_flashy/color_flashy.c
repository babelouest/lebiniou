/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "shuffler.h"
#include "parameters.h"


uint32_t version = 0;
uint32_t options = BO_COLORMAP;
char dname[] = "Color flashy";
enum LayerMode mode = LM_NONE;
char desc[] = "Flashy colors";


#define NCOLORS 7
static rgba_t colors[NCOLORS] = {
  { { 255, 0, 0, 0 } },
  { { 0, 255, 0, 0 } },
  { { 0, 0, 255, 0 } },
  { { 255, 255, 0, 0 } },
  { { 255, 0, 255, 0 } },
  { { 0, 255, 255, 0 } },
  { { 255, 255, 255, 0 } }
};

#define MIX(SRC, DST, IDX, I, PCT) \
  ((SRC.rgbav[IDX] * (1.0 - PCT) + DST.rgbav[IDX] * PCT * (reverse ? 255.0 - I : I) / 255.0))


/* parameters */
static int reverse = 0;


json_t *
get_parameters()
{
  json_t *params = json_object();

  plugin_parameters_add_boolean(params, BPP_REVERSE, reverse, "Reverse colors");

  return params;
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  // v1 API compat
  plugin_parameter_parse_int_range(in_parameters, BPP_REVERSE, &reverse);
  // v2 API
  plugin_parameter_parse_boolean(in_parameters, BPP_REVERSE, &reverse);
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


static uint8_t src_color_idx, dst_color_idx;
static rgba_t dst_color;

static Alarm_t *cirrus_alarm = NULL;
static Shuffler_t *shuffler = NULL;


int8_t
create(Context_t *ctx)
{
  cirrus_alarm = Alarm_new(3, 10);
  shuffler = Shuffler_new(NCOLORS);

  return 1;
}


void
destroy(Context_t *ctx)
{
  Alarm_delete(cirrus_alarm);
  Shuffler_delete(shuffler);
}


static void
set_colors()
{
  dst_color = colors[dst_color_idx];
}


static void
random_color()
{
  dst_color_idx = Shuffler_get(shuffler);
  set_colors();

  Alarm_init(cirrus_alarm);
}


void
on_switch_on(Context_t *ctx)
{
  src_color_idx = Shuffler_get(shuffler);
  ctx->sm->cur->auto_colormaps = ctx->cf->on = 0;
  random_color();
}


void
on_switch_off(Context_t *ctx)
{
  CmapFader_set(ctx->cf);
}


void
run(Context_t *ctx)
{
  rgba_t *col;

  for (uint16_t i = 0; i < 256; i++) {
    float pct;

    col = &(ctx->cf->cur)->colors[i];
    pct = Alarm_elapsed_pct(cirrus_alarm);

    col->col.r = MIX(ctx->cf->cur->colors[i], dst_color, 0, i, pct);
    col->col.g = MIX(ctx->cf->cur->colors[i], dst_color, 1, i, pct);
    col->col.b = MIX(ctx->cf->cur->colors[i], dst_color, 2, i, pct);
  }

  /* Ask the output driver to update it's colormap */
  ctx->cf->refresh = 1;

  /* Change to next color */
  if (Alarm_ring(cirrus_alarm)) {
    src_color_idx = dst_color_idx;
    random_color();
  }
}

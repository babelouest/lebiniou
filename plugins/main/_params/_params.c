/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "constants.h"
#include "context.h"
#include "parameters.h"


uint32_t version = 0;
uint32_t options = BO_SFX2D;
enum LayerMode mode = LM_OVERLAY;
char desc[] = "Debug parameters";
char dname[] = "_Params";


/* parameters */
static int integer = 32;
static double real = 1;
static int boolean = 0;
enum Mode { MODE_ONE = 0, MODE_TWO, MODE_THREE, MODE_FOUR, MODE_FIVE, MODE_NB } Mode_e;
static const char *string_list[MODE_NB] = { "Joe", "William", "Jack", "Averell", "Ma" };
static int string_idx = MODE_ONE;
static json_t *playlist = NULL;


json_t *
get_parameters()
{
  json_t *params = json_object();

  plugin_parameters_add_int(params, BPP_DEBUG_INTEGER, integer, 1, 64, 1, "Integer");
  plugin_parameters_add_double(params, BPP_DEBUG_REAL, real, 0, 10, 0.1, "Real");
  plugin_parameters_add_boolean(params, BPP_DEBUG_BOOLEAN, boolean, "Boolean");
  plugin_parameters_add_string_list(params, BPP_DEBUG_STRING_LIST, MODE_NB, string_list, string_idx, MODE_NB-1, "String list");
  plugin_parameters_add_playlist(params, BPP_DEBUG_PLAYLIST, playlist, "Playlist");

  return params;
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  plugin_parameter_parse_int_range(in_parameters, BPP_DEBUG_INTEGER, &integer);
  plugin_parameter_parse_double_range(in_parameters, BPP_DEBUG_REAL, &real);
  plugin_parameter_parse_boolean(in_parameters, BPP_DEBUG_BOOLEAN, &boolean);
  plugin_parameter_parse_string_list_as_int_range(in_parameters, BPP_DEBUG_STRING_LIST, MODE_NB, string_list, &string_idx);
  plugin_parameter_parse_playlist(in_parameters, BPP_DEBUG_PLAYLIST, &playlist);
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters();
}


int8_t
create(Context_t *ctx)
{
  playlist = json_array();

  return 1;
}


void
destroy(Context_t *ctx)
{
  json_decref(playlist);
}


void
run(Context_t *ctx)
{
  if (boolean) {
    xdebug("%s integer: %d, real: %f, boolean: %s, string: %s\n", __FILE__, integer, real,
           boolean ? "true" : "false", string_list[string_idx]);
    DEBUG_JSON("playlist", playlist);
  }
}

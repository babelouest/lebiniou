/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "parameters.h"
#include "gum.h"

/* TODO optimize
 * we can precompute an array for the "idx" variable
 * but then we have to handle on_delay_change event
 * --oliv3
 */

uint32_t version = 0;
uint32_t options = BO_LENS|BO_VER;
char dname[] = "Gum Y";
char desc[] = "Gum effect";


static inline void
gum(Context_t *ctx, uint16_t y, uint16_t max_x)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);
  short x;
  float cx, dx;

  dx = (float)HWIDTH / (float)(WIDTH - max_x);
  for (cx = x = MAXX; x >= max_x; x--) {
    set_pixel_nc(dst, x, y,
                 get_pixel_nc(src, (uint16_t)cx, y));
    cx -= dx;
  }

  dx = (float)(WIDTH - max_x) / (float)HWIDTH;
  for ( ; x >= 0; x--) {
    set_pixel_nc(dst, x, y,
                 get_pixel_nc(src, (uint16_t)cx, y));
    cx -= dx;
  }
}


static inline void
do_gum(Context_t *ctx, uint16_t y, float val)
{
  uint16_t max_x = HWIDTH + val * volume_scale * HWIDTH;
  gum(ctx, y, max_x);
}


void
on_switch_on(Context_t *ctx)
{
  /* Initialize parameters */
  volume_scale = 1;
}


void
run(Context_t *ctx)
{
  uint16_t y;

  pthread_mutex_lock(&ctx->input->mutex);
  for (y = 0; y < HEIGHT; y++) {
    uint16_t idx = (uint16_t)((float)y / (float)HEIGHT * (float)ctx->input->size);
    float value = Input_clamp(ctx->input->data[A_MONO][idx]);

    do_gum(ctx, y, value);
  }
  pthread_mutex_unlock(&ctx->input->mutex);
}

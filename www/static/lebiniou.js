/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

let lb_ws;
let ws_url;
let http_url;
let prefs = { };
let preview_fps = 1;
let update_interval = null;
let img = null;


function connect() {
    set_host_port();
    console.log("Connecting to " + ws_url);
    lb_ws = new WebSocket(ws_url);


    lb_ws.onopen = function() {
        console.log("Connected");
        ui_command('UI_CMD_CONNECT', null);
        ui_command('UI_CMD_APP_GET_SHORTCUTS', 'COL');
        ui_command('UI_CMD_APP_GET_SHORTCUTS', 'IMG');
        $('#lb_websocket_status').attr('src', '/ui/static/icons8-wi-fi-disconnected-48.png');
        if (prefs.display_frame) {
            updateFrame();
        }
    };


    lb_ws.onmessage = function(event) {
        // console.log("event.data", event.data);
        var payload = JSON.parse(event.data);
        // console.log("payload", payload);

        if (payload.ping) {
            // console.log("ping", payload.ping);
            // fps
            var fps = Math.min(payload.ping.fps, payload.ping.max_fps);
            var pct = Math.round(fps / payload.ping.max_fps * 100) + "%";
            $('#lb_fps').html(payload.ping.fps);
            var elem = document.getElementById("lb_fps");
            elem.style.width = pct;

            // uptime
            var uptime = payload.ping.uptime;
            var d = Math.floor(uptime / (3600*24));
            uptime -= d*3600*24;
            var h = Math.floor(uptime / 3600);
            uptime -= h*3600;
            var m = Math.floor(uptime / 60);
            uptime -= m*60;
            var res = '';
            if (d) {
                res += d + ' day' + ((d > 1) ? 's' : '');
            }
            res += ' ' + pad2(h) + ':' + pad2(m) + ':' + pad2(uptime);
            $('#lb_uptime').html(res);
        } else {
            // console.log("payload", payload);
            if (payload.sequence) {
                set_sequence(payload.sequence);
            }
            if (payload.sequence_name) {
                set_sequence_name(payload.sequence_name);
            }

            if ((payload.ui_command || payload.command) && payload.result) {
                if (payload.command) {
                    console.log("[C] " + payload.command + ":", payload.result);
                } else {
                    if ((payload.ui_command != "UI_CMD_APP_GET_SHORTCUTS")
                        && (payload.ui_command != "UI_CMD_APP_GET_BANK_SET")) {
                        console.log("[U] " + payload.ui_command + ":", payload.result);
                    }
                }

                if (!dispatch_command_result(payload)) {
                    if (payload.command) {
                        alert("FIXME: nothing to do for command '" + payload.command + "' ?");
                    } else {
                        alert("FIXME: nothing to do for UI command '" + payload.ui_command + "' ?");
                    }
                }
            }

            if (prefs.display_colormap && payload.sequence && payload.sequence.colormap) {
                set_colormap(payload.sequence.colormap);
            }
            if (prefs.display_image && payload.sequence && payload.sequence.image) {
                var url = http_url + "/image?name=" + encodeURI(payload.sequence.image)
                $('#lb_thumbnail').attr("src", url);
            }
        }
    }


    lb_ws.onclose = function(e) {
        $('#lb_shutdown').hide();
        lb_ws = null;
        clearInterval(update_interval);
        setTimeout(function() {
            connect();
        }, 1000);
    }


    lb_ws.onerror = function(err) {
        console.error('Socket encountered error: ', err.message, 'Closing socket');
        lb_ws.close();
    }
}


function command(cmd) {
    // console.log("SEND", ({ "command": cmd }));
    lb_ws.send(JSON.stringify({ "command": cmd }));
}


function ui_command(cmd, arg) {
    // console.log("SEND", ({ "ui_command": cmd, "arg": arg }));
    lb_ws.send(JSON.stringify({ "ui_command": cmd, "arg": arg }));
}


function start() {
    console.log("Starting LeBiniou");
    $('#lb_shutdown').hide();
    setup_preferences();
    threed_init();
    init_canvas();
    init_host_port();
    connect();
    plugins_init();
    sequence_init();
}


function dispatch_command_result(r) {
    var res = 0;

    res |= lebiniou_command_result(r);
    res |= banks_command_result(r);
    res |= colormaps_command_result(r);
    res |= images_command_result(r);
    res |= plugin_command_result(r);
    res |= plugins_command_result(r);
    res |= sequence_command_result(r);
    res |= threed_command_result(r);
    res |= shortcuts_command_result(r);

    return res;
}


function random_mode_result(what, bool) {
    if (bool) {
        $('#lb_random_' + what).css({ "color": "black", "background-color": "aqua" });
    } else {
        $('#lb_random_' + what).css({ "color": "white", "background-color": "black" });
    }
}


function auto_mode_result(what, bool) {
    if (bool) {
        $('#lb_auto_' + what).css({ "color": "black", "background-color": "aqua" });
    } else {
        $('#lb_auto_' + what).css({ "color": "white", "background-color": "black" });
    }
}


function lebiniou_command_result(r) {
    // console.log("lebiniou_command_result", r);
    switch (r.command) {
    case "CMD_APP_CLEAR_SCREEN":
    case "CMD_APP_NEXT_WEBCAM":
    case "CMD_APP_RANDOMIZE_SCREEN":
    case "CMD_APP_QUIT":
    case "CMD_APP_SAVE_QUIT":
    case "CMD_APP_SWITCH_BYPASS":
    case "CMD_APP_DISPLAY_COLORMAP":
        return 1;

    case "CMD_APP_FREEZE_INPUT":
        mute_result(r.result.mute);
        return 1;

    case "CMD_APP_NEXT_RANDOM_MODE":
    case "CMD_APP_PREVIOUS_RANDOM_MODE":
        random_mode_result("schemes", r.result.random_schemes);
        random_mode_result("sequences", r.result.random_sequences);
        return 1;

    case "CMD_APP_SET_WEBCAM_REFERENCE":
        return 1;
        
    case "CMD_APP_STOP_AUTO_MODES":
        random_mode_result("schemes", false);
        random_mode_result("sequences", false);
        auto_mode_result("colormaps", false);
        auto_mode_result("images", false);
        return 1;

    case "CMD_APP_SWITCH_CURSOR":
        return 1;

    case "CMD_APP_VOLUME_SCALE_DOWN":
    case "CMD_APP_VOLUME_SCALE_UP":
        $('#lb_volume_scale').html(double_value(r.result.volume_scale * 1000));
        return 1;

    case "CMD_APP_SWITCH_FULLSCREEN":
        set_fullscreen(r.result.fullscreen);
        return 1;

    case "CMD_APP_TOGGLE_AUTO_COLORMAPS":
        auto_mode_result("colormaps", r.result.auto_colormaps);
        return 1;

    case "CMD_APP_TOGGLE_AUTO_IMAGES":
        auto_mode_result("images", r.result.auto_images);
        return 1;

    case "CMD_SEQ_RESET":
        $('#lb_sequence').empty();
        return 1;
    }

    switch (r.ui_command) {
    case "UI_CMD_CONNECT":
        if (!r.result.input_plugin) {
            $('#lb_volume_scale_label').hide();
            $('#lb_volume_scale_container').hide();
            $('#lb_mute_label').hide();
            $('#lb_mute_container').hide();
        }
        $('#lb_shutdown').show();
        $('#lb_version').html(r.result.version);
        selected_plugin = r.result.selected_plugin;
        set_sequence_name(r.result.sequence_name);
        set_sequence(r.result.sequence);
        $('#lb_selected_plugin_dname').html(r.result.selected_plugin_dname);
        random_mode_result("schemes", r.result.random_schemes);
        random_mode_result("sequences", r.result.random_sequences);
        auto_mode_result("colormaps", r.result.auto_colormaps);
        auto_mode_result("images", r.result.auto_images);
        $.get(http_url + "/parameters/" + r.result.selected_plugin, function(data, status) {
            parameters({ parameters: data });
        });
        $('#lb_fade_delay').html(Math.floor(r.result.fade_delay * 1000) + " ms");
        $('#lb_fade_delay_slider').slider({
            min: 10,
            max: 10000,
            values: [ r.result.fade_delay * 1000 ],
            slide: function(event, ui) {
                ui_command('UI_CMD_APP_SET_FADE_DELAY', ui.value);
                $('#lb_fade_delay').html(ui.value + " ms");
            }
        });
        $('#lb_volume_scale').html(double_value(r.result.volume_scale * 1000));
        $('#lb_volume_scale_slider').slider({
            min: 10,
            max: 10000,
            values: [ r.result.volume_scale * 1000 ],
            slide: function(event, ui) {
                ui_command('UI_CMD_APP_SET_VOLUME_SCALE', ui.value);
                $('#lb_volume_scale').html(double_value(ui.value));
            }
        });
        $('#lb_auto_colormaps_range').html(r.result.colormaps_min + "-" + r.result.colormaps_max);
        $('#lb_auto_colormaps_slider').slider({
            range: true,
            min: 1,
            max: 60,
            values: [ r.result.colormaps_min, r.result.colormaps_max ],
            slide: function(event, ui) {
                ui_command('UI_CMD_APP_SET_DELAY', { what: "auto_colormaps", min: ui.values[0], max: ui.values[1] });
            }
        });
        $('#lb_auto_images_range').html(r.result.images_min + "-" + r.result.images_max);
        $('#lb_auto_images_slider').slider({
            range: true,
            min: 1,
            max: 60,
            values: [ r.result.images_min, r.result.images_max ],
            slide: function(event, ui) {
                ui_command('UI_CMD_APP_SET_DELAY', { what: "auto_images", min: ui.values[0], max: ui.values[1] });
            }
        });
        $('#lb_auto_sequences_range').html(r.result.sequences_min + "-" + r.result.sequences_max);
        $('#lb_auto_sequences_slider').slider({
            range: true,
            min: 1,
            max: 60,
            values: [ r.result.sequences_min, r.result.sequences_max ],
            slide: function(event, ui) {
                ui_command('UI_CMD_APP_SET_DELAY', { what: "auto_sequences", min: ui.values[0], max: ui.values[1] });
            }
        });
        if (!r.result.webcams) {
            $('#lb_bypass_webcam').hide();
            $('#lb_plugins_tab_webcam').hide();
        }
        if (r.result.webcams > 1) {
            $('#lb_auto_webcams_range').html(r.result.webcams_min + "-" + r.result.webcams_max);
            $('#lb_auto_webcams_slider').slider({
                range: true,
                min: 1,
                max: 60,
                values: [ r.result.webcams_min, r.result.webcams_max ],
                slide: function(event, ui) {
                    ui_command('UI_CMD_APP_SET_DELAY', { what: "auto_webcams", min: ui.values[0], max: ui.values[1] });
                }
            });
        } else {
            $('#lb_auto_webcams_row').hide();
        }
        $('#lb_max_fps').html("Max: " + r.result.max_fps);
        $('#lb_max_fps_slider').slider({
            min: 1,
            max: 255,
            values: [ r.result.max_fps ],
            slide: function(event, ui) {
                ui_command('UI_CMD_APP_SET_MAX_FPS', ui.value);
                $('#lb_max_fps').html("Max: " + ui.value);
            }
        });
        $('#lb_frame_refresh_slider').slider({
            min: 1,
            max: 10,
            values: [ preview_fps ],
            slide: function(event, ui) {
                preview_fps = ui.value;
                $('#lb_frame_refresh').html(preview_fps + " fps");
                updateFrame();
            }
        });
        $('#lb_websocket_status').attr('src', '/ui/static/icons8-wi-fi-48.png');

        if (r.result.output_plugins) {
            if (r.result.output_plugins.length > 1) {
                $('#lb_outputs_label').html("Outputs");
            }
            if (!r.result.output_plugins.includes("SDL2")) {
                $('#lb_output_SDL2').hide();
            } else {
                set_fullscreen(r.result.fullscreen);
            }
            if (!r.result.output_plugins.includes("mp4")) {
                $('#lb_output_mp4').hide();
            } else {
                set_encoding(r.result.encoding);
            }
        } else {
            $('#lb_outputs_label').hide();
            $('#lb_outputs_row').hide();
        }
        $('#lb_auto_sequences_mode').val(r.result.auto_sequences_mode);
        $('#lb_auto_colormaps_mode').val(r.result.auto_colormaps_mode);
        $('#lb_auto_images_mode').val(r.result.auto_images_mode);
        $('#lb_auto_webcams_mode').val(r.result.auto_webcams_mode);
        mute_result(r.result.mute);
        return 1;

    case "UI_CMD_APP_SET_AUTO_MODE":
    case "UI_CMD_APP_GET_SHORTCUTS":
        return 1;

    case "UI_CMD_APP_SELECT_PLUGIN":
        $('#lb_selected_plugin_dname').html(r.result.selected_plugin_dname);
        plugins_init();
        return 1;

    case "UI_CMD_APP_SET_DELAY":
        switch (r.result.what) {
        case "auto_colormaps":
            $('#lb_auto_colormaps_range').html(r.result.min + "-" + r.result.max);
            $('#lb_auto_colormaps_slider').slider({ values: [ r.result.min, r.result.max ] });
            break;

        case "auto_images":
            $('#lb_auto_images_range').html(r.result.min + "-" + r.result.max);
            $('#lb_auto_images_slider').slider({ values: [ r.result.min, r.result.max ] });
            break;

        case "auto_sequences":
            $('#lb_auto_sequences_range').html(r.result.min + "-" + r.result.max);
            $('#lb_auto_sequences_slider').slider({ values: [ r.result.min, r.result.max ] });
            break;

        case "auto_webcams":
            $('#lb_auto_webcams_range').html(r.result.min + "-" + r.result.max);
            $('#lb_auto_webcams_slider').slider({ values: [ r.result.min, r.result.max ] });
            break;
        }
        return 1;

    case "UI_CMD_APP_SET_FADE_DELAY":
        return 1;

    case "UI_CMD_APP_TOGGLE_RANDOM_SCHEMES":
        random_mode_result("schemes", r.result.random_schemes);
        return 1;

    case "UI_CMD_APP_TOGGLE_RANDOM_SEQUENCES":
        random_mode_result("sequences", r.result.random_sequences);
        return 1;

    case "UI_CMD_APP_SET_MAX_FPS":
        $('#lb_max_fps').html("Max: " + r.result.max_fps);
        $('#lb_max_fps_slider').slider({ value: r.result.max_fps});
        return 1;

    case "UI_CMD_APP_SET_VOLUME_SCALE":
        $('#lb_volume_scale').html(double_value(r.result.volume_scale));
        return 1;

    case "UI_CMD_NOOP":
        return 1;

    case "UI_CMD_OUTPUT":
        output_result(r.result);
        return 1;
    }

    return 0;
}


function init_host_port() {
    $('#lb_host').val(localStorage.getItem("host") || "localhost");
    $('#lb_port').val(localStorage.getItem("port") || 30543);
}


function set_host_port() {
    var host = $('#lb_host').val();
    var port = $('#lb_port').val();

    ws_url = "ws://" + host + ":" + port + "/ui";
    http_url = "http://" + host + ":" + port;

    localStorage.setItem("host", host);
    localStorage.setItem("port", Number(port));

    if (lb_ws) {
        lb_ws.close();
    }
}

window.onunload = function() {
    if (lb_ws) {
        lb_ws.close();
    }
}


function pad2(x) {
    return (x < 10) ? "0" + x : x;
}


function switch_boolean_preference(name, pref) {
    prefs[pref] = !prefs[pref];
    localStorage.setItem(name, prefs[pref]);
}


function set_visible(e, b) {
    if (b) {
        e.show();
    } else {
        e.hide();
    }
}


function toggle(what) {
    switch (what) {
    case "lb_pref_display_colormap":
        switch_boolean_preference("lb_pref_display_colormap", "display_colormap");
        set_visible($('#lb_colormap_row'), prefs.display_colormap);
        break;

    case "lb_pref_display_image":
        switch_boolean_preference("lb_pref_display_image", "display_image");
        set_visible($('#lb_image_row'), prefs.display_image);
        break;

    case "lb_pref_display_frame":
        switch_boolean_preference("lb_pref_display_frame", "display_frame");
        set_visible($('#lb_frame'), prefs.display_frame);
        set_visible($('#lb_frame_row'), prefs.display_frame);
        if (prefs.display_frame) {
            updateFrame();
        } else {
            clearInterval(update_interval);
        }
        break;

    case "lb_pref_display_threed":
        switch_boolean_preference("lb_pref_display_threed", "display_threed");
        set_visible($('#lb_threed'), prefs.display_threed);
        if (prefs.display_threed) {
            webGLStart();
        }
        break;

    case "lb_pref_display_outputs":
        switch_boolean_preference("lb_pref_display_outputs", "display_outputs");
        set_visible($('#lb_outputs'), prefs.display_outputs);
        break;

    case "lb_pref_display_tooltips":
        switch_boolean_preference("lb_pref_display_tooltips", "display_tooltips");
        if (prefs.display_tooltips) {
            $(document).tooltip("enable");
        } else {
            $(document).tooltip("disable");
        }
        break;

    case "lb_pref_display_controls":
        switch_boolean_preference("lb_pref_display_controls", "display_controls");
        if (prefs.display_controls) {
            $(".lb-sidebar").show();
            $(".lb-plugins").css({ "grid-column": "span 7" });
        } else {
            $(".lb-sidebar").hide();
            $(".lb-plugins").css({ "grid-column": "span 12" });
        }
        break;
    }
}


function setup_preferences() {
    prefs.display_colormap = JSON.parse(localStorage.getItem("lb_pref_dispaly_colormap") || true);
    set_visible($('#lb_colormap_row'), prefs.display_colormap);

    prefs.display_image = JSON.parse(localStorage.getItem("lb_pref_display_image") || true);
    set_visible($('#lb_image_row'), prefs.display_image);

    prefs.display_frame = JSON.parse(localStorage.getItem("lb_pref_display_frame") || true);
    set_visible($('#lb_frame'), prefs.display_frame);
    if (prefs.display_frame) {
        $('#lb_frame_refresh').html(preview_fps + " fps");
    }

    prefs.display_threed = JSON.parse(localStorage.getItem("lb_pref_display_threed") || true);
    set_visible($('#lb_threed'), prefs.display_threed);

    prefs.display_outputs = JSON.parse(localStorage.getItem("lb_pref_display_outputs") || true);
    set_visible($('#lb_outputs'), prefs.display_outputs);

    prefs.display_tooltips = JSON.parse(localStorage.getItem("lb_pref_display_tooltips") || true);
    $('#lb_tooltips').prop("checked", prefs.display_tooltips);
    $(document).tooltip();
    if (prefs.display_tooltips) {
        $(document).tooltip("enable");
    } else {
        $(document).tooltip("disable");
    }

    prefs.display_controls = JSON.parse(localStorage.getItem("lb_pref_display_controls") || true);
    $('#lb_controls').prop("checked", prefs.display_controls);
}


function update_frame() {
    if (prefs.display_frame && img) {
        const FACTOR = 2;
        const FRAME_WIDTH = 480 / FACTOR;
        const FRAME_HEIGHT = 270 / FACTOR;

        img.src = http_url + "/frame?id=" + new Date().getTime()
            + "&w=" + FRAME_WIDTH + "&h=" + FRAME_HEIGHT;
    }
}


function init_canvas() {
    const canvas = document.getElementById("lb_frame");
    if (canvas) {
        const context = canvas.getContext("2d");

        img = new Image();
        img.onload = function() {
            canvas.setAttribute("width", img.width)
            canvas.setAttribute("height", img.height)
            context.drawImage(this, 0, 0);
        };
        update_frame();
    }
}


function double_value(d) {
    return (d / 1000).toFixed(2);
}


function clear_frame() {
    if (window.event.shiftKey) {
        command('CMD_APP_RANDOMIZE_SCREEN');
    } else {
        command('CMD_APP_CLEAR_SCREEN');
    }
}


function set_fullscreen(b) {
    if (b) {
        $('#lb_fullscreen').attr("src", "/ui/static/icons8-normal-screen-24.png");
        $('#lb_fullscreen').attr("title", "Unset fullscreen");
    } else {
        $('#lb_fullscreen').attr("src", "/ui/static/icons8-toggle-full-screen-24.png");
        $('#lb_fullscreen').attr("title", "Set fullscreen");
    }
}


function set_encoding(b) {
    if (b) {
        $('#lb_encoding').attr("src", "/ui/static/icons8-stop-squared-24.png");
        $('#lb_encoding_button').attr("onclick", "ui_command('UI_CMD_OUTPUT', { plugin: 'mp4', command: 'stop_encoding' });");
        $('#lb_encoding_button').attr("title", "Stop video encoding");
    } else {
        $('#lb_encoding').attr("src", "/ui/static/icons8-record-24.png");
        $('#lb_encoding_button').attr("onclick", "ui_command('UI_CMD_OUTPUT', { plugin: 'mp4', command: 'start_encoding' });");
        $('#lb_encoding_button').attr("title", "Start video encoding");
    }
}


function output_result(r) {
    switch (r.plugin) {
    case "mp4":
        set_encoding(r.result.encoding);
        break;
    }
}


function shutdown() {
    if (lb_ws) {
        if (confirm("Quit Le Biniou ?")) {
            $('#lb_websocket_status').attr('src', '/ui/static/icons8-wi-fi-disconnected-48.png');
            command('CMD_APP_QUIT');
        }
    }
}


function mute_result(b) {
    if (b) {
        $('#lb_mute_button_img').attr("src", "/ui/static/icons8-play-button-circled-30.png");
        $('#lb_mute').html("Unfreeze input");
        $('#lb_mute_button').attr("title", "Unfreeze input");
    } else {
        $('#lb_mute_button_img').attr("src", "/ui/static/icons8-pause-button-30.png");
        $('#lb_mute').html("Freeze input");
        $('#lb_mute_button').attr("title", "Freeze input");
    }
}


function updateFrame() {
    if (update_interval) {
        clearInterval(update_interval);
    }
    let interval = 1 / preview_fps * 1000;
    update_interval = setInterval(update_frame, interval);
}

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

function shortcut_change(what, s) {
    command('CMD_' + what + '_USE_SHORTCUT_' + s);
}


function shortcut_store(what, s) {
    command('CMD_' + what + '_STORE_SHORTCUT_' + s);
}


function shortcut_click(what, s) {
    if (window.event.shiftKey) {
        shortcut_store(what, s);
    } else {
        shortcut_change(what, s);
    }
}


function create_shortcuts(css, type, shortcuts) {
    $('#lb_' + css + 's_shortcuts').empty();
    for (let i = 1; i <= shortcuts.length; i++) {
        $('#lb_' + css + 's_shortcuts').append('<div>'
                                               + '<button id="lb_' + css + '_shortcut_' + i + '"'
                                               + ' onclick="shortcut_click(\'' + type + '\', ' + i + ');"'
                                               + ' class="lb-button">'
                                               + i
                                               + '</button>'
                                               + '</div>');
        if (shortcuts[i - 1]) {
            $('#lb_' + css + '_shortcut_' + i).css({ "color": "orange" });
        } else {
            $('#lb_' + css + '_shortcut_' + i).css({ "color": "lime" });
        }
    }
}


function shortcuts_command_result(r) {
    if (r.command) {
        if (r.command.startsWith("CMD_COL_STORE_SHORTCUT_")) {
            const shortcut = Number(r.command.replace(/^CMD_COL_STORE_SHORTCUT_/, ''));
            $('#lb_colormap_shortcut_' + shortcut).css({ "color": "orange" });
            return 1;
        }

        if (r.command.startsWith("CMD_IMG_STORE_SHORTCUT_")) {
            const shortcut = Number(r.command.replace(/^CMD_IMG_STORE_SHORTCUT_/, ''));
            $('#lb_image_shortcut_' + shortcut).css({ "color": "orange" });
            return 1;
        }

        if (r.command.startsWith("CMD_COL_USE_SHORTCUT_")) {
            if (r.result.colormap) {
                $('#lb_colormap').html(r.result.colormap);
                set_colormap(r.result.colormap);
            }
            return 1;
        }

        if (r.command.startsWith("CMD_IMG_USE_SHORTCUT_")) {
            if (r.result.image) {
                $('#lb_image').html(r.result.image);
                set_image(r.result.image);
            }
            return 1;
        }
    }

    if (r.ui_command) {
        switch (r.ui_command) {
        case "UI_CMD_APP_GET_SHORTCUTS":
            switch (r.result.type) {
            case "COL":
                create_shortcuts("colormap", r.result.type, r.result.shortcuts);
                return 1;

            case "IMG":
                create_shortcuts("image", r.result.type, r.result.shortcuts);
                return 1;
            }
        }
    }

    return 0;
}

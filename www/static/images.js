/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

function images_command_result(r) {
    switch (r.command) {
    case "CMD_IMG_PREVIOUS":
    case "CMD_IMG_RANDOM":
    case "CMD_IMG_NEXT":
        set_image(r.result.image);
        return 1;
    }

    switch (r.ui_command) {
    case "UI_CMD_CONNECT":
        set_image(r.result.sequence.image);
        return 1;

    case "UI_CMD_IMG_PREVIOUS_N":
    case "UI_CMD_IMG_NEXT_N":
        set_image(r.result.image);
        return 1;
    }

    return 0;
}


function set_image(name) {
    var url = http_url + "/image?name=" + encodeURI(name);

    $('#lb_image').html(name);
    $('#lb_thumbnail').attr("src", url);
}

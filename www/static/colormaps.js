/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

function colormaps_command_result(r) {
    switch (r.command) {
    case "CMD_COL_PREVIOUS":
    case "CMD_COL_RANDOM":
    case "CMD_COL_NEXT":
        set_colormap(r.result.colormap);
        return 1;
    }

    switch (r.ui_command) {
    case "UI_CMD_CONNECT":
        set_colormap(r.result.sequence.colormap);
        return 1;

    case "UI_CMD_COL_PREVIOUS_N":
    case "UI_CMD_COL_NEXT_N":
        set_colormap(r.result.colormap);
        return 1;
    }

    return 0;
}


function set_colormap(name) {
    $('#lb_colormap').html(name);
    $.get(http_url + "/colormap?name=" + encodeURI(name), function(state, status) {
        $('#lb_colormap_w3c').empty();
        for (i = 0; i < state.w3c.length; i++) {
            $('#lb_colormap_w3c').append('<div id="lb_colormap_w3c_color_' + i + '">&nbsp;</div>');
            $('#lb_colormap_w3c_color_' + i).css({
                "background-color": state.w3c[i],
                "float": "left",
                "width": "1px",
                "height": "180px"
            });
        }
    });
}

BEGIN {
    print "/*";
    print " *  Copyright 1994-2020 Olivier Girondel";
    print " *";
    print " *  This file is part of lebiniou.";
    print " *";
    print " *  lebiniou is free software: you can redistribute it and/or modify";
    print " *  it under the terms of the GNU General Public License as published by";
    print " *  the Free Software Foundation, either version 2 of the License, or";
    print " *  (at your option) any later version.";
    print " *";
    print " *  lebiniou is distributed in the hope that it will be useful,";
    print " *  but WITHOUT ANY WARRANTY; without even the implied warranty of";
    print " *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the";
    print " *  GNU General Public License for more details.";
    print " *";
    print " *  You should have received a copy of the GNU General Public License";
    print " *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.";
    print " */";
    print;
    print "/*";
    print " * Automagically generated from commands.c.in";
    print " * DO NOT EDIT !!!";
    print " */";
    print;
    print "var labels = [];";
    print "var data = [];";
    print "var indexes = {};";
    print;
    print "function init_chart() {";

    i = 0;
}


{
    if (($1 == "#") || ($0 == "") || ($1 == "-") || ($1 == "**") || ($1 == "*"))
	next;
  
    if ($3 != "-") {
        if (($4 != "CMD_APP_SWITCH_FULLSCREEN") \
            && ($4 != "CMD_APP_QUIT") \
            && ($4 != "CMD_APP_SAVE_QUIT") \
            && ($4 != "CMD_APP_SCREENSHOT") \
            && ($4 != "CMD_SEQ_SAVE_FULL") \
            && ($4 != "CMD_SEQ_SAVE_BARE") \
            && ($4 != "CMD_SEQ_UPDATE_FULL") \
            && ($4 != "CMD_SEQ_UPDATE_BARE") \
            && ($4 !~ /_SHORTCUT_/) \
            && ($4 !~ /_BANK_/)) {
            tail = substr($0, (length($1 $2 $3 $4) + 5));

            printf "    labels.push('%s');\n", $4;
            printf "    data.push(0);\n";
            printf "    indexes['%s'] = %d;\n", $4, i;
        
            i++;
        }
    } else {
        if (($4 !~ /_BANK_/) \
            && ($4 != "UI_CMD_OUTPUT") \
            && ($4 != "UI_CMD_SEQ_RENAME")) {
            printf "    labels.push('%s');\n", $4;
            printf "    data.push(0);\n";
            printf "    indexes['%s'] = %d;\n", $4, i;
        
            i++;
        }
    }
}


END {
    print "}";
    printf "\ninit_chart();\n", i;
}

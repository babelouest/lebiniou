/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

var TABS = 10;
var OPTIONS = [
    "BO_SFX2D",
    "BO_SFX3D",
    "BO_GFX",
    "BO_IMAGE",
    "BO_BLUR",
    "BO_DISPLACE",
    "BO_WARP",
    "BO_LENS",
    "BO_WEBCAM",
    null
];
var tab = 0;


function select_tab(idx) {
    tab = idx;
    $('#lb_plugins_tab_' + idx).css({
        "color": "blue",
        "background-color": "black"
    });
}


function unselect_tab(idx) {
    $('#lb_plugins_tab_' + idx).css({
        "color": "white",
        "background-color": "black"
    });
}


function activate_tab(idx) {
    for (var i = 0; i < TABS; i++) {
        unselect_tab(i);
    }
    select_tab(idx);

    $.get(http_url + "/plugins" + (OPTIONS[idx] ? "?option=" + OPTIONS[idx] : ""), function(plugins, status) {
        $('#lb_plugins').empty();
        for (var i = 0; i < plugins.length; i++) {
            var plugin = plugins[i].name;

            $('#lb_plugins').append('<button id="lb_plugin_'
                                    + i + '" class="lb-plugin lb-button" onclick="select_plugin(\'' + plugin + '\');"'
                                    + ' title="Click to select ' + plugins[i].display_name + ', Ctrl+Click to add/remove">'
                                    + plugins[i].display_name + '</button>');
            if (plugins[i].selected) {
                $('#lb_plugin_' + i).css({
                    "color": "black",
                    "background-color": "lime"
                });
            }
        }
    });
}


function plugins_init() {
    activate_tab(tab);
}


function select_plugin(p) {
    ui_command('UI_CMD_APP_SELECT_PLUGIN', p);
    if (window.event.ctrlKey) {
        command('CMD_APP_TOGGLE_SELECTED_PLUGIN');
    }
}


function plugins_command_result(r) {
    switch (r.command) {
    case "CMD_PLG_PREVIOUS":
    case "CMD_PLG_NEXT":
    case "CMD_PLG_SCROLL_DOWN":
    case "CMD_PLG_SCROLL_UP":
        return 1;

    case "UI_CMD_CONNECT":
        plugins_init(r.result.selected_plugin);
        return 1;

    }

    return 0;
}

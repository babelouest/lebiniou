/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

function sequence_command_result(r) {
    switch (r.command) {
    case "CMD_APP_FIRST_SEQUENCE":
    case "CMD_APP_LAST_SEQUENCE":
    case "CMD_APP_NEXT_SEQUENCE":
    case "CMD_APP_PREVIOUS_SEQUENCE":
    case "CMD_APP_RANDOM_SCHEME":
    case "CMD_APP_RANDOM_SEQUENCE":
    case "CMD_SEQ_LAYER_DEFAULT":
    case "CMD_SEQ_LAYER_NEXT":
    case "CMD_SEQ_LAYER_PREVIOUS":
    case "CMD_SEQ_MOVE_DOWN":
    case "CMD_SEQ_MOVE_UP":
    case "CMD_SEQ_TOGGLE_LENS":
    case "CMD_APP_TOGGLE_SELECTED_PLUGIN":
        if (r.result.sequence_name) {
            set_sequence_name(r.result.sequence_name);
        }
        set_sequence(r.result.sequence);
        return 1;

    case "CMD_SEQ_PARAM_DEC":
    case "CMD_SEQ_PARAM_DEC_FAST":
    case "CMD_SEQ_PARAM_INC":
    case "CMD_SEQ_PARAM_INC_FAST":
        parameters(r.result);
        return 1;

    case "CMD_SEQ_PARAM_PREVIOUS":
    case "CMD_SEQ_PARAM_NEXT":
        return 1;

    case "CMD_SEQ_SAVE_BARE":
    case "CMD_SEQ_SAVE_FULL":
        set_sequence_name(r.result.sequence_name);
        return 1;

    case "CMD_SEQ_UPDATE_BARE":
    case "CMD_SEQ_UPDATE_FULL":
        return 1;

    case "CMD_SEQ_PARAM_NEXT":
    case "CMD_SEQ_PARAM_SELECT_PREVIOUS":
    case "CMD_SEQ_PARAM_SELECT_NEXT":
        return 1;
    }

    switch (r.ui_command) {
    case "UI_CMD_CONNECT":
    case "UI_CMD_SEQ_REORDER":
        set_sequence_name(r.result.sequence_name);
        set_sequence(r.result.sequence);
        return 1;

    case "UI_CMD_SEQ_RENAME":
        if (r.result.error) {
            alert('Renaming failed: ' + r.result.error);
        }
        set_sequence_name(r.result.sequence_name);
        return 1;
    }

    return 0;
}


function sequence_init() {
    $("#lb_sequence").sortable({
        update: function(event, ui) {
            var ul = $('#lb_sequence').children();
            var new_sequence = [];
            for (var i = 0; i < ul.length; i++) {
                new_sequence.push(ul[i].getAttribute('name'));
            }
            ui_command('UI_CMD_SEQ_REORDER', new_sequence);
        }
    });
}


function set_sequence(s) {
    // console.log("set_sequence", s);
    var p = s.plugins;

    if (p) {
        var lens = "|";
        var lens_found = false;

        $('#lb_sequence').empty();
        for (var i = 0; i < p.length; i++) {
            var li = '<li name="' + p[i].name + '">'
                + '<button class="lb-plugin" onclick="ui_command(\'UI_CMD_APP_SELECT_PLUGIN\', \'' + p[i].name + '\');">' + p[i].display_name + '</button>'
                + '<button class="lb-layer-mode" onclick="ui_command(\'UI_CMD_APP_SELECT_PLUGIN\', \'' + p[i].name + '\'); command(\'CMD_SEQ_LAYER_NEXT\');">' + fmt(p[i].mode) + '</button>';
            li += '<button class="lb-lens" onclick="ui_command(\'UI_CMD_APP_SELECT_PLUGIN\', \'' + p[i].name + '\'); command(\'CMD_SEQ_TOGGLE_LENS\');">';
            if (p[i].lens) {
                lens_found = true;
                li += '&#x21aa;';
            } else if (!lens_found) {
                li += '&#x2193;';
            } else {
                li += '&nbsp;';
            }
            li += '</button>';
            li += '</li>';
            $('#lb_sequence').append(li);

            if (p[i].name == selected_plugin) {
                parameters(p[i]);
            }
        }
    }

    auto_mode_result("colormaps", s.auto_colormaps);
    $('#lb_colormap').html(s.colormap);

    auto_mode_result("images", s.auto_images);
    $('#lb_image').html(s.image);

    // set_rotations(s.params3d);
}


function save_sequence() {
    if (window.event.shiftKey) {
        command('CMD_SEQ_SAVE_BARE');
    } else {
        command('CMD_SEQ_SAVE_FULL');
    }
}


function update_sequence() {
    if (window.event.shiftKey) {
        command('CMD_SEQ_UPDATE_BARE');
    } else {
        command('CMD_SEQ_UPDATE_FULL');
    }
}


function reset_sequence() {
    command('CMD_SEQ_RESET');
    if (window.event.ctrlKey) {
        command('CMD_APP_CLEAR_SCREEN');
    }
    if (window.event.shiftKey) {
        command('CMD_APP_RANDOMIZE_SCREEN');
    }
}


function set_sequence_name(name) {
    $('#lb_sequence_name').val(name);
}

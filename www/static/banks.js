/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

const DELAY = 2000; // for clear/store bank CSS changes
const MAX_BANKS = 24;
let bank, bank_set, banks = new Array(MAX_BANKS);

function bank_set_key_off(k) {
    $('#lb_bank_set_' + k).css({
        "background-color": "black",
        "color": "orange"
    });
}


function bank_set_key_on(k) {
    $('#lb_bank_set_' + k).css({
        "background-color": "orange",
        "color": "black"
    });
}


function bank_key_off(k) {
    $('#lb_bank_' + k).css({
        "background-color": "black",
        "color": banks[k - 1] ? "orange" : "lime"
    });
}


function bank_keys_off() {
    for (let i = 1; i <= MAX_BANKS; i++) {
        bank_key_off(i);
    }
}


function bank_set_change(b) {
    // console.log("Bank set change: " + b);
    command('CMD_APP_USE_BANK_SET_' + b);
    bank_set = b;
}


function bank_key_on(b) {
    $('#lb_bank_' + b).css({
        "background-color": "orange",
        "color": "black"
    });
}


function bank_change(b) {
    ui_command('UI_CMD_BANK', { "command": "use", "bank": b });
    bank_key_on(b);
}


function bank_store(b) {
    // console.log("Bank store: " + b);
    ui_command('UI_CMD_BANK', { "command": "store", "bank": b });
    $('#lb_bank_' + b).css({
        "background-color": "lime",
        "color": "black"
    });
    setTimeout(function() {
        if (b != bank) {
            $('#lb_bank_' + b).css({
                "background-color": "black",
                "color": "orange"
            });
        } else {
            bank_key_on(b);
        }
    }, DELAY);
}


function bank_clear(b) {
    // console.log("Bank clear: " + b);
    if (confirm("Clear bank " + b + " ?")) {
        // bank_keys_off();
        $('#lb_bank_' + b).css({
            "background-color": "red",
            "color": "black"
        });
        setTimeout(function() {
            ui_command('UI_CMD_BANK', { "command": "clear", "bank": b });
            $('#lb_bank_' + b).css({
                "background-color": "black",
                "color": "lime"
            });
        }, DELAY);
    }
}


function bank_click(b) {
    if (window.event.ctrlKey && window.event.shiftKey) {
        bank_clear(b);
    } else if (window.event.shiftKey) {
        bank_store(b);
    } else {
        bank_change(b);
    }
}


function banks_init() {
    // console.log("Initializing banks");
    $('#lb_bank_sets').empty();
    $('#lb_bank_sets').append('<div class="lb-label lb-bank-sets-label" title="Select a bank set">Set</div>');
    $('#lb_banks').empty();
    $('#lb_banks').append('<div class="lb-label lb-banks-label" title="Click to use a bank, Shift-click to store, Ctrl-Shift-click to erase">Bank</div>');

    for (let i = 1; i <= MAX_BANKS; i++) {
        $('#lb_bank_sets').append('<div class="lb-bank">'
                                  + '<button id="lb_bank_set_' + i + '"'
                                  + ' onclick="bank_set_change(' + i + ');"'
                                  + ' class="lb-button lb-bank-button">'
                                  + i
                                  + '</button>'
                                  + '</div>');
        $('#lb_banks').append('<div class="lb-bank">'
                              + '<button id="lb_bank_' + i + '"'
                              + ' onclick="bank_click(' + i + ');"'
                              + ' class="lb-button lb-bank-button">'
                              + i
                              + '</button>'
                              + '</div>');
    }
    bank_set_key_on(bank_set);
    bank_key_on(bank);
}


function update_banks() {
    for (let i = 0; i < banks.length; i++) {
        const bank_no = i + 1;
        if (banks[i]) {
            $('#lb_bank_' + bank_no).css({ "color": "orange" });
            $('#lb_bank_' + bank_no).attr('title', banks[i]);
        } else {
            $('#lb_bank_' + bank_no).css({ "color": "lime" });
            $('#lb_bank_' + bank_no).removeAttr('title');
        }
        if (bank == bank_no) {
            $('#lb_bank_' + bank_no).css({ "color": "black", "background-color": "orange" });
        } else {
            $('#lb_bank_' + bank_no).css({ "background-color": "black" });
        }
    }
}


function banks_command_result(r) {
    if (r.command) {
        if (r.command.startsWith("CMD_APP_USE_BANK_SET_")) {
            bank_key_off(bank);
            bank_set = r.command.replace(/^CMD_APP_USE_BANK_SET_/, '');
            for (let i = 1; i <= MAX_BANKS; i++) {
                bank_set_key_off(i);
            }
            bank_set_key_on(bank_set);
            bank = 1;
            bank_key_on(bank);
            banks = r.result;
            update_banks();
            return 1;
        }

        if (r.command.startsWith("CMD_APP_CLEAR_BANK_")) {
            const cleared = r.command.replace(/^CMD_APP_CLEAR_BANK_/, '');
            banks[cleared - 1] = null;
            update_banks();
            return 1;
        }

        if (r.command.startsWith("CMD_APP_STORE_BANK_")) {
            banks = r.result;
            update_banks();
            return 1;
        }

        if (r.command.startsWith("CMD_APP_USE_BANK_")) {
            bank_key_off(bank);
            bank = r.command.replace(/^CMD_APP_USE_BANK_/, '');
            bank_key_on(bank);
            return 1;
        }
    }

    if (r.ui_command) {
        switch (r.ui_command) {
        case "UI_CMD_CONNECT":
            bank_set = r.result.bank_set + 1;
            bank = r.result.bank + 1;
            banks_init();
            ui_command('UI_CMD_APP_GET_BANK_SET', bank_set);
            return 1;

        case "UI_CMD_APP_GET_BANK_SET":
            bank_key_off(bank);
            bank = 1;
            bank_key_on(bank);
            banks = r.result;
            update_banks();
            return 1;

        case "UI_CMD_BANK":
            switch (r.result.command) {
            case "clear":
                banks[r.result.bank - 1] = null;
                $('#lb_bank_' + r.result.bank).removeAttr('title');
                return 1;

            case "store":
                banks[r.result.bank - 1] = r.result.sequence_name;
                $('#lb_bank_' + r.result.bank).attr('title', r.result.sequence_name);
                $('#lb_sequence_name').html(r.result.sequence_name);
                return 1;

            case "use":
                bank_key_off(bank);
                bank = r.result.bank;
                bank_key_on(bank);
                return 1;
            }
        }
    }

    return 0;
}

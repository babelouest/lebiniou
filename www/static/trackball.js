/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

const TEXTURE = "moon.gif";
var moon;


function webGLStart() {
    var pos, $id = function(d) { return document.getElementById(d); };
    
    // Create moon
    moon = new PhiloGL.O3D.Sphere({
        nlat: 30,
        nlong: 30,
        radius: 2,
        textures: '/ui/static/' + TEXTURE
    });

    // Create application
    PhiloGL('lb_trackball_canvas', {
        camera: {
            position: {
                x: 0,
                y: 0,
                z: -7
            }
        },
        textures: {
            src: [ '/ui/static/' + TEXTURE ],
            parameters: [{
                name: 'TEXTURE_MAG_FILTER',
                value: 'LINEAR'
            }, {
                name: 'TEXTURE_MIN_FILTER',
                value: 'LINEAR_MIPMAP_NEAREST',
                generateMipmap: true
            }]
        },
        events: {
            onDragStart: function(e) {
                pos = {
                    x: e.x,
                    y: e.y
                };
                threed_event({ event: "onDragStart", pos: pos });
            },
            onDragMove: function(e) {
                var z = this.camera.position.z,
                    sign = Math.abs(z) / z;

                moon.rotation.y += -(pos.x - e.x) / 100;
                moon.rotation.x += sign * (pos.y - e.y) / 100;
                moon.update();
                pos.x = e.x;
                pos.y = e.y;
                threed_event({ event: "onDragMove", pos: pos, rotation: moon.rotation });
            },
            onMouseWheel: function(e) {
                e.stop();
                var camera = this.camera;
                if ((camera.position.z < -1) || (e.wheel < 0)) {
                    camera.position.z += e.wheel;
                    camera.update();
                    threed_event({ event: "onMouseWheel", wheel: e.wheel, "camera_position": camera.position });
                }
            }
        },
        onError: function() {
            alert("There was an error creating the app.");
        },
        onLoad: function(app) {
            // Unpack app properties
            var gl = app.gl,
                program = app.program,
                scene = app.scene,
                canvas = app.canvas,
                camera = app.camera,

                ambient = {
                    r: 1.0,
                    g: 0.2,
                    b: 0.2
                },
                direction = {
                    x: -1.0,
                    y: -1.0,
                    z: -1.0,
                    
                    r: 0.8,
                    g: 0.8,
                    b: 0.8
                };
            
            // Basic OpenGL setup
            gl.clearColor(0.0, 0.0, 0.0, 1.0);
            gl.clearDepth(1.0);
            gl.enable(gl.DEPTH_TEST);
            gl.depthFunc(gl.LEQUAL);
            gl.viewport(0, 0, +canvas.width, +canvas.height);

            // Add object to the scene
            scene.add(moon);

            // Animate
            draw();

            // Draw the scene
            function draw() {
                gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

                // Setup lighting
                var lights = scene.config.lights;
                lights.enable = true;
                lights.ambient = {
                    r: ambient.r,
                    g: ambient.g,
                    b: ambient.b,
                };
                lights.directional = {
                    color: {
                        r: direction.r,
                        g: direction.g,
                        b: direction.b,
                    },
                    direction: {
                        x: direction.x,
                        y: direction.y,
                        z: direction.z,
                    }
                };

                // Render moon
                scene.render();

                // Request new frame
                PhiloGL.Fx.requestAnimationFrame(prefs.display_threed ? draw : function() { });
            }
        }
    });
}

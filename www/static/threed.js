/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

function threed_command_result(r) {
    // console.log("threed_command_result:", r);
    switch (r.command) {
    case "CMD_APP_NEXT_3D_BOUNDARY":
        set_boundary(r.result.boundary);
        return 1;

    case "CMD_APP_RANDOMIZE_3D_ROTATIONS":
    case "CMD_APP_TOGGLE_3D_ROTATIONS":
        set_rotations(r.result);
        return 1;

    case "CMD_APP_DEC_3D_SCALE":
    case "CMD_APP_INC_3D_SCALE":
        return 1;
    }
    
    switch (r.ui_command) {
    case "UI_CMD_TRACKBALL_ON_DRAG_START":
    case "UI_CMD_TRACKBALL_ON_DRAG_MOVE":
    case "UI_CMD_TRACKBALL_ON_MOUSE_WHEEL":
       return 1;

    case "UI_CMD_CONNECT":
        set_boundary(r.result.params3d.boundary);
        $('#lb_rotation_amount_slider').slider({
            min: 0.0005,
            max: 0.01,
            value: r.result.params3d.rotate_amount,
            step: 0.0001,
            slide: function(event, ui) {
                ui_command('UI_CMD_APP_SET_3D_ROTATION_AMOUNT', ui.value);
            }
        });
        $('#lb_rotation_factor_x_slider').slider({
            min: -r.result.rotation_factor,
            max:  r.result.rotation_factor,
            value: r.result.params3d.rotate_factor[0],
            slide: function(event, ui) {
                ui_command('UI_CMD_APP_SET_3D_ROTATION_FACTOR', { 'axis': 'x', 'value': ui.value });
            }
        });
        $('#lb_rotation_factor_y_slider').slider({
            min: -r.result.rotation_factor,
            max:  r.result.rotation_factor,
            value: r.result.params3d.rotate_factor[1],
            slide: function(event, ui) {
                ui_command('UI_CMD_APP_SET_3D_ROTATION_FACTOR', { 'axis': 'y', 'value': ui.value });
            }
        });
        $('#lb_rotation_factor_z_slider').slider({
            min: -r.result.rotation_factor,
            max:  r.result.rotation_factor,
            value: r.result.params3d.rotate_factor[2],
            slide: function(event, ui) {
                ui_command('UI_CMD_APP_SET_3D_ROTATION_FACTOR', { 'axis': 'z', 'value': ui.value });
            }
        });
        set_rotations(r.result.params3d);
        return 1;

    case "UI_CMD_APP_SET_3D_ROTATION_AMOUNT":
        set_rotations(r.result);
        return 1;

    case "UI_CMD_APP_SET_3D_ROTATION_FACTOR":
        return 1;
    }
    
    return 0;
}


function set_rotations(rotations) {
    var a = rotations.rotate_factor;

    if (a[0] || a[1] || a[2]) {
        $('#lb_rotations_checkbox').prop('checked', true);
    } else {
        $('#lb_rotations_checkbox').prop('checked', false);
    }
    $('#lb_rotation_factor_x_slider').slider({ values: [ a[0] ] });
    $('#lb_rotation_factor_y_slider').slider({ values: [ a[1] ] });
    $('#lb_rotation_factor_z_slider').slider({ values: [ a[2] ] });
    $('#lb_rotation_x').html(a[0]);
    $('#lb_rotation_y').html(a[1]);
    $('#lb_rotation_z').html(a[2]);
    $('#lb_rotation_amount_slider').slider({ values: [ rotations.rotate_amount ] });
    $('#lb_rotation_amount').html(Math.floor(rotations.rotate_amount * 10000) / 10000);
}


function set_boundary(b) {
    switch (b) {
    case "none":
        $('#lb_boundary_button').html('None');
        break;
        
    case "cube":
        $('#lb_boundary_button').html('&#x25a1;');
        break;
        
    case "sphere_dots":
        $('#lb_boundary_button').html('&#x25cc;');
        break;
        
    case "sphere_wireframe":
        $('#lb_boundary_button').html('&#x25cb;');
        break;
    }
}


function threed_event(e) {
    // console.error("threed_event:", e);
    switch (e.event) {
    case "onDragStart":
        ui_command('UI_CMD_TRACKBALL_ON_DRAG_START', e.pos);
        break;

    case "onDragMove":
        ui_command('UI_CMD_TRACKBALL_ON_DRAG_MOVE', { pos: e.pos, rotation: e.rotation });
        break;

    case "onMouseWheel":
        ui_command('UI_CMD_TRACKBALL_ON_MOUSE_WHEEL', { wheel: e.wheel });
        break;
    }
}


function threed_init() {
    // console.log("threed_init");
    $('#lb_trackball_lighting').hide();
    if (PhiloGL.hasWebGL()) {
        if (prefs.display_threed) {
            webGLStart();
        } else {
            $('#lb_threed').hide();
        }
    } else {
        $('#lb_threed_row').hide();
    }
}


function toggle_lighting() {
    $('#lb_trackball_lighting').toggle();
}

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"


int
callback_get_replay(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  Context_t *ctx = user_data;
  json_t *payload = json_array();

  assert(NULL != ctx);
  Context_lock_replay(ctx);
  for (uint8_t i = 0; i < g_queue_get_length(ctx->replay); i++) {
    Sequence_t *s = (Sequence_t *)g_queue_peek_nth(ctx->replay, i);
    json_array_append(payload, Sequence_to_json(s, 1, 1));
  }
  Context_unlock_replay(ctx);
  ulfius_set_json_body_response(response, 200, payload);
  json_decref(payload);

  return U_CALLBACK_COMPLETE;
}

BEGIN {
    print "/*";
    print " *  Copyright 1994-2020 Olivier Girondel";
    print " *";
    print " *  This file is part of lebiniou.";
    print " *";
    print " *  lebiniou is free software: you can redistribute it and/or modify";
    print " *  it under the terms of the GNU General Public License as published by";
    print " *  the Free Software Foundation, either version 2 of the License, or";
    print " *  (at your option) any later version.";
    print " *";
    print " *  lebiniou is distributed in the hope that it will be useful,";
    print " *  but WITHOUT ANY WARRANTY; without even the implied warranty of";
    print " *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the";
    print " *  GNU General Public License for more details.";
    print " *";
    print " *  You should have received a copy of the GNU General Public License";
    print " *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.";
    print " */";
    print;
    print "/*";
    print " * Automagically generated from commands.c.in";
    print " * DO NOT EDIT !!!";
    print " */";
    print;
    print "#include \"bulfius.h\"";
    print;
    print;
    print "static json_t *";
    print "commands()";
    print "{";
    print "  json_t *commands = json_object();";
    print;
}


{
    if (($1 == "#") || ($0 == "") || ($1 == "*") || ($1 == "-") || ($1 == "**"))
	next;
    else {
        tail = substr($0, (length($1 $2 $3 $4) + 5));

        printf "  json_object_set_new(commands, \"%s\", json_string(\"%s\"));\n", $4, tail;
    }
}


END {
    print "";
    print "  return commands;"
    print "}";
    print "";
    print "";
    print "int";
    print "callback_get_commands(const struct _u_request *request, struct _u_response *response, void * user_data)";
    print "{";
    print "  json_t *res = commands();";
    print "";
    print "  ulfius_set_json_body_response(response, 200, res);";
    print "  json_decref(res);";
    print "";
    print "  return U_CALLBACK_COMPLETE;";
    print "}";
    print "";
    print "";
}

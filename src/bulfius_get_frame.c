/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"


int
callback_get_frame(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  Context_t *ctx = user_data;
  uint8_t *png = NULL;
  uint32_t png_datalen;
  long width = 0;
  long height = 0;
  const char *w = u_map_get(request->map_url, "w");
  const char *h = u_map_get(request->map_url, "h");

  assert(NULL != ctx);
  if ((NULL != w) && (NULL != h)) {
    width = xatol(w);
    height = xatol(h);

    // maximum rescale to HD
    if ((width > 0) && (height > 0) && (width <= 1920) && (height <= 1080)) {
      Context_to_PNG(ctx, &png, &png_datalen, width, height);
    } else {
      ulfius_set_string_body_response(response, 400, "Bad request");

      return U_CALLBACK_COMPLETE;
    }
  } else {
    Context_to_PNG(ctx, &png, &png_datalen, width, height);
  }
  ulfius_set_binary_body_response(response, 200, (const char *)png, png_datalen);
  xfree(png);
  u_map_put(response->map_header, "Content-Type", "image/png");

  return U_CALLBACK_COMPLETE;
}

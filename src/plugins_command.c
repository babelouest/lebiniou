/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"

/* Number of plugins to skip when using page up/down */
#define SKIP 10


static json_t *
Plugins_command_result(Context_t *ctx)
{
  json_t *res = json_object();

  json_object_set_new(res, "selected_plugin", json_string(plugins->selected->name));
  json_object_set_new(res, "selected_plugin_dname", json_string(plugins->selected->dname));
  if (NULL != plugins->selected->parameters) {
    json_object_set_new(res, "selected_param", json_integer(plugins->selected->selected_param));
    json_object_set_new(res, "parameters", plugins->selected->parameters(ctx, NULL));
  }

  return res;
}


json_t *
Plugins_command(Context_t *ctx, Plugins_t *plugins, const enum Command cmd)
{
  json_t *res = NULL;

  switch (cmd) {
    case CMD_PLG_NEXT:
#ifdef DEBUG_COMMANDS
      printf(">>> CMD_PLG_NEXT\n");
#endif
      Plugins_next();
      res =  Plugins_command_result(ctx);
      break;

    case CMD_PLG_PREVIOUS:
#ifdef DEBUG_COMMANDS
      printf(">>> CMD_PLG_PREVIOUS\n");
#endif
      Plugins_prev();
      res = Plugins_command_result(ctx);
      break;

#if 0
    case CMD_PLG_RELOAD_SELECTED:
#ifdef DEBUG_COMMANDS
      printf(">>> CMD_PLG_RELOAD_SELECTED\n");
#endif
      Plugins_reload_selected(plugins);
      break;
#endif

    case CMD_PLG_SCROLL_DOWN:
#ifdef DEBUG_COMMANDS
      printf(">>> CMD_PLG_SCROLL_DOWN\n");
#endif
      Plugins_next_n(SKIP);
      res = Plugins_command_result(ctx);
      break;

    case CMD_PLG_SCROLL_UP:
#ifdef DEBUG_COMMANDS
      printf(">>> CMD_PLG_SCROLL_UP\n");
#endif
      Plugins_prev_n(SKIP);
      res =  Plugins_command_result(ctx);
      break;

    default:
      xerror("Unhandled plugins command %d\n", cmd);
      break;
  }

  return res;
}

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "constants.h"
#include "brandom.h"
#include "imagefader.h"
#include "images.h"
#include "globals.h"


ImageFader_t *
ImageFader_new(const uint16_t size)
{
  ImageFader_t *pf = xcalloc(1, sizeof(ImageFader_t));

  pf->on = 0;
  pf->cur = Image8_new();
  if (NULL != images) {
    pf->dst = images->imgs[0];
  }
  pf->fader = Fader_new(BUFFSIZE);
  pf->shf = Shuffler_new(size);
  Shuffler_set_mode(pf->shf, Context_get_shuffler_mode(BD_IMAGES));

  ImageFader_set(pf);

  return pf;
}


void
ImageFader_delete(ImageFader_t *pf)
{
  Image8_delete(pf->cur);
  Fader_delete(pf->fader);
  Shuffler_delete(pf->shf);
  xfree(pf);
}


void
ImageFader_init(ImageFader_t *pf)
{
  Fader_t *fader = pf->fader;
  uint32_t i;
  const Buffer8_t *src = pf->cur->buff;
  const Buffer8_t *dst = pf->dst->buff;

  Fader_init(fader);
  for (i = BUFFSIZE; i--; ) { /* i < BUFFSIZE; i++) { */
    /* delta values for fading */
    fader->delta[i] = (long)(
                        ((float)dst->buffer[i] - (float)src->buffer[i])
                        / (float)(fader->max)
                        * MFACTOR);

    /* initial values for fading */
    /* fader->tmp[i] = (float)src->buffer[i]; */
    fader->tmp[i] = (uint32_t)src->buffer[i]*MFACTOR;
  }
  Fader_start(fader);
}


void
ImageFader_run(ImageFader_t *pf)
{
  Fader_t *fader = pf->fader;
  const uint32_t elapsed = Fader_elapsed(fader);

#ifdef DEBUG_FADERS
  printf("If ");
#endif
  Fader_start(fader);
  fader->faded += elapsed;

  if (fader->faded < fader->max) {
    /* now do some fading stuff #~{@ */
    /* we spent (elapsed) msecs */
    Pixel_t *ptr = pf->cur->buff->buffer;
    /*     float   *tmp = fader->tmp; */
    /*     float   *delta = fader->delta; */
    uint32_t   *tmp = fader->tmp;
    long   *delta = fader->delta;
    uint32_t i;

    for (i = BUFFSIZE; i--; ptr++, tmp++, delta++)
      /**ptr = (Pixel_t)(*tmp += (elapsed * *delta));*/
    {
      *ptr = (Pixel_t)((*tmp += (elapsed * *delta))/MFACTOR);
    }
  } else {
    /* we're done */
    fader->fading = 0;
    /* copy, just in case */
    Image8_copy(pf->dst, pf->cur);
  }
}


void
ImageFader_set(ImageFader_t *pf)
{
  if (NULL != pf) {
    Fader_t *fader = pf->fader;

    pf->dst = images->imgs[fader->target];

    if (NULL != pf->dst->name) {
#ifdef DEBUG_IMAGEFADER
      VERBOSE(printf("[i] Using image '%s'\n", pf->dst->name));
#endif
    } else {
      xerror("Image without name, WTF #@!\n");
    }

    ImageFader_init(pf);
    fader->fading = 1;
  }
}


void
ImageFader_prev(ImageFader_t *pf)
{
  if (NULL != pf) {
    DEC(pf->fader->target, images->size);
    ImageFader_set(pf);
  }
}


void
ImageFader_prev_n(ImageFader_t *pf, const uint16_t n)
{
  if (NULL != pf) {
    for (uint16_t i = 0; i < n; i++) {
      DEC(pf->fader->target, images->size);
    }
    ImageFader_set(pf);
  }
}


void
ImageFader_next(ImageFader_t *pf)
{
  if (NULL != pf) {
    INC(pf->fader->target, images->size);
    ImageFader_set(pf);
  }
}


void
ImageFader_next_n(ImageFader_t *pf, const uint16_t n)
{
  if (NULL != pf) {
    for (uint16_t i = 0; i < n; i++) {
      INC(pf->fader->target, images->size);
    }
    ImageFader_set(pf);
  }
}


void
ImageFader_random(ImageFader_t *pf)
{
  if (NULL != pf) {
    pf->fader->target = Shuffler_get(pf->shf);
    ImageFader_set(pf);
  }
}


int
ImageFader_ring(const ImageFader_t *pf)
{
  if (NULL != pf) {
    const Fader_t *fader = pf->fader;

    return (fader->fading && ((b_timer_elapsed(fader->timer) * MFACTOR) > 0));
  } else {
    return 0;
  }
}

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "globals.h"
#include "colormaps.h"
#include "images.h"


extern uint64_t frames;


int
callback_get_statistics(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  const Context_t *ctx = user_data;
  json_t *payload = json_object();

  assert(NULL != ctx);
  json_object_set_new(payload, "frames", json_integer(frames));
  json_object_set_new(payload, "uptime", json_integer(b_timer_elapsed(ctx->timer)));
  json_object_set_new(payload, "plugins", json_integer(plugins->size));
  json_object_set_new(payload, "colormaps", json_integer(colormaps->size));
  json_object_set_new(payload, "images", json_integer(images->size));
  json_object_set_new(payload, "sequences", json_integer(sequences->size));
  json_object_set_new(payload, "schemes", json_integer(schemes->size));

  ulfius_set_json_body_response(response, 200, payload);
  json_decref(payload);

  return U_CALLBACK_COMPLETE;
}

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "pnglite.h"

#define DIRECTORY      "/screenshots/"


static char *
screenshot_filename()
{
  gchar *blah = NULL;
  const gchar *home_dir = NULL;
  time_t s;
  struct tm *now;

  s = time(NULL);
  now = localtime(&s);
  home_dir = g_get_home_dir();
  blah = g_strdup_printf("%s/." PACKAGE_NAME DIRECTORY, home_dir);
  rmkdir(blah);
  g_free(blah);

  blah = g_strdup_printf("%s/." PACKAGE_NAME DIRECTORY PACKAGE_NAME "-%04d-%02d-%02d_%02d-%02d-%02d.png",
                         home_dir, now->tm_year + 1900, now->tm_mon + 1, now->tm_mday,
                         now->tm_hour, now->tm_min, now->tm_sec);

  VERBOSE(printf("[i] %s: saving screenshot to %s\n", __FILE__, blah));

  return blah;
}


void
write_png(const char *filename, const Pixel_t *data)
{
  png_t png;
  int res;

  png_init((png_alloc_t)0, (png_free_t)0);
  res = png_open_file_write(&png, filename);
  if (PNG_NO_ERROR == res) {
    res = png_set_data(&png, WIDTH, HEIGHT, 8, PNG_TRUECOLOR, (uint8_t *)data);
    if (PNG_NO_ERROR != res) {
      fprintf(stderr, "[!] png_set_data: %s (%s)\n", png_error_string(res), filename);
    }
  } else {
    fprintf(stderr, "[!] png_open_file_write: %s (%s)\n", png_error_string(res), filename);
    if (PNG_FILE_ERROR != res) {
      png_close_file(&png);
    }
    return;
  }
  png_close_file(&png);
}


void
Context_screenshot(const Context_t *ctx)
{
  char *filename = NULL;
  Pixel_t *data = NULL;

  filename = screenshot_filename();
  data = export_RGB_active_buffer(ctx, 1);
  write_png(filename, data);

  xfree(data);
  g_free(filename);
}

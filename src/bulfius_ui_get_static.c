/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include "bulfius.h"
#include "utils.h"
#include "defaults.h"


#define STATIC_FILE_CHUNK 256


static const char *
content_type_from_ext(const char *ext)
{
  if (is_equal(ext, ".html")) {
    return "text/html";
  } else if (is_equal(ext, ".css")) {
    return "text/css";
  } else if (is_equal(ext, ".js")) {
    return "application/javascript";
  } else if (is_equal(ext, ".png")) {
    return "image/png";
  } else if (is_equal(ext, ".jpg") || is_equal(ext, ".jpeg")) {
    return "image/jpeg";
  } else if (is_equal(ext, ".ico")) {
    return "image/x-icon";
  }

  return "application/octet-stream";
}


/**
 * Return the filename extension
 */
const char *
get_filename_ext(const char *path)
{
  const char *dot = strrchr(path, '.');

  if ((NULL == dot) || (dot == path)) {
    return "*";
  }
  if (NULL != strchr(dot, '?')) {
    *strchr(dot, '?') = '\0';
  }

  return dot;
}


/**
 * Streaming callback function to ease sending large files
 */
static ssize_t
callback_static_file_stream(void * cls, uint64_t pos, char * buf, size_t max)
{
  if (NULL != cls) {
    return fread(buf, 1, max, (FILE *)cls);
  } else {
    return U_STREAM_END;
  }
}


/**
 * Cleanup FILE* structure when streaming is complete
 */
static void
callback_static_file_stream_free(void * cls)
{
  if (NULL != cls) {
    fclose((FILE *)cls);
  }
}


/**
 * static file callback endpoint
 */
int
callback_ui_get_static(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  size_t length;
  FILE *f;
  char *file_path = NULL;
  const char *content_type;

  if (NULL != user_data) {
    if (getenv("DEV_WEB_UI")) {
      file_path = g_strdup_printf("./%s", (char *)user_data);
    } else {
      // FIXME le "/../"
      file_path = g_strdup_printf("%s/../%s", DEFAULT_WWWDIR, (char *)user_data);
    }
  } else if (NULL != u_map_get(request->map_url, "file")) {
    if (getenv("DEV_WEB_UI")) {
      file_path = g_strdup_printf("./www/static/%s", u_map_get(request->map_url, "file"));
    } else {
      file_path = g_strdup_printf("%s/static/%s", DEFAULT_WWWDIR, u_map_get(request->map_url, "file"));
    }
  } else {
    ulfius_set_string_body_response(response, 404, "File not found");

    return U_CALLBACK_COMPLETE;
  }
#ifdef DEBUG_BULFIUS_GET
  printf("=== %s: file_path= %s\n", __func__, file_path);
#endif

  if ((NULL != file_path) && (access(file_path, F_OK) != -1)) {
    f = fopen (file_path, "rb");
    if (NULL != f) {
      fseek(f, 0, SEEK_END);
      length = ftell(f);
      fseek(f, 0, SEEK_SET);

      content_type = content_type_from_ext(get_filename_ext(file_path));
      u_map_put(response->map_header, "Content-Type", content_type);

      if (ulfius_set_stream_response(response, 200, callback_static_file_stream, callback_static_file_stream_free, length, STATIC_FILE_CHUNK, f) != U_OK) {
        printf("[!] %s: callback_ui_get_static - Error ulfius_set_stream_response\n", __FILE__);
      }
      xfree(file_path);

      return U_CALLBACK_COMPLETE;
    }
  }

  return U_CALLBACK_ERROR;
}

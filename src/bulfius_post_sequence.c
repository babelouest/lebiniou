/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "plugins.h"


int
callback_post_sequence(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  // XXX need more error checking ?

  Context_t *ctx = user_data;

  assert(NULL != ctx);
#ifdef DEBUG_BULFIUS_POST
  printf("[U] POST sequence\n");
#endif

  json_error_t jerror;
  // parse request body
  json_t *body = ulfius_get_json_body_request(request, &jerror);

  if (NULL == body) {
    // error
    VERBOSE(printf("[!] %s:%d JSON error: %s\n", __FILE__, __LINE__, jerror.text));
  } else {
#ifdef DEBUG_BULFIUS_POST
    char *b = json_dump(body, 0);
    printf("[U] POST body: %s\n", b);
    xfree(b);
#endif
    json_t *j_body = json_object_get(body, "body");
    const char *pb = json_string_value(j_body);
    json_t *parsed_body = json_loads(pb, 0, &jerror);

    if (NULL == parsed_body) {
      VERBOSE(printf("[!] %s:%d JSON error: %s\n", __FILE__, __LINE__, jerror.text));
    } else {
#ifdef DEBUG_BULFIUS_POST
      char *b = json_dump(parsed_body, 0);
      printf("[U] POST parsed_body: %s\n", b);
      xfree(b);
#endif
      Sequence_t *seq = Sequence_from_json(parsed_body);
      Sequence_display(seq);

      gchar *buff = g_strdup_printf("Sequence %d uploaded\n", seq->id);
      ulfius_set_string_body_response(response, 200, buff);
#ifdef DEBUG_BULFIUS_POST
      printf("[U] POST sequence: id= %d, response: %s", seq->id, buff);
#endif
      g_free(buff);
      Sequence_copy(ctx, seq, ctx->sm->next);
      Context_set(ctx);
      Sequence_delete(seq);
    }
    json_decref(j_body);
  }

  return U_CALLBACK_COMPLETE;
}

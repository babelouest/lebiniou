/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "btimer.h"
#include "utils.h"

static enum BTimerMode b_timer_mode = BT_HARD;

#define GETTIME(v) do { gettimeofday(&v, NULL); } while (0)

extern uint64_t frames;
extern uint8_t  max_fps;


BTimer_t *
b_timer_new(const char *name)
{
  BTimer_t *timer = xcalloc(1, sizeof(BTimer_t));

#ifdef DEBUG_TIMERS
  timer->name = name;
#else
  (void)name;
#endif
  b_timer_start(timer);

  return timer;
}


void
b_timer_delete(BTimer_t *timer)
{
  xfree(timer);
}


void
b_timer_start(BTimer_t *timer)
{
  GETTIME(timer->h_start);
  timer->s_start = frames;
}


float
b_timer_elapsed(const BTimer_t *timer)
{
  float ret;

  if (b_timer_mode == BT_HARD) {
    struct timeval now, elapsed;

    GETTIME(now);
    if (timer->h_start.tv_usec > now.tv_usec) {
      now.tv_usec += 1000000;
      now.tv_sec--;
    }
    elapsed.tv_usec = now.tv_usec - timer->h_start.tv_usec;
    elapsed.tv_sec = now.tv_sec - timer->h_start.tv_sec;

    ret = (float)(elapsed.tv_sec + ((float)elapsed.tv_usec / 1e6));
  } else {
    ret = (frames - timer->s_start) / (float)max_fps;
  }

#ifdef DEBUG_TIMERS
  printf("=== %s timer (%s) elapsed: %f\n", (b_timer_mode == BT_HARD) ? "Hard" : "Soft", timer->name, ret);
#endif

  return ret;
}


void
b_timer_set_mode(const enum BTimerMode mode)
{
  b_timer_mode = mode;
}

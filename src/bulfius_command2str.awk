BEGIN {
    print "/*";
    print " *  Copyright 1994-2020 Olivier Girondel";
    print " *";
    print " *  This file is part of lebiniou.";
    print " *";
    print " *  lebiniou is free software: you can redistribute it and/or modify";
    print " *  it under the terms of the GNU General Public License as published by";
    print " *  the Free Software Foundation, either version 2 of the License, or";
    print " *  (at your option) any later version.";
    print " *";
    print " *  lebiniou is distributed in the hope that it will be useful,";
    print " *  but WITHOUT ANY WARRANTY; without even the implied warranty of";
    print " *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the";
    print " *  GNU General Public License for more details.";
    print " *";
    print " *  You should have received a copy of the GNU General Public License";
    print " *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.";
    print " */";
    print;
    print "/*";
    print " * Automagically generated from commands.c.in";
    print " * DO NOT EDIT !!!";
    print " */";
    print;
    print "#include <string.h>";
    print "#include \"commands.h\"";
    print;
    print;
    print "const char *";
    print "command2str(const enum Command cmd)";
    print "{";
}


{
    if (($1 == "#") || ($0 == "") || ($1 == "*") || ($1 == "-") || ($1 == "**"))
	next;
    else {
        printf "  if (cmd == %s) { return \"%s\"; }\n", $4, $4;
    }
}


END {
    print "";
    print "  return NULL;"
    print "}";
}

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_SCHEMES_H
#define __BINIOU_SCHEMES_H

#include "shuffler.h"
#include "context.h"


typedef struct SchemeItem_s {
  double             p;            // probability to add the plugin (default: 1.0)
  uint8_t            mandatory;    // 1 if proba is 1.0
  uint8_t            disable_lens; // do not set this plugin as lens if added
  enum PluginOptions options;      // or-ed list of options that a plugin must have to be added
} SchemeItem_t;


typedef struct Schemes_s {
  SchemeItem_t **schemes;
  uint16_t     size;
  Shuffler_t   *shuffler;
} Schemes_t;


void Schemes_new(const char *);
void Schemes_delete();

void Schemes_random(Context_t *);

enum PluginOptions Schemes_str2option(const char *);

#endif /* __BINIOU_SCHEMES_H */

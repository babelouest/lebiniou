/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.h"
#include "webcam.h"


#ifdef WITH_WEBCAM
extern int hflip, vflip, webcams;
extern char *video_base;
#endif
extern uint8_t  statistics;
extern uint16_t http_port;
extern uint8_t  statistics;
extern uint8_t  max_fps;
extern uint8_t  use_hard_timers;
extern uint8_t  encoding;
extern char    *playlist_filename;
extern char    *audio_file;
extern uint8_t  enable_osd;
#ifdef WITH_ULFIUS
extern uint8_t  usage_statistics;
#endif


#define MIN_WIDTH  80
#define MIN_HEIGHT 60


static void
usage()
{
  printf("Usage: " PACKAGE_NAME " [options]\n\n"
         "\t-a, --audio <file>\tSet audio file (sndfile plugin)\n"
         "\t-b, --basedir <path>\tSet base directory [" DEFAULT_PLUGINSDIR "]\n"
         "\t-c, --config <file>\tSet configuration file [~/" KEYFILE "]\n"
         "\t-d, --datadir <path>\tSet data directory [" DEFAULT_DATADIR "]\n"
         "\t-f, --fullscreen\tSet fullscreen\n"
         "\t-h, --help\t\tDisplay this help\n"
         "\t-i, --input <plugin>\tSet input plugin [" DEFAULT_INPUT_PLUGIN "]\n"
         "\t-m, --max-fps <fps>\tSet maximum framerate [%d]\n"
         "\t-n, --no-borders\tDisable window borders\n"
         "\t-o, --output <plugin>\tSet output plugin [" DEFAULT_OUTPUT_PLUGIN "]\n"
         "\t-p, --pidfile <file>\tSet the PID file [%s]\n"
         "\t-q, --quiet\t\tSuppress messages\n"
         "\t-r, --random <mode>\tSet auto-random mode\n"
         "\t-s, --soft-timers\tUse soft timers instead of the system clock\n"
         "\t-t, --themes <themes>\tComma-separated list of themes to use [biniou]\n"
         "\t-v, --version\t\tDisplay the version and exit\n"
#ifndef FIXED
         "\t-x, --width <width>\tSet width [%d]\n"
         "\t-y, --height <height>\tSet height [%d]\n"
#endif
         "\t-z, --schemes <file>\tSet the schemes file [" DEFAULT_SCHEMES_FILE "]\n"
#ifdef WITH_WEBCAM
         "\t-C, --cam-flip <h|v>\tFlip webcam image horizontally/vertically\n"
         "\t-D, --device <device>\tWebcam base [" DEFAULT_VIDEO_DEVICE "]\n"
#endif
         "\t-E, --start-encoding\tStart encoding directly\n"
         "\t-F, --fade <delay>\tSet fade delay (in seconds)\n"
#ifdef WITH_ULFIUS
         "\t-G, --enable-sdl2-gui\tEnable the legacy SDL2 GUI\n"
         "\t-N, --no-statistics\tDo not send usage statistics\n"
#endif
         "\t-P, --playlist <file>\tSet playlist file\n"
         "\t-S, --stats\t\tDisplay statistics\n"
#ifdef WITH_WEBCAM
         "\t-W, --webcams <webcams>\tNumber of webcams [%d]\n"
#endif
         "\t-X, --xpos <x position>\tSet left position of the window\n"
         "\t-Y, --ypos <y position>\tSet top position of the window\n"
         "\n"
         "This version of " PACKAGE_STRING " was compiled with:\n"
         "\tInput plugins: " INPUT_PLUGINS "\n"
         "\tOutput plugins: " OUTPUT_PLUGINS "\n"
         "\n"
         , max_fps
         , pid_file
#ifndef FIXED
         , DEFAULT_WIDTH
         , DEFAULT_HEIGHT
#endif
#ifdef WITH_WEBCAM
         , webcams
#endif
        );

  /* TODO print default values for all options */
  exit(0);
}


void
getargs(int argc, char **argv)
{
  int ch;
#ifndef FIXED
  int w, h;
#endif

  static const char *arg_list = "a:b:c:C:d:D:EfF:Ghi:lm:nNo:p:P:qr:sSt:x:X:y:Y:vw:W:z:";

#if HAVE_GETOPT_LONG
  static struct option long_opt[] = {
    {"audio", required_argument, NULL, 'a'},
    {"basedir", required_argument, NULL, 'b'},
    {"config", required_argument, NULL, 'c'},
    {"datadir", required_argument, NULL, 'd'},
    {"fullscreen", no_argument, NULL, 'f'},
    {"help", no_argument, NULL, 'h'},
    {"http-port", required_argument, NULL, 'w'},
    {"input", required_argument, NULL, 'i'},
    {"max-fps", required_argument, NULL, 'm'},
    {"no-borders", no_argument, NULL, 'n'},
    {"output", required_argument, NULL, 'o'},
    {"pidfile", required_argument, NULL, 'p'},
    {"quiet", no_argument, NULL, 'q'},
    {"random", required_argument, NULL, 'r'},
    {"soft-timers", no_argument, NULL, 's'},
    {"start-encoding", no_argument, NULL, 'E'},
    {"themes", required_argument, NULL, 't'},
    {"version", no_argument, NULL, 'v'},
#ifndef FIXED
    {"width", required_argument, NULL, 'x'},
    {"height", required_argument, NULL, 'y'},
#endif
    {"schemes", required_argument, NULL, 'z'},
#ifdef WITH_WEBCAM
    {"cam-flip", required_argument, NULL, 'C'},
    {"device", required_argument, NULL, 'D'},
#endif
    {"fade", required_argument, NULL, 'F'},
#ifdef WITH_ULFIUS
    {"enable-sdl2-gui", no_argument, NULL, 'G'},
    {"no-statistics", no_argument, NULL, 'N'},
#endif
    {"playlist", required_argument, NULL, 'P'},
    {"stats", no_argument, NULL, 'S'},
#ifdef WITH_WEBCAM
    {"webcams", required_argument, NULL, 'W'},
#endif
    {"xpos", required_argument, NULL, 'X'},
    {"ypos", required_argument, NULL, 'Y'},

    {0, 0, 0, 0}
  };


  /* Get command line arguments */
  while ((ch = getopt_long(argc, argv, arg_list, long_opt, NULL)) != -1)
#else
  while ((ch = getopt(argc, argv, arg_list)) != -1)
#endif
    switch (ch) {
      case 'a':
        audio_file = optarg;
        break;

      case 'b':
        if (NULL == base_dir) {
          base_dir = optarg;
        }
        break;

      case 'c':
        /* re-read configuration file */
        set_configuration(optarg);
        read_keyfile();
        break;

      case 'd':
        if (NULL == data_dir) {
          data_dir = optarg;
        }
        break;

      case 'E':
        encoding = 1;
        break;

      case 'w':
        http_port = xatol(optarg);
        if (http_port && (http_port < 1024)) {
          xerror("HTTP API port must be >= 1024, or 0 to disable\n");
        } else {
          if (http_port) {
            printf("[c] Setting HTTP_API port to %d\n", http_port);
	  } else {
            printf("[c] Disabling HTTP_API\n");
	  }
        }
        break;

      case 'z':
        if (NULL == schemes_file) {
          schemes_file = optarg;
        }
        break;

      case 'f':
        fullscreen = 1;
        break;

      case 'h':
        usage();
        break;

      case 'n':
        window_decorations = 0;
        VERBOSE(printf("[c] Deactivate window decorations\n"));
        break;

      case 'i':
        if (NULL != input_plugin) {
          xfree(input_plugin);
        }
        input_plugin = optarg;
        VERBOSE(printf("[c] Setting input plugin: %s\n", input_plugin));
        break;

      case 'o':
        if (NULL != output_plugin) {
          xfree(output_plugin);
        }
        output_plugin = optarg;
        VERBOSE(printf("[c] Setting output plugin: %s\n", output_plugin));
        break;

      case 'm':
        max_fps = xatol(optarg);
        if (max_fps > 0) {
          VERBOSE(printf("[c] Maximum fps set to %i\n", max_fps));
        } else {
          xerror("Invalid max_fps (%d)\n", max_fps);
        }
        break;

      case 'r':
	if (optarg[0] != '-') {
          random_mode = (enum RandomMode)xatol(optarg);
          if (random_mode > BR_BOTH) {
            xerror("Invalid random mode (%d)\n", random_mode);
          } else {
            VERBOSE(printf("[c] Random mode set to %d\n", random_mode));
          }
	} else {
	  xerror("Random mode must be in [0..3]\n");
	}
        break;

      case 't':
        if (NULL != themes) {
          xfree(themes);
        }
        themes = strdup(optarg);
        VERBOSE(printf("[c] Using themes: %s\n", themes));
        break;

      case 'v':
        printf("%s %s\n", PACKAGE_NAME, PACKAGE_VERSION);
        exit(0);
        break;

      case 'q':
        libbiniou_verbose = 0;
        break;

      case 'x':
#ifndef FIXED
        w = xatol(optarg);
        if (w >= MIN_WIDTH) {
          width = w;
          VERBOSE(printf("[c] Width set to %i\n", width));
        } else {
          xerror("Invalid width: %d (min: %d)\n", w, MIN_WIDTH);
        }
#else
        VERBOSE(fprintf(stderr, "[!] Compiled with fixed buffers, ignoring width= %li\n", xatol(optarg)));
#endif
        break;

      case 'X':
        x_origin = xatol(optarg);
        VERBOSE(printf("[c] X origin set to %i\n", x_origin));
        break;

      case 'y':
#ifndef FIXED
        h = xatol(optarg);
        if (h >= MIN_HEIGHT) {
          height = h;
          VERBOSE(printf("[c] Height set to %i\n", height));
        } else {
          xerror("Invalid height: %d (min: %d)\n", h, MIN_HEIGHT);
        }
#else
        VERBOSE(fprintf(stderr, "[!] Compiled with fixed buffers, ignoring height= %li\n", xatol(optarg)));
#endif
        break;

      case 'Y':
        y_origin = xatol(optarg);
        VERBOSE(printf("[c] Y origin set to %i\n", y_origin));
        break;

      case 'p':
        if (NULL == pid_file) {
          pid_file = optarg;
        }
        break;

#ifdef WITH_WEBCAM
      case 'C':
        if (*optarg == 'h') {
          hflip = !hflip;
        }
        if (*optarg == 'v') {
          vflip = !vflip;
        }
        break;

      case 'W': /* webcams */
        webcams = xatol(optarg);
        if ((webcams >= 0) && (webcams <= MAX_CAMS)) {
          VERBOSE(printf("[c] webcam: grabbing %d device%s\n", webcams, (webcams == 1 ? "": "s")));
        } else {
          webcams = 1;
        }
        break;

      case 'D': /* video_base */
        if (NULL != video_base) {
          xfree(video_base);
        }
        video_base = strdup(optarg);
        VERBOSE(printf("[c] webcam: first device is %s\n", video_base));
        break;
#endif

      case 's':
        use_hard_timers = 0;
        VERBOSE(printf("[c] Using %s timers\n", use_hard_timers ? "hard" : "soft"));
        break;

      case 'P':
        playlist_filename = optarg;
        VERBOSE(printf("[c] Playlist: %s\n", playlist_filename));
        break;

      case 'S':
        libbiniou_verbose = 0;
        statistics = 1;
        break;

      case 'F':
        fade_delay = xatof(optarg);
        VERBOSE(printf("[c] Fading delay set to %f\n", fade_delay));
        break;

#ifdef WITH_ULFIUS
      case 'G':
        enable_osd = 1;
        VERBOSE(printf("[c] Enabling legacy SDL2 GUI\n"));
        break;

      case 'N':
        usage_statistics = 0;
        VERBOSE(printf("[c] Not sending statistics usage\n"));
        break;
#endif

      default:
        usage();
        break;
    }

  if (NULL == base_dir) {
    base_dir = DEFAULT_PLUGINSDIR;
  }

  if (NULL == data_dir) {
    data_dir = DEFAULT_DATADIR;
  }

  if (NULL == schemes_file) {
    schemes_file = DEFAULT_SCHEMES_FILE;
  }

  if (NULL == input_plugin) {
    input_plugin = DEFAULT_INPUT_PLUGIN;
  } else if (is_equal(input_plugin, "NULL")) {
    input_plugin = NULL;
  }

  if (NULL == output_plugin) {
    output_plugin = DEFAULT_OUTPUT_PLUGIN;
  } else if (is_equal(output_plugin, "NULL")) {
    output_plugin = NULL;
  }

  if (NULL == themes) {
    themes = strdup("biniou");
  }

#ifdef WITH_WEBCAM
  if (NULL == video_base) {
    video_base = strdup(DEFAULT_VIDEO_DEVICE);
  }
#endif
}

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


void
Context_add_replay(Context_t *ctx, const Sequence_t *seq)
{
  guint length;

  Context_lock_replay(ctx);
  length = g_queue_get_length(ctx->replay);

#ifdef DEBUG_REPLAY
  printf("add_replay(%p): old length: %d\n", seq, length);
#endif
  if (length == REPLAY_SIZE) {
    Sequence_t *s = (Sequence_t *)g_queue_pop_tail(ctx->replay);
    assert(NULL != s);
#ifdef DEBUG_REPLAY
    printf("add_replay(%p): old length: %d\n", seq, length);
    printf("add_replay(%p): delete id= %d\n", seq, seq->id);
#endif
    Sequence_delete(s);
  }

  g_queue_push_head(ctx->replay, (gpointer)seq);
#ifdef DEBUG_REPLAY
  printf("add_replay(%p): new length: %d\n", seq, g_queue_get_length(ctx->replay));
#endif
  Context_unlock_replay(ctx);
}


void
Context_init_replay(Context_t *ctx)
{
  ctx->replay = g_queue_new();
  pthread_mutex_init(&ctx->replay_lock, NULL);
}


static void
delete_replay(gpointer data)
{
  Sequence_t *seq = (Sequence_t *)data;

  assert(NULL != seq);
#ifdef DEBUG_REPLAY
  printf("delete_replay(%p): id= %d\n", data, seq->id);
#endif
  Sequence_delete(seq);
}


void
Context_free_replay(Context_t *ctx)
{
#ifdef DEBUG_REPLAY
  assert(NULL != ctx->replay);
#endif
  g_queue_free_full(ctx->replay, delete_replay);
}


void
Context_lock_replay(Context_t *ctx)
{
  pthread_mutex_lock(&ctx->replay_lock);
}


void
Context_unlock_replay(Context_t *ctx)
{
  pthread_mutex_unlock(&ctx->replay_lock);
}

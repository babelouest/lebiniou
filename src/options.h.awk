BEGIN {
    print "/*";
    print " *  Copyright 1994-2020 Olivier Girondel";
    print " *";
    print " *  This file is part of lebiniou.";
    print " *";
    print " *  lebiniou is free software: you can redistribute it and/or modify";
    print " *  it under the terms of the GNU General Public License as published by";
    print " *  the Free Software Foundation, either version 2 of the License, or";
    print " *  (at your option) any later version.";
    print " *";
    print " *  lebiniou is distributed in the hope that it will be useful,";
    print " *  but WITHOUT ANY WARRANTY; without even the implied warranty of";
    print " *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the";
    print " *  GNU General Public License for more details.";
    print " *";
    print " *  You should have received a copy of the GNU General Public License";
    print " *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.";
    print " */";
    print;
    print "#ifndef __BINIOU_OPTIONS_H";
    print "#define __BINIOU_OPTIONS_H";
    print;
    print "#include \"utils.h\"";
    print;
    print "/*";
    print " * Automagically generated from options.h.in";
    print " * DO NOT EDIT !!!";
    print " */";
    print;
    print;
    print "enum PluginOptions {";
    print "  BO_NONE = 0,";

    i = 1;
}


{
    tail = substr($0, (length($1) + 2));

    if (tail != "") {
        printf "  // %s\n", tail;
        printf "  %s = 1 << %d,\n", $1, i;
    } else
        printf "  %s = 1 << %d,\n", $1, i;

    i = i + 1;
}


END {
    print "};";
    print "";
    print "";
    printf "#define MAX_OPTIONS %d\n", i;
    print "";
    print "typedef struct PluginType_s {";
    print "  enum PluginOptions option;";
    print "  uint16_t count;";
    print "} PluginType_t;"
    print "";
    print "";
    print "#endif /* __BINIOU_OPTIONS_H */";
}

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BRANDOM_H
#define _BRANDOM_H

#include "utils.h"

void b_rand_init(void);
void b_rand_free(void);

uint32_t b_rand_int(void);
int32_t  b_rand_int32_range(const int32_t begin, const int32_t end);
uint32_t b_rand_uint32_range(const uint32_t begin, const uint32_t end);
double b_rand_double_range(const double begin, const double end);
int b_rand_boolean(void);
void b_rand_set_seed(const uint32_t seed);

#endif /* _BRANDOM_H */


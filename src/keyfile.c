/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.h"

/* check doc/lebiniourc.in for the complete list of options */

/* configuration file */
gchar *keyfile = NULL;

/* groups */
#define PLUGINS  "Plugins"
#define SCREEN   "Screen"
#define ENGINE   "Engine"
#define INPUT    "Input"
#define WEBCAM   "Webcam"

extern float phase;
#ifdef WITH_WEBCAM
extern int hflip, vflip, webcams;
extern char *video_base;
#endif
extern char *data_dir;
extern uint32_t input_size;
extern double volume_scale;
extern uint8_t start_with_first_sequence;
extern uint16_t http_port;
extern uint8_t use_hard_timers;


static void process_timers(GKeyFile *);
static void process_change_modes(GKeyFile *);


static void
process(GKeyFile *kf)
{
  GError *error = NULL;
  gchar *input = NULL, *output = NULL, *kf_themes = NULL;
  gint kf_input_size;
  gchar **blacklist;
  gsize blacklist_length = 0;
  gchar fs = FALSE, antiphase = FALSE;
#ifndef FIXED
  gint kf_width, kf_height;
#endif
  gint kf_fps, kf_rnd;
  gdouble kf_fade_delay;
#ifdef WITH_WEBCAM
  gchar whflip = FALSE, wvflip = FALSE;
  gint kf_webcams;
  gchar *kf_video_base;
#endif
  gchar *kf_sequences_dir = NULL;
  gchar *kf_data_dir = NULL;
  double kf_volume_scale = 0;
  gchar first_sequence = FALSE;
  gint kf_http_port;
  gchar hard_timers = FALSE;

  input = g_key_file_get_string(kf, PLUGINS, "Input", NULL);
  if (NULL != input) {
    input_plugin = input;
#ifdef DEBUG
    printf("[k] Setting input plugin: %s\n", input_plugin);
#endif
  }

  kf_input_size = g_key_file_get_integer(kf, PLUGINS, "InputSize", &error);
  if (NULL == error) {
    if (kf_input_size > 0) {
      input_size = kf_input_size;
#ifdef DEBUG
      printf("[k] Setting input size to %d\n", input_size);
#endif
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  output = g_key_file_get_string(kf, PLUGINS, "Output", NULL);
  if (NULL != output) {
    output_plugin = output;
#ifdef DEBUG
    printf("[k] Setting output plugin: %s\n", output_plugin);
#endif
  }

  fs = g_key_file_get_boolean(kf, SCREEN, "FullScreen", &error);
  if (NULL == error) {
    if (fs != fullscreen) {
      fullscreen = fs;
#ifdef DEBUG
      printf("[k] Setting full-screen to: %s\n", (fs ? "on" : "off"));
#endif
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  first_sequence = g_key_file_get_boolean(kf, ENGINE, "StartWithFirstSequence", &error);
  if (NULL == error) {
    start_with_first_sequence = first_sequence;
#ifdef DEBUG
    printf("[k] Starting with the %s sequence\n", (start_with_first_sequence ? "first" : "last"));
#endif
  } else {
    g_error_free(error);
    error = NULL;
  }

#ifndef FIXED
  kf_width = g_key_file_get_integer(kf, SCREEN, "Width", &error);
  if (NULL == error) {
    if (kf_width > 0)
      if (width != kf_width) {
        width = kf_width;
#ifdef DEBUG
        printf("[k] Setting screen width to %d\n", width);
#endif
      }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_height = g_key_file_get_integer(kf, SCREEN, "Height", &error);
  if (NULL == error) {
    if (kf_height > 0)
      if (height != kf_height) {
        height = kf_height;
#ifdef DEBUG
        printf("[k] Setting screen height to %d\n", height);
#endif
      }
  } else {
    g_error_free(error);
    error = NULL;
  }
#endif

  kf_fps = g_key_file_get_integer(kf, ENGINE, "Fps", &error);
  if (NULL == error) {
    if (kf_fps > 0)
      if (max_fps != kf_fps) {
        max_fps = kf_fps;
#ifdef DEBUG
        printf("[k] Setting FPS to %d\n", max_fps);
#endif
      }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_http_port = g_key_file_get_integer(kf, ENGINE, "HttpPort", &error);
  if (NULL == error) {
    if (!http_port || (kf_http_port > 1024)) {
      http_port = kf_http_port;
#ifdef DEBUG
      if (http_port) {
        printf("[k] Setting HTTP API port to %d\n", http_port);
      } else {
        printf("[k] Disabling HTTP API\n");
      }
#endif
    } else {
      xerror("HTTP API port must be >= 1024, or 0 to disable\n");
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_fade_delay = g_key_file_get_double(kf, ENGINE, "FadeDelay", &error);
  if (NULL == error) {
    if (kf_fade_delay >= 0 && fade_delay != kf_fade_delay) {
      fade_delay = kf_fade_delay;
#ifdef DEBUG
      printf("[k] Setting fade delay to %.2f s\n", fade_delay);
#endif
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_rnd = g_key_file_get_integer(kf, ENGINE, "RandomMode", &error);
  if (NULL == error) {
    if ((kf_rnd >= BR_NONE) && (kf_rnd <= BR_BOTH))
      if ((gint)random_mode != kf_rnd) {
        random_mode = kf_rnd;
#ifdef DEBUG
        printf("[k] Setting random mode to %d\n", kf_rnd);
#endif
      }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_themes = g_key_file_get_string(kf, ENGINE, "Themes", NULL);
  if (NULL != kf_themes) {
    themes = kf_themes;
#ifdef DEBUG
    printf("[k] Setting themes to: %s\n", themes);
#endif
  }

  kf_sequences_dir  = g_key_file_get_string(kf, ENGINE, "SequencesDir", NULL);
  if (NULL != kf_sequences_dir) {
#ifdef DEBUG
    printf("[k] Setting sequences directory to: %s\n", kf_sequences_dir);
#endif
    Sequences_set_dir(kf_sequences_dir);
  }

  kf_data_dir  = g_key_file_get_string(kf, ENGINE, "DataDir", NULL);
  if (NULL != kf_data_dir) {
#ifdef DEBUG
    printf("[k] Setting data directory to: %s\n", kf_data_dir);
#endif
    data_dir = kf_data_dir;
  }

  antiphase = g_key_file_get_boolean(kf, INPUT, "AntiPhase", &error);
  if (NULL == error) {
    if (antiphase == TRUE) {
      phase = -1.0;
#ifdef DEBUG
      printf("[k] Setting antiphase\n");
#endif
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_volume_scale = g_key_file_get_double(kf, INPUT, "VolumeScale", &error);
  if (NULL == error) {
    if (kf_volume_scale > 0) {
#ifdef DEBUG
      printf("[k] Setting volume scale to %.1f\n", kf_volume_scale);
#endif
      volume_scale = kf_volume_scale;
    } else {
      xerror("VolumeScale must be > 0\n");
    }
  } else if (G_KEY_FILE_ERROR_INVALID_VALUE == error->code) {
    xerror("Invalid VolumeScale\n");
  } else {
    g_error_free(error);
    error = NULL;
  }

#ifdef WITH_WEBCAM
  whflip = g_key_file_get_boolean(kf, WEBCAM, "HorizontalFlip", &error);
  if (NULL == error) {
    if (whflip == TRUE) {
#ifdef DEBUG
      printf("[k] Setting webcam horizontal flip\n");
#endif
      hflip = !hflip;
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  wvflip = g_key_file_get_boolean(kf, WEBCAM, "VerticalFlip", &error);
  if (NULL == error) {
    if (wvflip == TRUE) {
#ifdef DEBUG
      printf("[k] Setting webcam vertical flip\n");
#endif
      vflip = !vflip;
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_webcams = g_key_file_get_integer(kf, WEBCAM, "Webcams", &error);
  if (NULL == error) {
    if ((kf_webcams >= 0) && (kf_webcams <= MAX_CAMS)) {
      webcams = kf_webcams;
#ifdef DEBUG
      printf("[k] Webcam: grabbing %d device%s\n", webcams, (webcams == 1 ? "": "s"));
#endif
    }
  } else {
    g_error_free(error);
    error = NULL;
  }

  kf_video_base = g_key_file_get_string(kf, WEBCAM, "Device", NULL);
  if (NULL != kf_video_base) {
    free(video_base);
    video_base = kf_video_base;
#ifdef DEBUG
    printf("[k] Webcam: device base %s\n", video_base);
#endif
  }
#endif /* WITH_WEBCAM */

  blacklist = g_key_file_get_string_list(kf, PLUGINS, "Blacklist",
                                         &blacklist_length, NULL);
#ifdef DEBUG
  if (blacklist_length) {
    printf("[k] Plugins: blacklist of %"G_GSIZE_FORMAT" plugins\n", blacklist_length);
  }
#endif
  Plugins_set_blacklist(blacklist);

  hard_timers = g_key_file_get_boolean(kf, ENGINE, "HardTimers", &error);
  if (NULL == error) {
    use_hard_timers = hard_timers;
#ifdef DEBUG
    printf("[k] Using %s timers\n", use_hard_timers ? "hard" : "soft");
#endif
  } else {
    g_error_free(error);
    error = NULL;
  }
}


void
read_keyfile()
{
  const gchar *home_dir = NULL;
  g_autoptr(GKeyFile) kf = g_key_file_new();
  GKeyFileFlags flags = G_KEY_FILE_NONE;
  g_autoptr(GError) error = NULL;

  if (NULL == keyfile) {
    home_dir = g_get_home_dir();
    keyfile = g_strdup_printf("%s/%s", home_dir, KEYFILE);
#ifdef DEBUG
    printf("[k] Using default configuration file: %s\n", keyfile);
#endif
  }

  if (TRUE != g_key_file_load_from_file(kf, keyfile, flags, &error)) {
    if (G_FILE_ERROR_NOENT == error->code) {
      g_free(keyfile);
      return;
    } else {
      xerror("Failed to load %s\n", keyfile);
    }
  }

  process(kf);
  process_timers(kf);
  process_change_modes(kf);

  g_free(keyfile);
}


static void
process_timers(GKeyFile *kf)
{
  GError *error = NULL;
  gint colormaps_min, colormaps_max;
  gint images_min, images_max;
  gint sequences_min, sequences_max;

  colormaps_min = g_key_file_get_integer(kf, ENGINE, "ColormapsMin", &error);
  if (NULL == error) {
#ifdef DEBUG
    printf("[k] Setting ColormapsMin to %d\n", colormaps_min);
#endif
  } else {
    colormaps_min = DELAY_MIN;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
#ifdef DEBUG
      printf("[!] Invalid value for ColormapsMin, using default= %d\n", colormaps_min);
#endif
    }
    g_error_free(error);
    error = NULL;
  }

  colormaps_max = g_key_file_get_integer(kf, ENGINE, "ColormapsMax", &error);
  if (NULL == error) {
#ifdef DEBUG
    printf("[k] Setting ColormapsMax to %d\n", colormaps_max);
#endif
  } else {
    colormaps_max = DELAY_MAX;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
#ifdef DEBUG
      printf("[!] Invalid value for ColormapsMax, using default= %d\n", colormaps_max);
#endif
    }
    g_error_free(error);
    error = NULL;
  }

  biniou_set_delay(BD_COLORMAPS, colormaps_min, colormaps_max);

  images_min = g_key_file_get_integer(kf, ENGINE, "ImagesMin", &error);
  if (NULL == error) {
#ifdef DEBUG
    printf("[k] Setting ImagesMin to %d\n", images_min);
#endif
  } else {
    images_min = DELAY_MIN;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
#ifdef DEBUG
      printf("[!] Invalid value for ImagesMin, using default= %d\n", images_min);
#endif
    }
    g_error_free(error);
    error = NULL;
  }

  images_max = g_key_file_get_integer(kf, ENGINE, "ImagesMax", &error);
  if (NULL == error) {
#ifdef DEBUG
    printf("[k] Setting ImagesMax to %d\n", images_max);
#endif
  } else {
    images_max = DELAY_MAX;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
#ifdef DEBUG
      printf("[!] Invalid value for ImagesMax, using default= %d\n", images_max);
#endif
    }
    g_error_free(error);
    error = NULL;
  }

  biniou_set_delay(BD_IMAGES, images_min, images_max);

  sequences_min = g_key_file_get_integer(kf, ENGINE, "SequencesMin", &error);
  if (NULL == error) {
#ifdef DEBUG
    printf("[k] Setting SequencesMin to %d\n", sequences_min);
#endif
  } else {
    sequences_min = DELAY_MIN;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
#ifdef DEBUG
      printf("[!] Invalid value for SequencesMin, using default= %d\n", sequences_min);
#endif
    }
    g_error_free(error);
    error = NULL;
  }

  sequences_max = g_key_file_get_integer(kf, ENGINE, "SequencesMax", &error);
  if (NULL == error) {
#ifdef DEBUG
    printf("[k] Setting SequencesMax to %d\n", sequences_max);
#endif
  } else {
    sequences_max = DELAY_MAX;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
#ifdef DEBUG
      printf("[!] Invalid value for SequencesMax, using default= %d\n", sequences_max);
#endif
    }
    g_error_free(error);
    error = NULL;
  }

  biniou_set_delay(BD_SEQUENCES, sequences_min, sequences_max);

#ifdef WITH_WEBCAM
  gint webcams_min, webcams_max;

  webcams_min = g_key_file_get_integer(kf, WEBCAM, "WebcamsMin", &error);
  if (NULL == error) {
#ifdef DEBUG
    printf("[k] Setting WebcamsMin to %d\n", webcams_min);
#endif
  } else {
    webcams_min = DELAY_MIN;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
#ifdef DEBUG
      printf("[!] Invalid value for WebcamsMin, using default= %d\n", webcams_min);
#endif
    }
    g_error_free(error);
    error = NULL;
  }

  webcams_max = g_key_file_get_integer(kf, WEBCAM, "WebcamsMax", &error);
  if (NULL == error) {
#ifdef DEBUG
    printf("[k] Setting WebcamsMax to %d\n", webcams_max);
#endif
  } else {
    webcams_max = DELAY_MAX;
    if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND) {
#ifdef DEBUG
      printf("[!] Invalid value for WebcamsMax, using default= %d\n", webcams_max);
#endif
    }
    g_error_free(error);
    error = NULL;
  }

  biniou_set_delay(BD_WEBCAMS, webcams_min, webcams_max);
#endif
}


void
set_configuration(gchar *file)
{
  keyfile = strdup(file);
#ifdef DEBUG
  printf("[k] Setting configuration file: %s\n", keyfile);
#endif
}


static void
process_change_modes(GKeyFile *kf)
{
  gchar *colormaps_mode = NULL;
  gchar *images_mode = NULL;
  gchar *sequences_mode = NULL;

  colormaps_mode = g_key_file_get_string(kf, ENGINE, "ColormapsMode", NULL);
  if (NULL != colormaps_mode) {
#ifdef DEBUG
    printf("[k] Setting colormaps mode: %s\n", colormaps_mode);
#endif
    Context_set_shuffler_mode(BD_COLORMAPS, Shuffler_parse_mode(colormaps_mode));
    xfree(colormaps_mode);
  }

  images_mode = g_key_file_get_string(kf, ENGINE, "ImagesMode", NULL);
  if (NULL != images_mode) {
#ifdef DEBUG
    printf("[k] Setting images mode: %s\n", images_mode);
#endif
    Context_set_shuffler_mode(BD_IMAGES, Shuffler_parse_mode(images_mode));
    xfree(images_mode);
  }

  sequences_mode = g_key_file_get_string(kf, ENGINE, "SequencesMode", NULL);
  if (NULL != sequences_mode) {
#ifdef DEBUG
    printf("[k] Setting sequences mode: %s\n", sequences_mode);
#endif
    Context_set_shuffler_mode(BD_SEQUENCES, Shuffler_parse_mode(sequences_mode));
    xfree(sequences_mode);
  }

#ifdef WITH_WEBCAM
  gchar *webcams_mode = NULL;
  webcams_mode = g_key_file_get_string(kf, WEBCAM, "WebcamsMode", NULL);
  if (NULL != webcams_mode) {
#ifdef DEBUG
    printf("[k] Setting webcams mode: %s\n", webcams_mode);
#endif
    Context_set_shuffler_mode(BD_WEBCAMS, Shuffler_parse_mode(webcams_mode));
  }
#endif
}

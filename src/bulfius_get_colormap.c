/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "colormaps.h"


int
callback_get_colormap(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  const Context_t *ctx = user_data;
  const char *name = u_map_get(request->map_url, "name");
  json_t *res;

  assert(NULL != ctx);
  if (NULL != name) {
    // With "name" as a parameter
    res = Cmap8_to_json(colormaps->cmaps[Colormaps_name_to_index(name)]);
  } else {
    // Current colormap
    res = Cmap8_to_json(ctx->cf->dst);
  }
  ulfius_set_json_body_response(response, 200, res);
  json_decref(res);
  ulfius_add_header_to_response(response, "Access-Control-Allow-Origin", "*");

  return U_CALLBACK_COMPLETE;
}

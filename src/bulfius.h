/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_ULFIUS_H
#define __BINIOU_ULFIUS_H

#include <ulfius.h>
#include "commands.h"

#define BULFIUS_GET  "GET"
#define BULFIUS_POST "POST"

#define BULFIUS_COLORMAP       "/colormap"
#define BULFIUS_COMMAND        "/command"
#define BULFIUS_COMMAND_FMT    "/:cmd"
#define BULFIUS_COMMANDS       "/commands"
#define BULFIUS_IMAGE          "/image"
#define BULFIUS_FRAME          "/frame"
#define BULFIUS_PARAMETERS     "/parameters"
#define BULFIUS_PARAMETERS_FMT "/:plugin"
#define BULFIUS_PLUGINS        "/plugins"
#define BULFIUS_REPLAY         "/replay"
#define BULFIUS_SEQUENCE       "/sequence"
#define BULFIUS_STATISTICS     "/statistics"
// Web UI
#define BULFIUS_UI_COMMAND     "/ui/command"
#define BULFIUS_UI_ROOT        "/"
#define BULFIUS_TESTING_ROOT   "/testing"
#define BULFIUS_UI_WEBSOCKET   "/ui"
#define BULFIUS_UI_STATIC      "/ui/static"
#define BULFIUS_UI_STATIC_FMT  "/:file"
#define BULFIUS_UI_FAVICO_URL  "/favicon.ico"
#define BULFIUS_UI_FAVICO_ICO  "www/favicon.ico"
#define BULFIUS_UI_INDEX       "www/index.html"
#define BULFIUS_TESTING_INDEX  "www/testing.html"
// JavaScript/CSS
#define BULFIUS_UI_JQUERY_MIN_JS_URL     "/jquery.min.js"
#define BULFIUS_UI_JQUERY_MIN_JS         "www/jquery.min.js"
#define BULFIUS_UI_JQUERY_UI_MIN_JS_URL  "/jquery-ui.min.js"
#define BULFIUS_UI_JQUERY_UI_MIN_JS      "www/jquery-ui.min.js"
#define BULFIUS_UI_JQUERY_UI_MIN_CSS_URL "/jquery-ui.min.css"
#define BULFIUS_UI_JQUERY_UI_MIN_CSS     "www/jquery-ui.min.css"

// GET
int callback_get_colormap(const struct _u_request *, struct _u_response *, void *);
int callback_get_commands(const struct _u_request *, struct _u_response *, void *);
int callback_get_image(const struct _u_request *, struct _u_response *, void *);
int callback_get_frame(const struct _u_request *, struct _u_response *, void *);
int callback_get_parameters(const struct _u_request *, struct _u_response *, void *);
int callback_get_plugins(const struct _u_request *, struct _u_response *, void *);
int callback_get_replay(const struct _u_request *, struct _u_response *, void *);
int callback_get_sequence(const struct _u_request *, struct _u_response *, void *);
int callback_get_statistics(const struct _u_request *, struct _u_response *, void *);
// GET/Web UI
int callback_ui_get_static(const struct _u_request *, struct _u_response *, void *);

// POST
int callback_post_command(const struct _u_request *, struct _u_response *, void *);
int callback_post_parameters(const struct _u_request *, struct _u_response *, void *);
int callback_post_sequence(const struct _u_request *, struct _u_response *, void *);
// POST/Web UI
int callback_ui_post_command(const struct _u_request *, struct _u_response *, void *);


// Websockets
int callback_websocket(const struct _u_request *, struct _u_response *, void *);

// Commands
int is_allowed(const enum Command cmd);
enum Command str2command(const char *);
const char *command2str(const enum Command);

#endif /* __BINIOU_ULFIUS_H */

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "circle.h"


void
draw_circle(Buffer8_t *buf, float rayon, float centre_x, float centre_y)
{
  static unsigned char c = 0;
  float u, w, x, y, z, distance_a_origine;

  distance_a_origine = rayon;

  u = rayon * rayon;

  for (x = (centre_x - rayon); x <= centre_x; x++) {
    distance_a_origine = x - centre_x;

    y = u - (distance_a_origine * distance_a_origine);

    y = sqrtf(y) + centre_y;

    set_pixel(buf, x, y,  c++);

    /*  Mirrors */
    z = centre_y - (y - centre_y);

    set_pixel(buf, x, z,  c++);

    w =  centre_x - distance_a_origine;

    set_pixel(buf, w, y,  c++);
    set_pixel(buf, w, z,  c++);
  }
}


void
draw_rosace(Buffer8_t *buff, float rayon, float center_x, float center_y)
{
  draw_circle(buff, rayon, center_x, center_y);
  draw_circle(buff, rayon, center_x + rayon, center_y);
  draw_circle(buff, rayon, center_x, center_y - rayon);
  draw_circle(buff, rayon, center_x - rayon, center_y);
  draw_circle(buff, rayon, center_x, center_y + rayon);
}

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_BTIMER_H
#define __BINIOU_BTIMER_H

#include "utils.h"

enum BTimerMode { BT_SOFT = 0, BT_HARD };

typedef struct BTimer_s {
#ifdef DEBUG_TIMERS
  const char *name;
#endif
  struct timeval h_start; // hard timers
  uint64_t       s_start; // soft timers
} BTimer_t;


BTimer_t *b_timer_new(const char *); // name will be displayed when compiled with DEBUG_TIMERS
void b_timer_delete(BTimer_t *);

void b_timer_start(BTimer_t *);
float b_timer_elapsed(const BTimer_t *);

void b_timer_set_mode(const enum BTimerMode);

#endif /* __BINIOU_BTIMER_H */

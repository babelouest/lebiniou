/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "biniou.h"
#include "defaults.h"


extern uint64_t frames;
extern uint8_t  max_fps;
extern uint8_t  encoding;


#define DELIM ", "

static json_t *
json_strtok(char *s)
{
  json_t *res = json_array();
  char *str = strdup(s);
  char *token = strtok(str, DELIM);

  while (NULL != token) {
    json_array_append_new(res, json_string(token));
    token = strtok(NULL, DELIM);
  }
  xfree(str);

  return res;
}


json_t *
Context_get_state(const Context_t *ctx)
{
  json_t *res = json_object();
  json_t *seq;
  int colormaps_min, colormaps_max;
  int images_min, images_max;
  int sequences_min, sequences_max;
#ifdef WITH_WEBCAM
  int webcams_min, webcams_max;
#endif

  json_object_set_new(res, "version", json_string(LEBINIOU_VERSION));
  seq = Sequence_to_json(ctx->sm->cur, 1, 0);
  json_object_set_new(res, "sequence_name", json_string((NULL == ctx->sm->cur->name) ? UNSAVED_SEQUENCE : ctx->sm->cur->name));

  biniou_get_delay(BD_COLORMAPS, &colormaps_min, &colormaps_max);
  biniou_get_delay(BD_IMAGES, &images_min, &images_max);
  biniou_get_delay(BD_SEQUENCES, &sequences_min, &sequences_max);
#ifdef WITH_WEBCAM
  biniou_get_delay(BD_WEBCAMS, &webcams_min, &webcams_max);
#endif

  if (NULL != plugins->selected) {
    json_object_set_new(res, "selected_plugin", json_string(plugins->selected->name));
    json_object_set_new(res, "selected_plugin_dname", json_string(plugins->selected->dname));
  }
  json_object_set_new(res, "sequence", json_copy(seq));
  json_decref(seq);
  json_object_set_new(res, "frames", json_integer(frames));
  json_object_set_new(res, "uptime", json_integer((uint32_t)b_timer_elapsed(ctx->timer)));
  json_object_set_new(res, "random_schemes", json_boolean((ctx->random_mode == BR_SCHEMES) || (ctx->random_mode == BR_BOTH)));
  json_object_set_new(res, "random_sequences", json_boolean((ctx->random_mode == BR_SEQUENCES) || (ctx->random_mode == BR_BOTH)));
  json_object_set_new(res, "auto_colormaps", json_boolean(ctx->auto_colormaps));
  json_object_set_new(res, "auto_colormaps_mode", json_string(Shuffler_mode2str(ctx->cf->shf)));
  json_object_set_new(res, "auto_images", json_boolean(ctx->auto_images));
  if (NULL != ctx->imgf) {
    json_object_set_new(res, "auto_images_mode", json_string(Shuffler_mode2str(ctx->imgf->shf)));
  }
  json_object_set_new(res, "colormaps_min", json_integer(colormaps_min));
  json_object_set_new(res, "colormaps_max", json_integer(colormaps_max));
  json_object_set_new(res, "images_min", json_integer(images_min));
  json_object_set_new(res, "images_max", json_integer(images_max));
  json_object_set_new(res, "auto_sequences_mode", json_string(Shuffler_mode2str(sequences->shuffler)));
  json_object_set_new(res, "sequences_min", json_integer(sequences_min));
  json_object_set_new(res, "sequences_max", json_integer(sequences_max));
#ifdef WITH_WEBCAM
  json_object_set_new(res, "webcams_min", json_integer(webcams_min));
  json_object_set_new(res, "webcams_max", json_integer(webcams_max));
  json_object_set_new(res, "webcams", json_integer(ctx->webcams));
  if (webcams > 1) {
    json_object_set_new(res, "auto_webcams_mode", json_string(Shuffler_mode2str(ctx->webcams_shuffler)));
  }
#endif
  json_object_set_new(res, "max_fps", json_integer(max_fps));
  if (NULL != ctx->locked) {
    json_object_set_new(res, "locked_plugin", json_string(ctx->locked->name));
  } else {
    json_object_set_new(res, "locked_plugin", json_null());
  }
  json_object_set_new(res, "bank_set", json_integer(ctx->bank_set));
  json_object_set_new(res, "bank", json_integer(ctx->bank));
  json_object_set_new(res, "volume_scale", json_real(Context_get_volume_scale(ctx)));
  json_object_set_new(res, "fade_delay", json_real(fade_delay));
  json_object_set_new(res, "params3d", Params3d_to_json(&ctx->params3d));
  if (NULL != ctx->input_plugin) {
    json_object_set_new(res, "input_plugin", json_string(ctx->input_plugin->name));
    json_object_set_new(res, "mute", json_boolean(ctx->input->mute));
  } else {
    json_object_set_new(res, "input_plugin", json_null());
  }
  json_object_set_new(res, "output_plugins", Context_output_plugins(ctx));
  json_object_set_new(res, "fullscreen", json_boolean(ctx->fullscreen));
  json_object_set_new(res, "encoding", json_boolean(encoding));
  json_object_set_new(res, "all_input_plugins", json_strtok(INPUT_PLUGINS));
  json_object_set_new(res, "all_output_plugins", json_strtok(OUTPUT_PLUGINS));
  json_object_set_new(res, "rotation_factor", json_integer(ctx->params3d.rotation_factor));

  return res;
}


void
Context_websocket_send_state(Context_t *ctx)
{
  if (NULL != ctx->ws_clients) {
    json_t *state = Context_get_state(ctx);
#ifndef ULFIUS_GE_2_6_9
    char *payload = json_dumps(state, 0);
#endif

    pthread_mutex_lock(&ctx->ws_clients_mutex);
    for (GSList *client = ctx->ws_clients; NULL != client; client = client->next) {
#ifdef ULFIUS_GE_2_6_9
      ulfius_websocket_send_json_message((struct _websocket_manager *)client->data, state);
#else
      ulfius_websocket_send_message((struct _websocket_manager *)client->data, U_WEBSOCKET_OPCODE_TEXT, strlen(payload), payload);
#endif
    }
    pthread_mutex_unlock(&ctx->ws_clients_mutex);
#ifndef ULFIUS_GE_2_6_9
    xfree(payload);
#endif
    json_decref(state);
  }
}


void
Context_websocket_send_command_result(Context_t *ctx, const enum Command cmd, json_t *res,
                                      const struct _websocket_manager *from)
{
  if (NULL == res) {
    json_t *json = json_pack("{ssss}", "command", command2str(cmd), "result", "ok");
#ifndef ULFIUS_GE_2_6_9
    char *str = g_strdup_printf("{ \"command\": \"%s\", \"result\": \"ok\" }", command2str(cmd));
#endif
#ifdef DEBUG_WS
    VERBOSE(printf("[i] %s (%s) %d: JSON send '%s' command result: 'ok'\n", __FILE__, __func__, __LINE__, command2str(cmd)));
#endif

    pthread_mutex_lock(&ctx->ws_clients_mutex);
    for (GSList *client = ctx->ws_clients; NULL != client; client = client->next) {
#ifdef ULFIUS_GE_2_6_9
      ulfius_websocket_send_json_message((struct _websocket_manager *)client->data, json);
#else
      ulfius_websocket_send_message((struct _websocket_manager *)client->data, U_WEBSOCKET_OPCODE_TEXT, strlen(str), str);
#endif
    }
    pthread_mutex_unlock(&ctx->ws_clients_mutex);
#ifndef ULFIUS_GE_2_6_9
    xfree(str);
#endif
    json_decref(json);
  } else {
    json_t *json = json_pack("{ssso}", "command", command2str(cmd), "result", json_deep_copy(res));
#ifndef ULFIUS_GE_2_6_9
    char *js_str = json_dumps(res, JSON_COMPACT);
    char *str = g_strdup_printf("{ \"command\": \"%s\", \"result\": %s }", command2str(cmd), js_str);
    xfree(js_str);
#endif
#ifdef DEBUG_WS
    char *tmp = json_dumps(res, 0);
    VERBOSE(printf("[i] %s (%s) %d: JSON send '%s' command result: '%s'\n", __FILE__, __func__, __LINE__, command2str(cmd), tmp));
    xfree(tmp);
#endif
    pthread_mutex_lock(&ctx->ws_clients_mutex);
    for (GSList *client = ctx->ws_clients; NULL != client; client = client->next) {
      struct _websocket_manager *to = (struct _websocket_manager *)client->data;

      if (cmd == UI_CMD_CONNECT) {
        if (from == to) {
#ifdef DEBUG_WS
          VERBOSE(printf("[i] %s (%s) %d: JSON send command result '%s' from %p to %p\n", __FILE__, __func__, __LINE__, command2str(cmd), from, to));
#endif
#ifdef ULFIUS_GE_2_6_9
          ulfius_websocket_send_json_message(to, json);
#else
          ulfius_websocket_send_message(to, U_WEBSOCKET_OPCODE_TEXT, strlen(str), str);
#endif
        }
      } else {
#ifdef ULFIUS_GE_2_6_9
        ulfius_websocket_send_json_message(to, json);
#else
        ulfius_websocket_send_message(to, U_WEBSOCKET_OPCODE_TEXT, strlen(str), str);
#endif
      }
    }
    pthread_mutex_unlock(&ctx->ws_clients_mutex);
#ifndef ULFIUS_GE_2_6_9
    xfree(str);
#endif
    json_decref(json);
  }
}

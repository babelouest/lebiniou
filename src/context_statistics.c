/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/utsname.h>
#include <ulfius.h>
#include "context.h"
#include "defaults.h"


#define STATS_URL "https://stats.biniou.net/lebiniou"


extern uint64_t frames;


void Context_statistics(const Context_t *ctx)
{
  json_t *stats = json_object();

#ifdef DEBUG
  json_object_set_new(stats, "debug", json_true());
#else
  json_object_set_new(stats, "debug", json_false());
#endif
#ifdef FIXED
  json_object_set_new(stats, "fixed", json_true());
#else
  json_object_set_new(stats, "fixed", json_false());
#endif
  json_object_set_new(stats, "version", json_string(LEBINIOU_VERSION));
  json_object_set_new(stats, "elapsed", json_integer((uint64_t)b_timer_elapsed(ctx->timer)));

  if (NULL != ctx->input_plugin) {
    json_object_set_new(stats, "input", json_string(ctx->input_plugin->name));
  } else {
    json_object_set_new(stats, "input", json_string("NULL"));
  }

  for (GSList *outputs = ctx->outputs; NULL != outputs; outputs = g_slist_next(outputs)) {
    Plugin_t *output = (Plugin_t *)outputs->data;
    char *o = g_strdup_printf("output.%s", output->name);

    json_object_set_new(stats, o, json_true());
    xfree(o);
  }

  json_object_set_new(stats, "width", json_integer(WIDTH));
  json_object_set_new(stats, "height", json_integer(HEIGHT));
  
  json_object_set_new(stats, "frames", json_integer(frames));
  json_object_set_new(stats, "sdl2_commands", json_integer(ctx->commands[BC_SDL2]));
  json_object_set_new(stats, "rest_commands", json_integer(ctx->commands[BC_REST]));
  json_object_set_new(stats, "web_commands", json_integer(ctx->commands[BC_WEB]));

  struct utsname _uname;
  if (!uname(&_uname)) {
    json_object_set_new(stats, "sysname", json_string(_uname.sysname));
    json_object_set_new(stats, "nodename", json_string(_uname.nodename));
    json_object_set_new(stats, "release", json_string(_uname.release));
    json_object_set_new(stats, "kernel", json_string(_uname.version));
    json_object_set_new(stats, "machine", json_string(_uname.machine));
  } else {
    xperror("uname");
  }

  DEBUG_JSON("stats", stats);

  struct _u_request req;
  ulfius_init_request(&req);
  req.http_verb = o_strdup("POST");
  req.http_url = o_strdup(STATS_URL);
  req.timeout = 5000;
  ulfius_set_json_body_request(&req, stats);

  int res = ulfius_send_http_request(&req, NULL);
  if (res != U_OK) {
#ifdef DEBUG
    fprintf(stderr, "[!] Error in http request: %d\n", res);
#endif
  }
  ulfius_clean_request(&req);
  json_decref(stats);
}

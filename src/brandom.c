/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "brandom.h"


static GRand *brand = NULL;


void
b_rand_init(void)
{
  uint32_t seed;
  char *seedstr;

  if (NULL != (seedstr = getenv("LEBINIOU_SEED"))) {
    seed = xatol(seedstr);
    VERBOSE(printf("[i] Random seed set to %d\n", seed));
  } else {
    struct timeval t;

    gettimeofday(&t, NULL);
    seed = t.tv_sec;
    VERBOSE(printf("[i] No random seed, using %d\n", seed));
  }
  brand = g_rand_new_with_seed(seed);
}


void
b_rand_free(void)
{
  if (NULL != brand) {
    g_rand_free(brand);
  }
}


uint32_t
b_rand_int(void)
{
  return g_rand_int(brand);
}


int32_t
b_rand_int32_range(const int32_t begin, const int32_t end)
{
  return (begin == end) ? end : g_rand_int_range(brand, begin, end);
}


uint32_t
b_rand_uint32_range(const uint32_t begin, const uint32_t end)
{
  return (begin == end) ? end : (uint32_t)g_rand_int_range(brand, begin, end);
}


double
b_rand_double_range(const double begin, const double end)
{
  return g_rand_double_range(brand, begin, end);
}


int
b_rand_boolean()
{
  return g_rand_boolean(brand);
}


void
b_rand_set_seed(const uint32_t seed)
{
  g_rand_set_seed(brand, seed);
}

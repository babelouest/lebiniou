/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_FADER_H
#define __BINIOU_FADER_H

#include "utils.h"
#include "btimer.h"

/* default fade delay in seconds */
#define DEFAULT_FADE_DELAY 3

extern float fade_delay;

/* for fading timers */
#define MFACTOR    1000


typedef struct Fader_s {
  uint16_t  target;
  long     *delta;
  uint32_t   *tmp;
  uint8_t   fading;
  uint32_t   max;
  uint32_t   faded;
  BTimer_t *timer;
} Fader_t;


Fader_t *Fader_new(const uint32_t);
void Fader_delete(Fader_t *);


static inline void
Fader_init(Fader_t *fa)
{
  /* we fade by msecs */
  fa->max = (uint32_t)(fade_delay * MFACTOR);
  fa->faded = 0;
}


static inline void
Fader_start(Fader_t *fa)
{
  b_timer_start(fa->timer);
}


uint32_t Fader_elapsed(const Fader_t *);


#endif /* __BINIOU_FADER_H */

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_SEQUENCE_MANAGER_H
#define __BINIOU_SEQUENCE_MANAGER_H

#include "context.h"
#include "sequence.h"
#include "commands.h"
#include "constants.h"


typedef struct SequenceManager_s {
  Sequence_t      *cur, *next;
  GList           *curseq;
  pthread_mutex_t mutex;
} SequenceManager_t;


SequenceManager_t *SequenceManager_new();
void SequenceManager_delete(SequenceManager_t *);

void SequenceManager_toggle_lens(Sequence_t *);

void SequenceManager_select_previous_plugin(Sequence_t *);
void SequenceManager_select_next_plugin(Sequence_t *);

void SequenceManager_move_selected_front(Sequence_t *);
void SequenceManager_move_selected_back(Sequence_t *);

void SequenceManager_default_layer_mode(const Sequence_t *);
void SequenceManager_prev_layer_mode(const Sequence_t *);
void SequenceManager_next_layer_mode(const Sequence_t *);

json_t *SequenceManager_command(struct Context_s *, const enum Command, const char, const char);

void SequenceManager_lock(SequenceManager_t *);
void SequenceManager_unlock(SequenceManager_t *);

void SequenceManager_reorder(Sequence_t *, const json_t *);

#endif /* __BINIOU_SEQUENCE_MANAGER_H */

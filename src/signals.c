/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.h"


/* signal callback */
static void
sighandle(int sig)
{
  printf("\n[+] Got signal %d !\n", sig);
  biniou_stop();
#ifdef WITH_ULFIUS
  Context_stop_ulfius(context);
#endif
  biniou_delete();
  remove_pid_file();
  exit(0);
}


void
register_signals(void)
{
  if (signal(SIGTERM, sighandle) == SIG_ERR) {
    printf("[!] can't catch SIGTERM signal\n");
  }
  if (signal(SIGHUP, sighandle) == SIG_ERR) {
    printf("[!] can't catch SIGHUP signal\n");
  }
  if (signal(SIGINT, sighandle) == SIG_ERR) {
    printf("[!] can't catch SIGINT signal\n");
  }
}

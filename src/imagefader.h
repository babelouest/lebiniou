/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_IMAGEFADER_H
#define __BINIOU_IMAGEFADER_H

#include <jansson.h>
#include "image_8bits.h"
#include "fader.h"
#include "commands.h"
#include "shuffler.h"


typedef struct ImageFader_s {
  uint8_t     on;
  Image8_t   *cur;
  Image8_t   *dst;
  Fader_t    *fader;
  Shuffler_t *shf;
} ImageFader_t;


ImageFader_t *ImageFader_new(const uint16_t);
void ImageFader_delete(ImageFader_t *);

void ImageFader_set(ImageFader_t *);

void ImageFader_prev(ImageFader_t *);
void ImageFader_prev_n(ImageFader_t *, const uint16_t);
void ImageFader_next(ImageFader_t *);
void ImageFader_next_n(ImageFader_t *, const uint16_t);
void ImageFader_random(ImageFader_t *);

void ImageFader_init(ImageFader_t *);
void ImageFader_run(ImageFader_t *);

json_t *ImageFader_command(ImageFader_t *, const enum Command);
json_t *ImageFader_command_result(const ImageFader_t *);

int ImageFader_ring(const ImageFader_t *);

#endif /* __BINIOU_IMAGEFADER_H */

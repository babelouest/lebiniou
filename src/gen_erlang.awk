BEGIN {
    print;
    print "cmd(To, Cmd, Arg) ->";
    print "    {biniou, node()} ! {cmd, <<To, Cmd, Arg>>}.";
}


function pmod(mod) {
    if (mod == "-")
	return;
    if (mod == "A")
	return "Alt-";
    if (mod == "C")
	return "Ctrl-";
    if (mod == "S")
	return "Shift-";
    if (mod == "CS")
	return "Ctrl-Shift-";
}


{
    if (($1 == "#") || ($0 == "") || ($1 == "-"))
	next;
  
    if ($1 == "*") {
	print "\n";
	print "%% =============== "$2" ===============";
    } else {
	print "";
	tail = substr($0, (length($1 $2 $3 $4 $5 $6) + 7));

	if (tail != "")
	    printf "%%%% %s\n", tail;

	arg = substr($6, 0, 2);
	if (arg == "BA")
	    printf "cmd(\"%s\") -> cmd(?%s, ?%s, ?%s);", $1, $4, $5, $6;
	else
	    printf "cmd(\"%s\") -> cmd(?%s, ?%s, %s);", $1, $4, $5, $6;
    }
}


END {
    print "\ncmd(_) -> ok.";
}

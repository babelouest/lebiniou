/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


void
Context_set(Context_t *ctx)
{
  GList *tmp;

  tmp = g_list_first(ctx->sm->cur->layers);

  /* call on_switch_off() on old plugins */
  while (NULL != tmp) {
    Layer_t *layer = tmp->data;
    Plugin_t *p = layer->plugin;

    assert(NULL != p);
    if (NULL != p->on_switch_off) {
      p->on_switch_off(ctx);
    }
    tmp = g_list_next(tmp);
  }

  Context_set_colormap(ctx);
  Context_set_image(ctx);

  /* call on_switch_on() on new plugins */
  tmp = g_list_first(ctx->sm->next->layers);
  while (NULL != tmp) {
    Layer_t *layer = (Layer_t *)tmp->data;
    Plugin_t *p = layer->plugin;

    assert(NULL != p);
    if (NULL != p->on_switch_on) {
      p->on_switch_on(ctx);
    }

    if (NULL != p->parameters) {
      /* Todo: check and use returned parameters ? */
      json_t *res = p->parameters(ctx, layer->plugin_parameters);

      json_decref(res);
    }
    tmp = g_list_next(tmp);
  }

  Sequence_copy(ctx, ctx->sm->next, ctx->sm->cur);
  Params3d_from_json(&ctx->params3d, ctx->sm->cur->params3d);
  Context_update_auto(ctx);
  Sequence_display(ctx->sm->cur);
#ifdef WITH_ULFIUS
  Context_websocket_send_state(ctx);
#endif
  okdone("Context_set");
}

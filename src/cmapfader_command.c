/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "cmapfader.h"


json_t *
CmapFader_command_result(const CmapFader_t *cf)
{
  json_t *res = json_object();

  json_object_set_new(res, "colormap", json_string(cf->dst->name));

  return res;
}


json_t *
CmapFader_command(CmapFader_t *cf, const enum Command cmd)
{
  switch (cmd) {
    case CMD_COL_PREVIOUS:
#ifdef DEBUG_COMMANDS
      printf(">>> CMD_COL_PREVIOUS\n");
#endif
      CmapFader_prev(cf);
      return CmapFader_command_result(cf);
      break;

    case CMD_COL_NEXT:
#ifdef DEBUG_COMMANDS
      printf(">>> CMD_COL_NEXT\n");
#endif
      CmapFader_next(cf);
      return CmapFader_command_result(cf);
      break;

    case CMD_COL_RANDOM:
#ifdef DEBUG_COMMANDS
      printf(">>> CMD_COL_RANDOM\n");
#endif
      CmapFader_random(cf);
      return CmapFader_command_result(cf);
      break;

    default:
      printf("Unhandled colormaps command %d\n", cmd);
      return NULL;
  }
}

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"
#include "sequence.h"
#include "images.h"
#include "colormaps.h"

#define SEQUENCE_VERSION 1


json_t *
Sequence_to_json(const Sequence_t *s, const uint8_t full, const uint8_t parameter_value_only)
{
  json_t *sequence_j = json_object();
  json_t *plugins_j = json_array();

  json_object_set_new(sequence_j, "version", json_integer(SEQUENCE_VERSION));
  json_object_set_new(sequence_j, "id", json_integer(s->id));

  if (full) {
    json_object_set_new(sequence_j, "auto_colormaps", json_boolean(s->auto_colormaps));
    json_object_set_new(sequence_j, "colormap", json_string(Colormaps_name(s->cmap_id)));
    json_object_set_new(sequence_j, "auto_images", json_boolean(s->auto_images));
    json_object_set_new(sequence_j, "image", json_string(Images_name(s->image_id)));
  }

  /* iterate over plugins list */
  for (GList *layers = g_list_first(s->layers); NULL != layers; layers = g_list_next(layers)) {
    Layer_t *layer = (Layer_t *)layers->data;
    Plugin_t *p = layer->plugin;
    json_t *j_plugin = json_object();

    json_object_set_new(j_plugin, "lens", json_boolean((NULL != s->lens) && (p == s->lens)));
    json_object_set_new(j_plugin, "name", json_string(p->name));
    json_object_set_new(j_plugin, "display_name", json_string(p->dname));
    json_object_set_new(j_plugin, "version", json_integer(p->version));
    json_object_set_new(j_plugin, "mode", json_string(LayerMode_to_string(layer->mode)));

    if (NULL != p->parameters) {
      json_t *j_params = p->parameters(NULL, NULL);

      if (parameter_value_only) {
        json_object_set_new(j_plugin, "parameters", plugin_parameters_to_saved_parameters(j_params));
        json_decref(j_params);
      } else {
        json_object_set_new(j_plugin, "parameters", j_params);
      }
    }
    json_array_append_new(plugins_j, j_plugin);
  }
  json_object_set_new(sequence_j, "plugins", plugins_j);
  json_object_set(sequence_j, "params3d", s->params3d);

  return sequence_j;
}

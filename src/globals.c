/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"

int32_t     WIDTH_ORIGIN, HEIGHT_ORIGIN;
#ifndef FIXED
uint16_t     WIDTH, HEIGHT;
#endif

Plugins_t   *plugins   = NULL;
Sequences_t *sequences = NULL;
Context_t   *context   = NULL;
Schemes_t   *schemes   = NULL;

uint8_t libbiniou_verbose = 1;

char *data_dir = NULL;
#ifdef WITH_WEBCAM
int webcams = 1;
int hflip = 1;
int vflip = 1;
char *video_base;
#else
int webcams = 0;
#endif

uint8_t max_fps = DEFAULT_MAX_FPS;
uint8_t use_hard_timers = 1;
uint64_t frames = 0;

// sndfile
char *audio_file = NULL;

// start mp4 encoding automatically
uint8_t  encoding = 0;

int override_webcam = 0;
#ifdef WITH_ULFIUS
uint8_t usage_statistics = 1;
uint8_t enable_osd = 0;
#else
uint8_t enable_osd = 1;
#endif

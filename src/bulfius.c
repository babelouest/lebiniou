/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "defaults.h"


extern uint8_t max_fps;
extern uint16_t http_port;
static pthread_t ping_thread;


static void *
ui_thread(void *attr)
{
  Context_t *ctx = (Context_t *)attr;

  while (ctx->running) {
    if (NULL != ctx->ws_clients) {
      json_t *res = json_pack("{so}",
                              "ping", json_pack("{sisisi}",
                                  "fps", (uint16_t)Context_fps(ctx),
                                  "max_fps", max_fps,
                                  "uptime", (uint64_t)b_timer_elapsed(ctx->timer)));
#ifndef ULFIUS_GE_2_6_9
      char *payload = json_dumps(res, 0);
#endif
      pthread_mutex_lock(&ctx->ws_clients_mutex);
      for (GSList *client = ctx->ws_clients; NULL != client; client = client->next) {
#ifdef ULFIUS_GE_2_6_9
        ulfius_websocket_send_json_message((struct _websocket_manager *)client->data, res);
#else
        ulfius_websocket_send_message((struct _websocket_manager *)client->data, U_WEBSOCKET_OPCODE_TEXT, strlen(payload), payload);
#endif
      }
      pthread_mutex_unlock(&ctx->ws_clients_mutex);
#ifndef ULFIUS_GE_2_6_9
      xfree(payload);
#endif
      json_decref(res);
    }
    ms_sleep(1000);
  }

  return NULL;
}


void
Context_start_ulfius(Context_t *ctx)
{
  pthread_mutex_init(&ctx->ws_clients_mutex, NULL);

  // Initialize instance
  if (ulfius_init_instance(&ctx->instance, http_port, NULL, NULL) != U_OK) {
    xerror("ulfius_init_instance error on port %d, aborting\n", http_port);
  }

  // Endpoint list declaration
  // GET
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_COLORMAP, NULL, 0, &callback_get_colormap, ctx);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_COMMANDS, NULL, 0, &callback_get_commands, ctx);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_IMAGE, NULL, 0, &callback_get_image, ctx);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_FRAME, NULL, 0, &callback_get_frame, ctx);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_PARAMETERS, BULFIUS_PARAMETERS_FMT, 0, &callback_get_parameters, ctx);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_PLUGINS, NULL, 0, &callback_get_plugins, NULL);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_REPLAY, NULL, 0, &callback_get_replay, ctx);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_SEQUENCE, NULL, 0, &callback_get_sequence, ctx);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_STATISTICS, NULL, 0, &callback_get_statistics, ctx);
  // GET/UI
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_UI_ROOT, NULL, 0, &callback_ui_get_static, BULFIUS_UI_INDEX);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_UI_FAVICO_URL, NULL, 0, &callback_ui_get_static, BULFIUS_UI_FAVICO_ICO);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_TESTING_ROOT, NULL, 0, &callback_ui_get_static, BULFIUS_TESTING_INDEX);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_UI_STATIC, BULFIUS_UI_STATIC_FMT, 0, &callback_ui_get_static, NULL);
  // JavaScript/CSS
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_UI_JQUERY_MIN_JS_URL, NULL, 0, &callback_ui_get_static, BULFIUS_UI_JQUERY_MIN_JS);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_UI_JQUERY_UI_MIN_JS_URL, NULL, 0, &callback_ui_get_static, BULFIUS_UI_JQUERY_UI_MIN_JS);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_UI_JQUERY_UI_MIN_CSS_URL, NULL, 0, &callback_ui_get_static, BULFIUS_UI_JQUERY_UI_MIN_CSS);

  // POST
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_POST, BULFIUS_COMMAND, BULFIUS_COMMAND_FMT, 0, &callback_post_command, ctx);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_POST, BULFIUS_PARAMETERS, BULFIUS_PARAMETERS_FMT, 0, &callback_post_parameters, ctx);
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_POST, BULFIUS_SEQUENCE, NULL, 0, &callback_post_sequence, ctx);
  // POST/UI
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_POST, BULFIUS_UI_COMMAND, BULFIUS_COMMAND_FMT, 0, &callback_ui_post_command, ctx);

  // Websocket
  ulfius_add_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_UI_WEBSOCKET, NULL, 0, &callback_websocket, ctx);

  // Start the framework
  if (ulfius_start_framework(&ctx->instance) == U_OK) {
    VERBOSE(printf("[i] Started ulfius framework on port %d\n", ctx->instance.port));
  } else {
    xerror("Error starting ulfius framework on port %d\n", ctx->instance.port);
  }

  pthread_create(&ping_thread, NULL, ui_thread, (void *)ctx);
}


void
Context_stop_ulfius(Context_t *ctx)
{
  pthread_join(ping_thread, NULL);

  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_COLORMAP, NULL);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_COMMANDS, NULL);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_IMAGE, NULL);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_FRAME, NULL);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_PARAMETERS, BULFIUS_PARAMETERS_FMT);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_PLUGINS, NULL);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_REPLAY, NULL);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_SEQUENCE, NULL);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_STATISTICS, NULL);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_UI_ROOT, NULL);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_TESTING_ROOT, NULL);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_UI_STATIC, BULFIUS_UI_STATIC_FMT);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_POST, BULFIUS_COMMAND, BULFIUS_COMMAND_FMT);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_POST, BULFIUS_PARAMETERS, BULFIUS_PARAMETERS_FMT);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_POST, BULFIUS_SEQUENCE, NULL);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_POST, BULFIUS_UI_COMMAND, BULFIUS_COMMAND_FMT);
  ulfius_remove_endpoint_by_val(&ctx->instance, BULFIUS_GET, BULFIUS_UI_WEBSOCKET, NULL);

  ulfius_stop_framework(&ctx->instance);
  ulfius_clean_instance(&ctx->instance);
}

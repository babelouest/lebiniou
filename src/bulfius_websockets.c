/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "plugins.h"


static void
websocket_onclose_callback(const struct _u_request *request,
                           struct _websocket_manager *websocket_manager,
                           void *user_data)
{
  Context_t *ctx = (Context_t *)user_data;

#ifdef DEBUG_WS
  VERBOSE(printf("[i] %s (%s): end session for client: %p\n", __FILE__, __func__, websocket_manager));
#endif
  pthread_mutex_lock(&ctx->ws_clients_mutex);
  ctx->ws_clients = g_slist_remove(ctx->ws_clients, (gpointer)websocket_manager);
  pthread_mutex_unlock(&ctx->ws_clients_mutex);
}


static void
websocket_manager_callback(const struct _u_request *request,
                           struct _websocket_manager *websocket_manager,
                           void *user_data)
{
  Context_t *ctx = (Context_t *)user_data;
  int ret;

  pthread_mutex_lock(&ctx->ws_clients_mutex);
  ctx->ws_clients = g_slist_prepend(ctx->ws_clients, (gpointer)websocket_manager);
  pthread_mutex_unlock(&ctx->ws_clients_mutex);
#ifdef DEBUG_WS
  VERBOSE(printf("[i] %s (%s): new client: %p\n", __FILE__, __func__, websocket_manager));
#endif
  while (ctx->running && (ret = (ulfius_websocket_wait_close(websocket_manager, 2000) == U_WEBSOCKET_STATUS_OPEN))) {
#ifdef DEBUG_WS
    // VERBOSE(printf("[i] %s (%s): websocket_manager_callback: websocket still open: %s\n",
    // __FILE__, __func__, ret ? "true" : "false"));
#endif
  }
}


static void
process_json_payload(Context_t *ctx, const json_t *payload, const struct _websocket_manager *from)
{
  assert(NULL != ctx);
  json_t *cmd = json_object_get(payload, "command");

  if (NULL != cmd) {
    assert(json_is_string(cmd));
#ifdef DEBUG_WS
    VERBOSE(printf("[i] %s (%s) %d: JSON command '%s'\n", __FILE__, __func__, __LINE__, json_string_value(cmd)));
#endif
    const int command = str2command(json_string_value(cmd));

    if ((command != -1) && is_allowed(command)) {
      json_t *res = Context_process_command(ctx, (const enum Command)command, from, BC_WEB);

      if (NULL != res) {
#ifdef DEBUG_WS
        char *str = json_dumps(res, 0);
        VERBOSE(printf("[i] %s (%s) %d: JSON command '%s', res: '%s'\n", __FILE__, __func__, __LINE__, json_string_value(cmd), str));
        xfree(str);
#endif
        json_decref(res);
      }
    }
  } else {
    cmd = json_object_get(payload, "ui_command");
    if (NULL != cmd) {
      const json_t *arg = json_object_get(payload, "arg");

#ifdef DEBUG_WS
      if (json_is_string(arg)) {
        VERBOSE(printf("[i] %s (%s) %d: JSON ui_command %s, arg: %s\n", __FILE__, __func__, __LINE__, json_string_value(cmd), json_string_value(arg)));
      } else if (json_is_integer(arg)) {
        VERBOSE(printf("[i] %s (%s) %d: JSON ui_command %s, arg: %lld\n", __FILE__, __func__, __LINE__, json_string_value(cmd), json_integer_value(arg)));
      } else if (json_is_real(arg)) {
        VERBOSE(printf("[i] %s (%s) %d: JSON ui_command %s, arg: %f\n", __FILE__, __func__, __LINE__, json_string_value(cmd), json_real_value(arg)));
      } else if (json_is_null(arg)) {
        VERBOSE(printf("[i] %s (%s) %d: JSON ui_command %s, no argument\n", __FILE__, __func__, __LINE__, json_string_value(cmd)));
      } else if (json_is_boolean(arg)) {
        VERBOSE(printf("[i] %s (%s) %d: JSON ui_command %s, arg: %s\n", __FILE__, __func__, __LINE__, json_string_value(cmd), json_boolean_value(arg) ? "true" : "false"));
      } else if (json_is_array(arg)) {
        char *str = json_dumps(arg, 0);
        VERBOSE(printf("[i] %s (%s) %d: JSON ui_command %s, arg: %s\n", __FILE__, __func__, __LINE__, json_string_value(cmd), str));
        xfree(str);
      } else if (json_is_object(arg)) {
        char *str = json_dumps(arg, 0);
        VERBOSE(printf("[i] %s (%s) %d: JSON ui_command %s, arg: '%s'\n", __FILE__, __func__, __LINE__, json_string_value(cmd), str));
        xfree(str);
      } else {
        xerror("Unexpected argument type\n");
      }
#endif

      const int command = str2command(json_string_value(cmd));

      if ((command != -1) && is_allowed(command)) {
        json_t *res = Context_process_ui_command(ctx, (const enum Command)command, arg, BC_WEB);

        if (NULL == res) {
          json_t *json = json_pack("{ssss}", "ui_command", command2str(command), "result", "ok");
#ifndef ULFIUS_GE_2_6_9
          char *str = json_dumps(json, 0);
#endif
#ifdef DEBUG_WS
          char *str1 = json_dumps(json, 0);
          char *str2 = json_dumps(arg, 0);
          VERBOSE(printf("[i] %s (%s) %d: JSON ui_command result '%s', arg: '%s': %s\n", __FILE__, __func__, __LINE__, json_string_value(cmd), str1, str2));
          xfree(str2);
          xfree(str1);
#endif
          pthread_mutex_lock(&ctx->ws_clients_mutex);
          for (GSList *client = ctx->ws_clients; NULL != client; client = client->next) {
#ifdef ULFIUS_GE_2_6_9
            ulfius_websocket_send_json_message((struct _websocket_manager *)client->data, json);
#else
            ulfius_websocket_send_message((struct _websocket_manager *)client->data, U_WEBSOCKET_OPCODE_TEXT, strlen(str), str);
#endif
          }
          pthread_mutex_unlock(&ctx->ws_clients_mutex);
#ifndef ULFIUS_GE_2_6_9
          xfree(str);
#endif
          json_decref(json);
        } else {
          json_t *json = json_pack("{ssso}", "ui_command", command2str(command), "result", res);
#ifndef ULFIUS_GE_2_6_9
          char *js_str = json_dumps(res, JSON_COMPACT);
          char *str = g_strdup_printf("{ \"ui_command\": \"%s\", \"result\": %s }", command2str(command), js_str);
          xfree(js_str);
#endif
#ifdef DEBUG_WS
          char *str1 = json_dumps(arg, 0);
          char *str2 = json_dumps(res, 0);
          VERBOSE(printf("[i] %s (%s) %d: JSON ui_command result '%s', arg: '%s': %s\n", __FILE__, __func__, __LINE__, json_string_value(cmd), str1, str2));
          xfree(str2);
          xfree(str1);
#endif
          pthread_mutex_lock(&ctx->ws_clients_mutex);
          for (GSList *client = ctx->ws_clients; NULL != client; client = client->next) {
#ifdef ULFIUS_GE_2_6_9
            ulfius_websocket_send_json_message((struct _websocket_manager *)client->data, json);
#else
            ulfius_websocket_send_message((struct _websocket_manager *)client->data, U_WEBSOCKET_OPCODE_TEXT, strlen(str), str);
#endif
          }
          pthread_mutex_unlock(&ctx->ws_clients_mutex);
#ifndef ULFIUS_GE_2_6_9
          xfree(str);
#endif
          json_decref(json);
        }
      }
    }
  }
#ifdef DEBUG_WS
  printf("\n");
#endif
}


static void
websocket_incoming_message_callback(const struct _u_request *request,
                                    struct _websocket_manager *websocket_manager,
                                    const struct _websocket_message* last_message,
                                    void *user_data)
{
  Context_t *ctx = (Context_t *)user_data;

  if (last_message->opcode == U_WEBSOCKET_OPCODE_TEXT) {
#ifdef DEBUG_WS
    // VERBOSE(printf("[i] %s (%s): text payload '%.*s'\n", __FILE__, __func__, (int)last_message->data_len, last_message->data));
#endif
    json_t *payload = json_loadb(last_message->data, last_message->data_len, 0, NULL);

    if (NULL != payload) {
      SequenceManager_lock(ctx->sm);
      process_json_payload(ctx, payload, websocket_manager);
      SequenceManager_unlock(ctx->sm);
      json_decref(payload);
    }
  } else if (last_message->opcode == U_WEBSOCKET_OPCODE_BINARY) {
#ifdef DEBUG_WS
    VERBOSE(printf("[i] %s (%s): binary payload\n", __FILE__, __func__));
#endif
  } else if (last_message->opcode == U_WEBSOCKET_OPCODE_CLOSE) {
#ifdef DEBUG_WS
    VERBOSE(printf("[i] %s (%s): got a CLOSE message\n", __FILE__, __func__));
#endif
  } else if (last_message->opcode == U_WEBSOCKET_OPCODE_PING) {
#ifdef DEBUG_WS
    VERBOSE(printf("[i] %s (%s): got a PING message\n", __FILE__, __func__));
#endif
  } else if (last_message->opcode == U_WEBSOCKET_OPCODE_PONG) {
#ifdef DEBUG_WS
    VERBOSE(printf("[i] %s (%s): got a PONG message\n", __FILE__, __func__));
#endif
  }
}


int
callback_websocket(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  int ret;

  if ((ret = ulfius_set_websocket_response(response, NULL, NULL,
             &websocket_manager_callback, user_data,
             &websocket_incoming_message_callback, user_data,
             &websocket_onclose_callback, user_data)) == U_OK) {
    return U_CALLBACK_CONTINUE;
  } else {
    return U_CALLBACK_ERROR;
  }
}

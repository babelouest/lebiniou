/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_RGBA_H
#define __BINIOU_RGBA_H

#include "utils.h"


/*!
 * \struct RGBA_t
 * \brief RGBA color, as four uint8_t
 */
typedef struct _rgba_s {
  uint8_t r;
  uint8_t g;
  uint8_t b;
  uint8_t a;
} RGBA_t;


/*!
 * \union rgba_t
 * \brief RGBA color as a union
 */
typedef union _rgba_u {
  RGBA_t  col;
  uint8_t rgbav[4];
} rgba_t;

#endif /* __BINIOU_RGBA_H */

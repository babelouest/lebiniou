/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "sequence.h"
#include "globals.h"


void
Sequence_copy(struct Context_s *ctx, const Sequence_t *from, Sequence_t *to)
{
  GList *tmp;

  to->id = from->id;

  if (NULL != to->name) {
    xfree(to->name);
  }

  if (NULL != from->name) {
    to->name = strdup(from->name);
  }

  tmp = g_list_first(to->layers);
  while (NULL != tmp) {
    Layer_t *l = (Layer_t *)tmp->data;

    Layer_delete(l);
    tmp = g_list_next(tmp);
  }
  g_list_free(to->layers);
  to->layers = NULL;

  tmp = g_list_first(from->layers);
  while (NULL != tmp) {
    Layer_t *lfrom = (Layer_t *)tmp->data;
    Layer_t *lto = Layer_copy(lfrom);

    to->layers = g_list_append(to->layers, (gpointer)lto);
    tmp = g_list_next(tmp);
  }

  to->lens = from->lens;

  to->image_id = from->image_id;
  to->auto_images = from->auto_images;

  to->cmap_id = from->cmap_id;
  to->auto_colormaps = from->auto_colormaps;

  if (NULL != to->params3d) {
    json_decref(to->params3d);
  }
  to->params3d = (NULL != from->params3d) ? json_deep_copy(from->params3d) : NULL;
}


gint
Sequence_sort_func(gconstpointer a, gconstpointer b)
{
  const Sequence_t *sa = (const Sequence_t *)a;
  const Sequence_t *sb = (const Sequence_t *)b;

  assert(NULL != sa);
  assert(NULL != sb);

  return (sa->id - sb->id);
}


void
Sequence_insert(Sequence_t *s, Plugin_t *p, const uint8_t random_layer)
{
  Layer_t *layer = Layer_new(p);

  assert(NULL != p);
  /* set layer mode */
  if (NULL != p->mode) {
    if (random_layer) {
      enum LayerMode mode = b_rand_uint32_range(LM_NORMAL, NB_LAYER_MODES);

#ifdef DEBUG_RANDOM_LAYERS
      assert(mode >= LM_NORMAL);
      assert(mode <= LM_RANDOM);
      printf("[s] Setting layer mode for %s to %s\n", p->name, LayerMode_to_string(mode));
#endif
      layer->mode = mode;
    } else {
      assert(*(p->mode) < NB_LAYER_MODES);
      layer->mode = (enum LayerMode)*(p->mode);
    }
  }

  /* insert plugin in sequence */
  if (*p->options & BO_FIRST) {
    s->layers = g_list_prepend(s->layers, (gpointer)layer);
  } else if (*p->options & BO_LAST) {
    /* insert before lens if any, or at the end if no lens */
    GList *lens = (NULL != s->lens) ? Sequence_find(s, s->lens) : NULL;
    s->layers = g_list_insert_before(s->layers, lens, (gpointer)layer);
  } else {
    /* add to the end anyway if nothing specified */
    s->layers = g_list_append(s->layers, (gpointer)layer);
  }

  /* set as lens if it's a lens, and there is no lens yet */
  if ((*p->options & BO_LENS) && (NULL == s->lens)) {
    s->lens = (Plugin_t *)p;
  }

  Sequence_changed(s);
}


void
Sequence_insert_at_position(Sequence_t *s, const uint16_t position, Plugin_t *p) /* TODO LayerMode ? */
{
  Layer_t *layer = Layer_new(p);

  /* set layer mode */
  if (NULL != p->mode) {
    layer->mode = (enum LayerMode)*p->mode;
  }

  /* insert plugin in sequence */
  s->layers = g_list_insert(s->layers, (gpointer)layer, position);

  /* set as lens if it's a lens, and there is no lens yet */
  if ((*p->options & BO_LENS) && (NULL == s->lens)) {
    s->lens = (Plugin_t *)p;
  }

  Sequence_changed(s);
}


void
Sequence_remove(Sequence_t *s, const Plugin_t *p)
{
  const GList *p_layer = Sequence_find(s, p);
  Layer_t *layer = (Layer_t *)p_layer->data;

  /* remove plugin from sequence */
  Layer_delete(layer);

  s->layers = g_list_remove(s->layers, (gconstpointer)layer);

  /* unset if it is a lens we removed */
  if (s->lens == p) {
    s->lens = NULL;
  }

  Sequence_changed(s);
}


uint8_t
Sequence_size(const Sequence_t *seq)
{
  return g_list_length(seq->layers);
}


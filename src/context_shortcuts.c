/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


void
Context_save_shortcuts(const Context_t *ctx)
{
  const gchar *home_dir = NULL;
  char *filename = NULL;
  json_t *shortcuts = json_array();
#ifdef DEBUG_SHORTCUTS
  uint8_t c_shortcuts = 0;
  uint8_t i_shortcuts = 0;
#endif

  // create ~/.lebiniou directory (be safe if it doesn't exist)
  home_dir = g_get_home_dir();
  filename = g_strdup_printf("%s/." PACKAGE_NAME, home_dir);
  rmkdir(filename);
  g_free(filename);

  filename = g_strdup_printf("%s/." PACKAGE_NAME "/shortcuts.json", home_dir);
  printf("[s] Writing shortcuts to: %s\n", filename);

  for (uint8_t i = 0; i < MAX_SHORTCUTS; i++) {
    uint32_t cid = ctx->shortcuts[SH_COLORMAP][i];
    uint32_t iid = ctx->shortcuts[SH_IMAGE][i];

    if (cid || iid) {
      json_t *s = json_object();

      json_object_set_new(s, "shortcut", json_integer(i));
      if (cid) {
        json_object_set_new(s, "colormap_id", json_integer(cid));
#ifdef DEBUG_SHORTCUTS
        printf("[C] Store colormap shortcut %d: %u\n", i, cid);
        c_shortcuts++;
#endif
      }
      if (iid) {
        json_object_set_new(s, "image_id", json_integer(iid));
#ifdef DEBUG_SHORTCUTS
        printf("[I] Store image shortcut %d: %u\n", i, iid);
        i_shortcuts++;
#endif
      }
      json_array_append_new(shortcuts, s);
    }
  }
  json_dump_file(shortcuts, filename, JSON_INDENT(4));
#ifdef DEBUG_SHORTCUTS
  const uint8_t total = c_shortcuts + i_shortcuts;
  printf("[S] Total %d shortcuts: %d colormaps, %d images\n", total, c_shortcuts, i_shortcuts);
#endif
  json_decref(shortcuts);
  g_free(filename);
}


void
Context_load_shortcuts(Context_t *ctx)
{
  const gchar *home_dir = NULL;
  char *filename;
  int res;
  struct stat dummy;
#ifdef DEBUG_SHORTCUTS
  uint8_t c_shortcuts = 0;
  uint8_t i_shortcuts = 0;
#endif

  home_dir = g_get_home_dir();
  filename = g_strdup_printf("%s/." PACKAGE_NAME "/shortcuts.json", home_dir);
  res = stat(filename, &dummy);

  if (!res) {
    json_t *j_shortcuts = json_load_file(filename, 0, NULL);

    for (uint8_t i = 0; i < json_array_size(j_shortcuts); i++) {
      const json_t *s = json_array_get(j_shortcuts, i);
      const uint8_t id = json_integer_value(json_object_get(s, "shortcut"));
      const json_t *j_cmap = json_object_get(s, "colormap_id");
      const json_t *j_image = json_object_get(s, "image_id");

      assert(id < MAX_SHORTCUTS);

      if (NULL != j_cmap) {
        const uint32_t cid = json_integer_value(j_cmap);
        ctx->shortcuts[SH_COLORMAP][id] = cid;
#ifdef DEBUG_SHORTCUTS
        printf("[C] Read colormap shortcut %d: %u\n", id, cid);
        c_shortcuts++;
#endif
      }
      if (NULL != j_image) {
        const uint32_t iid = json_integer_value(j_image);
        ctx->shortcuts[SH_IMAGE][id] = iid;
#ifdef DEBUG_SHORTCUTS
        printf("[I] Read image shortcut %d: %u\n", id, iid);
        i_shortcuts++;
#endif
      }
    }
#ifdef DEBUG_SHORTCUTS
    const uint8_t total = c_shortcuts + i_shortcuts;
    printf("[S] Total %d shortcuts: %d colormaps, %d images\n", total, c_shortcuts, i_shortcuts);
#endif
    json_decref(j_shortcuts);
  }
  g_free(filename);
}


static json_t *get_shortcuts(const Context_t *ctx, const enum Shortcuts s)
{
  json_t *res = json_array();

  for (uint8_t i = 0; i < MAX_SHORTCUTS; i++) {
    json_array_append_new(res, ctx->shortcuts[s][i] ? json_integer(ctx->shortcuts[s][i]) : json_null());
  }

  return res;
}


json_t *
Context_get_shortcuts(const Context_t *ctx, const char *type)
{
  json_t *res = NULL;

  if (NULL != type) {
    if (is_equal(type, "COL")) {
      res = json_object();
      json_object_set_new(res, "type", json_string(type));
      json_object_set_new(res, "shortcuts", get_shortcuts(ctx, SH_COLORMAP));
    } else if (is_equal(type, "IMG")) {
      res = json_object();
      json_object_set_new(res, "type", json_string(type));
      json_object_set_new(res, "shortcuts", get_shortcuts(ctx, SH_IMAGE));
    }
  }

  return res;
}

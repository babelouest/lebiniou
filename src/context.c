/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"
#include "context.h"
#include "brandom.h"
#include "colormaps.h"
#include "images.h"
#include "sequences.h"
#include "defaults.h"
#include "biniou.h"

extern uint64_t frames;
extern uint8_t max_fps;
// start mp4 encoding automatically
extern uint8_t encoding;
// video plugin
extern int override_webcam;
// playlist
extern char *playlist_filename;
// legacy SDL2 GUI
#ifdef WITH_ULFIUS
extern uint8_t usage_statistics;
#endif


static enum ShufflerMode random_modes[MAX_TIMERS] = {
  BS_RANDOM,  // colormaps
  BS_RANDOM,  // images
  BS_SHUFFLE  // sequences
#ifdef WITH_WEBCAM
  , BS_CYCLE  // webcams
#endif
};


#ifdef WITH_WEBCAM
#include "webcam.h"

extern char *video_base;

static webcam_t *cams[MAX_CAMS];
static pthread_t thr[MAX_CAMS];

#define MAX_TRIES 5


static void
Context_open_webcam(Context_t *ctx)
{
  int i;
  uint8_t try = 0;

  parse_options();

  for (i = 0; i < MAX_CAMS; i++) {
    pthread_mutex_init(&ctx->cam_mtx[i], NULL);
  }

  for (i = 0; (i < ctx->webcams) && (try < MAX_TRIES); i++) {
retry:
      cams[i] = xmalloc(sizeof(webcam_t));

      cams[i]->io = IO_METHOD_MMAP;
      cams[i]->fd = -1;
      cams[i]->ctx = ctx;
      cams[i]->cam_no = i;

      if (-1 != open_device(cams[i], try)) {
        if (-1 != init_device(cams[i])) {
          enumerate_cids(cams[i]);
          list_inputs(cams[i]);
          start_capturing(cams[i]);

          pthread_create(&thr[i], NULL, loop, (void *)cams[i]);
        } else {
          VERBOSE(printf("[i] Webcam: failed to initialize device #%d\n", i));
          close_device(cams[i]);
          xfree(cams[i]);
          try++;
          goto retry;
        }
      } else {
        xfree(cams[i]);
        ctx->webcams--;
      }
    }
}
#endif


Context_t *
Context_new(const int webcams)
{
  Context_t *ctx = xcalloc(1, sizeof(Context_t));

  ctx->running = 1;
  pthread_mutex_init(&ctx->frame_mutex, NULL);

  VERBOSE(printf("[+] Creating buffers... "));
  for (uint8_t i = 0; i < NSCREENS; i++) {
    VERBOSE(printf("%d ", i));
    ctx->buffers[i] = Buffer8_new();
  }

  ctx->webcams = webcams;
#ifdef WITH_WEBCAM
  {
    for (uint8_t k = 0; k < webcams; k++) {
      for (uint8_t i = 0; i < CAM_SAVE; i++) {
        ctx->cam_save[k][i] = Buffer8_new();
      }
      ctx->cam_ref[k] = Buffer8_new();
      ctx->cam_ref0[k] = Buffer8_new();
    }
  }
#endif
  ctx->rgba_buffers[ACTIVE_BUFFER] = BufferRGBA_new();
  VERBOSE(printf("\n"));

#if WITH_GL
  glGenTextures(NSCREENS, ctx->textures); // TODO: delete on exit
  glGenTextures(MAX_CAMS, ctx->cam_textures); // TODO: delete on exit
#endif

  VERBOSE(printf("[+] Initializing sequence manager\n"));
  ctx->sm = SequenceManager_new();

  Context_load_banks(ctx);
  Context_load_shortcuts(ctx);

  VERBOSE(printf("[+] Initializing 3D engine\n"));
  Params3d_init(&ctx->params3d);

  ctx->timer = b_timer_new("timer");
  ctx->fps_timer = b_timer_new("fps_timer");

  ctx->target_pic = Image8_new();

  extern char *data_dir;
  gchar *tmp;

  /* load target pic */
  tmp = g_strdup_printf("%s/%s", data_dir, "images/z-biniou-tv-1.png");
  VERBOSE(printf("[+] Loading '%s'\n", tmp));
  int res = Image8_load_any(ctx->target_pic, tmp);
  g_free(tmp);
  if (res == -1) {
    Buffer8_randomize(ctx->target_pic->buff);
  }

#ifdef WITH_WEBCAM
  VERBOSE(printf("[i] Initializing %d webcams base: %s\n", webcams, video_base));

  Context_open_webcam(ctx);

  for (uint8_t i = 0; i < webcams; i++) {
    Buffer8_copy(ctx->target_pic->buff, ctx->cam_save[i][0]);
  }

  if (webcams > 1) {
    VERBOSE(printf("[+] Creating webcams shuffler (%d webcams)\n", webcams));
    ctx->webcams_shuffler = Shuffler_new(webcams);
    Shuffler_set_mode(ctx->webcams_shuffler, Context_get_shuffler_mode(BD_WEBCAMS));
#ifdef DEBUG
    Shuffler_verbose(ctx->webcams_shuffler);
#endif
  }
#endif

  ctx->random = Buffer8_new();
  for (uint32_t i = 0; i < BUFFSIZE; i++) {
    ctx->random->buffer[i] = b_rand_boolean();
  }

  Context_init_replay(ctx);

  // Video plugin
  for (uint8_t i = 0; i < CAM_SAVE; i++) {
    ctx->video_save[i] = Buffer8_new();
  }

  ctx->allow_random_changes = 1;

  return ctx;
}


#ifdef WITH_WEBCAM
static void
Context_close_webcam(const uint8_t cam_no)
{
  if (NULL != cams[cam_no]) {
    pthread_join(thr[cam_no], NULL);
    stop_capturing(cams[cam_no]);
    uninit_device(cams[cam_no]);
    close_device(cams[cam_no]);
    xfree(cams[cam_no]);
  }
}
#endif


void
Context_delete(Context_t *ctx)
{
  int i;
  GSList *outputs = ctx->outputs, *_outputs = outputs;

#ifdef WITH_ULFIUS
  if (usage_statistics) {
    Context_statistics(ctx);
  }
#endif

#ifdef WITH_WEBCAM
  VERBOSE(printf("[i] Closing %d webcams\n", ctx->webcams));
  if (ctx->webcams) {
    int i;

    for (i = 0; i < ctx->webcams; i++) {
      Context_close_webcam(i);
    }
  }
  xfree(video_base);
  if (ctx->webcams > 1) {
    Shuffler_delete(ctx->webcams_shuffler);
    Alarm_delete(ctx->a_webcams);
  }
#endif

  VERBOSE(printf("[i] %"PRIu64" frames\n", frames));

  if (NULL != ctx->input_plugin) {
    Plugin_delete(ctx->input_plugin);
  }

  for ( ; NULL != outputs; outputs = g_slist_next(outputs)) {
    Plugin_t *output = (Plugin_t *)outputs->data;

    Plugin_delete(output);
  }
  g_slist_free(_outputs);

  VERBOSE(printf("[+] Freeing buffers... "));
  for (i = 0; i < NSCREENS; i++) {
    VERBOSE(printf("%d ", i));
    Buffer8_delete(ctx->buffers[i]);
  }
#ifdef WITH_WEBCAM
  {
    int k;
    for (k = 0; k < ctx->webcams; k++) {
      for (i = 0; i < CAM_SAVE; i++) {
        Buffer8_delete(ctx->cam_save[k][i]);
      }
      Buffer8_delete(ctx->cam_ref[k]);
      Buffer8_delete(ctx->cam_ref0[k]);
    }
  }
#endif
  BufferRGBA_delete(ctx->rgba_buffers[ACTIVE_BUFFER]);
  VERBOSE(printf("\n"));

  if (NULL != ctx->imgf) {
    VERBOSE(printf("[+] Freeing images fader\n"));
    ImageFader_delete(ctx->imgf);

    VERBOSE(printf("[+] Freeing images timer\n"));
    Alarm_delete(ctx->a_images);
  }

  if (NULL != ctx->cf) {
    VERBOSE(printf("[+] Freeing colormaps fader\n"));
    CmapFader_delete(ctx->cf);

    VERBOSE(printf("[+] Freeing colormaps timer\n"));
    Alarm_delete(ctx->a_cmaps);
  }

  Alarm_delete(ctx->a_random);
  SequenceManager_delete(ctx->sm);
  b_timer_delete(ctx->timer);
  b_timer_delete(ctx->fps_timer);

  if (NULL != ctx->target_pic) {
    Image8_delete(ctx->target_pic);
  }

  Buffer8_delete(ctx->random);

  Context_free_replay(ctx);

  json_decref(ctx->playlist);
  b_timer_delete(ctx->track_timer);
  Shuffler_delete(ctx->playlist_shuffler);

  for (i = 0; i < CAM_SAVE; i++) {
    Buffer8_delete(ctx->video_save[i]);
  }

  xfree(ctx->frame);
  xfree(ctx);
}


void
Context_set_colormap(Context_t *ctx)
{
  /* have to change auto_colormaps ? */
  if (ctx->sm->next->auto_colormaps != -1) {
    /* find the cmap, if any. default cmap otherwise */
    if (NULL != ctx->cf) {
      ctx->cf->fader->target
        = (ctx->sm->next->cmap_id) ?
          Colormaps_index(ctx->sm->next->cmap_id) : 0;
      CmapFader_set(ctx->cf);
    }
    ctx->auto_colormaps = ctx->sm->next->auto_colormaps;
  } else {
#ifdef DEBUG
    printf("%s: leaving auto_colormaps unchanged\n", __FILE__);
#endif
    ctx->sm->next->cmap_id = ctx->sm->cur->cmap_id;
  }
}


void
Context_set_image(Context_t *ctx)
{
  /* have to change auto_images ? */
  if (ctx->sm->next->auto_images != -1) {
    /* find the image, if any. default image otherwise */
    if (NULL != ctx->imgf) {
      ctx->imgf->fader->target
        = (ctx->sm->next->image_id) ?
          Images_index(ctx->sm->next->image_id) : 0;
      ImageFader_set(ctx->imgf);
    }
    ctx->auto_images = ctx->sm->next->auto_images;
  } else {
#ifdef DEBUG
    printf("%s: leaving auto_images unchanged\n", __FILE__);
#endif
    ctx->sm->next->image_id = ctx->sm->cur->image_id;
  }
}


void
Context_update_auto(Context_t *ctx)
{
  /* figure out auto_colormaps */
  if (NULL != ctx->cf) {
    if (ctx->sm->cur->auto_colormaps == -1) { // using a bare sequence
      ctx->cf->on = ctx->auto_colormaps;
    } else { // using a full sequence
      ctx->cf->on = ctx->sm->cur->auto_colormaps;
    }

    if (ctx->cf->on && (colormaps->size > 1)) {
      /* select random colormap and reinitialize timer */
      CmapFader_random(ctx->cf);
      Alarm_init(ctx->a_cmaps);
    }

    /* set as current */
    ctx->auto_colormaps = ctx->cf->on;
  }

  /* figure out auto_images */
  if (NULL != ctx->imgf) {
    if (ctx->sm->cur->auto_images == -1) { // using a bare sequence
      ctx->imgf->on = ctx->auto_images;
    } else { // using a full sequence
      ctx->imgf->on = ctx->sm->cur->auto_images;
    }

    if (ctx->imgf->on && (NULL != images) && (images->size > 1)) {
      /* select random image and reinitialize timer */
      ImageFader_random(ctx->imgf);
      Alarm_init(ctx->a_images);
    }

    /* set as current */
    ctx->auto_images = ctx->imgf->on;
  }
}


int
Context_add_rand(Sequence_t *seq, const enum PluginOptions options,
                 const uint8_t not_lens, const Plugin_t *locked, const uint8_t randomize_layers)
{
  Plugin_t *p;

  do {
    p = Plugins_get_random(options, locked);
    if (NULL == p) {
      return -1;
    }
  } while (NULL != Sequence_find(seq, p));

  Sequence_insert(seq, p, randomize_layers);

  if ((*p->options & BO_LENS) && not_lens && (NULL != seq->lens)) {
    seq->lens = NULL;
  }

  return 0;
}


void
Context_randomize(Context_t *ctx)
{
  Sequence_t *new = ctx->sm->next;
  int rand;

  /* Use random image */
  if (NULL != images) {
    if (images->size > 1) {
      rand = b_rand_uint32_range(0, images->size-1);
      ctx->auto_images = new->auto_images = b_rand_boolean();
    } else {
      rand = new->auto_images = 0;
    }
    new->image_id = images->imgs[rand]->id;
  }

  /* Use random colormap */
  assert(NULL != colormaps);
  if (colormaps->size > 1) {
    rand = b_rand_uint32_range(0, colormaps->size-1);
    ctx->auto_colormaps = new->auto_colormaps = b_rand_boolean();
  } else {
    rand = new->auto_colormaps = 0;
  }
  new->cmap_id = colormaps->cmaps[rand]->id;

  /* Set or reset 3D rotations */
  if (b_rand_boolean()) {
    Params3d_randomize(&ctx->params3d);
  } else {
    zero_3d(&ctx->params3d);
  }
}


void
Context_insert_plugin(Context_t *ctx, Plugin_t *p)
{
  /* switch the plugin on */
  if (NULL != p->on_switch_on) {
    VERBOSE(printf("[i] on_switch_on '%s' (%s)\n", p->name, p->dname));
    p->on_switch_on(ctx);
  }

  Sequence_insert(ctx->sm->cur, p, 0);
}


void
Context_remove_plugin(Context_t *ctx, Plugin_t *p)
{
  /* switch the plugin off */
  if (NULL != p->on_switch_off) {
    VERBOSE(printf("[i] on_switch_off '%s' (%s)\n", p->name, p->dname));
    p->on_switch_off(ctx);
  }

  Sequence_remove(ctx->sm->cur, p);
}


void
Context_set_max_fps(Context_t *ctx, const uint8_t _max_fps)
{
  assert(_max_fps);
  max_fps = _max_fps;
  ctx->i_max_fps = 1.0 / max_fps;
}


void
Context_set_engine_random_mode(Context_t *ctx, const enum RandomMode r)
{
  ctx->random_mode = r;
}


float
Context_fps(const Context_t *ctx)
{
  float mfps = 0.0;
  int i;

  for (i = 0; i < NFPS; i++) {
    mfps += ctx->fps[i];
  }
  return (mfps / (float)NFPS);
}


void
Context_first_sequence(Context_t *ctx)
{
  if (NULL != ctx->sm->curseq) {
    Sequence_t *s;

    ctx->sm->curseq = g_list_first(sequences->seqs);
    s = (Sequence_t *)ctx->sm->curseq->data;
    Sequence_copy(ctx, s, ctx->sm->next);

    Context_set(ctx);
  }
}


void
Context_previous_sequence(Context_t *ctx)
{
  if (NULL != ctx->sm->curseq) {
    Sequence_t *s;

    if (NULL != ctx->sm->curseq->prev) {
      ctx->sm->curseq = ctx->sm->curseq->prev;
    } else {
      ctx->sm->curseq = g_list_last(sequences->seqs);
    }

    s = (Sequence_t *)ctx->sm->curseq->data;
    Sequence_copy(ctx, s, ctx->sm->next);

    Context_set(ctx);
  }
}


void
Context_next_sequence(Context_t *ctx)
{
  if (NULL != ctx->sm->curseq) {
    Sequence_t *s;

    if (NULL != ctx->sm->curseq->next) {
      ctx->sm->curseq = ctx->sm->curseq->next;
    } else {
      ctx->sm->curseq = g_list_first(sequences->seqs);
    }

    s = (Sequence_t *)ctx->sm->curseq->data;
    Sequence_copy(ctx, s, ctx->sm->next);

    Context_set(ctx);
  }
}


void
Context_last_sequence(Context_t *ctx)
{
  if (NULL != ctx->sm->curseq) {
    Sequence_t *s;

    ctx->sm->curseq = g_list_last(sequences->seqs);

    s = (Sequence_t *)ctx->sm->curseq->data;
    Sequence_copy(ctx, s, ctx->sm->next);

    Context_set(ctx);
  }
}


void
Context_random_sequence(Context_t *ctx)
{
  uint16_t rand;
  GList *tmp;

  rand = Shuffler_get(sequences->shuffler);

  tmp = g_list_nth(sequences->seqs, rand);
  assert(NULL != tmp);

  VERBOSE(printf("[s] Random sequence: %d\n", rand));

  Sequence_copy(ctx, tmp->data, ctx->sm->next);
  Context_set(ctx);
}


void
Context_set_sequence(Context_t *ctx, const uint32_t id)
{
  GList *seq = Sequences_find(id);

  if (NULL != seq) {
    Sequence_t *s = (Sequence_t *)seq->data;
    ctx->sm->curseq = seq;
    Sequence_copy(ctx, s, ctx->sm->next);

    Context_set(ctx);
  }
}


Buffer8_t *
active_buffer(const Context_t *ctx)
{
  return ctx->buffers[ACTIVE_BUFFER];
}


Buffer8_t *
passive_buffer(const Context_t *ctx)
{
  return ctx->buffers[PASSIVE_BUFFER];
}


#ifdef WITH_WEBCAM
void
Context_push_webcam(Context_t *ctx, Buffer8_t *buff, const int cam)
{
  int i;

  Buffer8_delete(ctx->cam_save[cam][CAM_SAVE-1]);
  for (i = CAM_SAVE-1; i >= 1; i--) {
    ctx->cam_save[cam][i] = ctx->cam_save[cam][i-1];
  }
  ctx->cam_save[cam][0] = buff;
}
#endif


void
Context_set_shuffler_mode(const enum RandomDelays what, const enum ShufflerMode mode)
{
  random_modes[what] = mode;
}


enum ShufflerMode
Context_get_shuffler_mode(const enum RandomDelays what) {
  return random_modes[what];
}


void
Context_set_input_size(Context_t *ctx, const uint32_t input_size)
{
  ctx->input_size = input_size;
}


uint32_t
Context_get_input_size(const Context_t *ctx)
{
  return ctx->input_size;
}


void
Context_set_volume_scale(Context_t *ctx, const double volume_scale)
{
  if (NULL != ctx->input) {
    ctx->input->volume_scale = volume_scale;
  }
}


double
Context_get_volume_scale(const Context_t *ctx)
{
  return (NULL != ctx->input) ? ctx->input->volume_scale : 0;
}


void
Context_mix_buffers(const Context_t *ctx, Buffer8_t *buffs[2])
{
  Pixel_t *d = buffs[0]->buffer;

  uint32_t rnd_offset = b_rand_uint32_range(0, BUFFSIZE-1);
  const Pixel_t *random = ctx->random->buffer + rnd_offset;

  uint32_t i = 0;
  for (; i < BUFFSIZE - rnd_offset; i++, d++, random++) {
    *d = buffs[*random]->buffer[i];
  }
  random = ctx->random->buffer;
  for (; i < BUFFSIZE; i++, d++, random++) {
    *d = buffs[*random]->buffer[i];
  }
}


void
Context_interleave_buffers(const Context_t *ctx)
{
  const Pixel_t *src = passive_buffer(ctx)->buffer;
  Pixel_t *dst = active_buffer(ctx)->buffer;

  for (uint16_t l = 0; l < HEIGHT; l += 2) {
    memcpy(dst, src, WIDTH * sizeof(Pixel_t));
    src += 2 * WIDTH * sizeof(Pixel_t);
    dst += 2 * WIDTH * sizeof(Pixel_t);
  }
}


json_t *
Context_output_plugins(const Context_t *ctx)
{
  if (NULL == ctx->outputs) {
    return json_null();
  } else {
    json_t *a = json_array();

    for (GSList *outputs = ctx->outputs; NULL != outputs; outputs = g_slist_next(outputs)) {
      Plugin_t *output = (Plugin_t *)outputs->data;

      json_array_append_new(a, json_string(output->name));
    }

    return a;
  }
}


void
Context_push_video(Context_t *ctx, Buffer8_t *buff)
{
  int i;

  Buffer8_delete(ctx->video_save[CAM_SAVE-1]);
  for (i = CAM_SAVE-1; i >= 1; i--) {
    ctx->video_save[i] = ctx->video_save[i-1];
  }
  ctx->video_save[0] = buff;
}

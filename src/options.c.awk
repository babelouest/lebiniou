BEGIN {
    print "/*";
    print " *  Copyright 1994-2020 Olivier Girondel";
    print " *";
    print " *  This file is part of lebiniou.";
    print " *";
    print " *  lebiniou is free software: you can redistribute it and/or modify";
    print " *  it under the terms of the GNU General Public License as published by";
    print " *  the Free Software Foundation, either version 2 of the License, or";
    print " *  (at your option) any later version.";
    print " *";
    print " *  lebiniou is distributed in the hope that it will be useful,";
    print " *  but WITHOUT ANY WARRANTY; without even the implied warranty of";
    print " *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the";
    print " *  GNU General Public License for more details.";
    print " *";
    print " *  You should have received a copy of the GNU General Public License";
    print " *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.";
    print " */";
    print;
    print "#include \"options.h\"";
    print;
    print "/*";
    print " * Automagically generated from options.h.in";
    print " * DO NOT EDIT !!!";
    print " */";
    print;
    print;
    print "PluginType_t pTypes[MAX_OPTIONS] = {";
    print "  { BO_NONE, 0 },";

    i = 1;
}


{
    printf "  { %s, 0 },\n", $1;

    i = i + 1;
}


END {
    print "};";
}

/*
 *  Copyright 1994-2020 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "plugins.h"


extern uint64_t frames;


int
callback_get_parameters(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  Context_t *ctx = user_data;
  const char *plugin = u_map_get(request->map_url, "plugin");

  assert(NULL != ctx);

  if (NULL != plugin) {
    Plugin_t *p = Plugins_find(plugin);
    json_t *res = NULL;

#ifdef DEBUG_BULFIUS_GET
    printf("%s: %s Plugin %s: %p\n", __FILE__, __func__, plugin, p);
#endif
    if (NULL != p) {
      if (NULL != p->parameters) {
#ifdef DEBUG_BULFIUS_GET
        printf("%s: %s Plugin %s has parameters\n", __FILE__, __func__, plugin);
#endif
        res = p->parameters(ctx, NULL);
        const char *param_name = u_map_get(request->map_url, "name");

        if (NULL != param_name) {
#ifdef DEBUG_BULFIUS_GET
          printf("%s: %s Plugin %s has parameter %s\n", __FILE__, __func__, plugin, param_name);
#endif
          json_t *new_res = NULL;
          json_t *param = json_object_get(res, param_name);

          if (NULL != param) {
            new_res = json_object();
            json_object_set_new(new_res, param_name, json_object_get(param, "value"));

            assert(NULL != new_res);
            ulfius_set_json_body_response(response, 200, new_res); // value
            json_decref(res);
            res = new_res;
          } else {
#ifdef DEBUG_BULFIUS_GET
            printf("%s: %s Plugin %s has parameter %s\n", __FILE__, __func__, plugin, param_name);
#endif
            ulfius_set_string_body_response(response, 404, "Parameter not found");
          }
        } else {
          ulfius_set_json_body_response(response, 200, res); // all parameters
        }
        json_decref(res);
      } else {
#ifdef DEBUG_BULFIUS_GET
        printf("%s: %s Plugin %s has no parameters\n", __FILE__, __func__, plugin);
#endif
        ulfius_set_string_body_response(response, 204, NULL); // no parameters
      }
    } else {
#ifdef DEBUG_BULFIUS_GET
      printf("%s: %s Plugin %s not found\n", __FILE__, __func__, plugin);
#endif
      ulfius_set_string_body_response(response, 404, "Plugin not found");
    }
  } else {
#ifdef DEBUG_BULFIUS_GET
    printf("%s: %s Bad request, plugin %s\n", __FILE__, __func__, plugin);
#endif
    ulfius_set_string_body_response(response, 400, "Bad request");
  }
  ulfius_add_header_to_response(response, "Access-Control-Allow-Origin", "*");

  return U_CALLBACK_COMPLETE;
}

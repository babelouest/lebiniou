```
## Sample configuration file to copy as ~/.lebiniourc
## Remove the leading "#" to uncomment lines

[Plugins]
## Available input plugins: oss, alsa, jackaudio, pulseaudio, sndfile, twip, random, NULL
# Input=alsa

## Input buffer size (samples)
## Note: not applicable to the jackaudio or sndfile plugins
# InputSize=1024

## Available output plugins: SDL2, caca, RTMP, mp4, NULL
# Output=SDL2

## Plugins blacklist
# Blacklist=plugin1;plugin2;...

[Screen]
# Width=960
# Height=540
# FullScreen=false

[Engine]
## DataDir: path to a directory containing colormaps and images
# DataDir: /usr/share/lebiniou

## Themes: comma-separated list of themes to use (default: biniou)
## If a theme name begins with a "~", it will be loaded from your
## .lebiniou/images/ directory eg: "~mytheme" will load images from
## ~/.lebiniou/images/mytheme/
# Themes=biniou

## SequencesDir: path to a directory containing sequences
## If unset, ~/.lebiniou/sequences/ will be used
# SequencesDir=/path/to/sequences/
#
# Uncomment this line to use the sequences from lebiniou-data package
# WARNING: you can not create or update sequences in this directory !
# SequencesDir=/usr/share/lebiniou/sequences/json

## StartWithFirstSequence
## By default, start with the most recent (last) sequence saved
# StartWithFirstSequence=false

## RandomMode
## 0 = Off
## 1 = Sequences
## 2 = Schemes
## 3 = Sequences and schemes
# RandomMode=3

## Frames per second
# Fps=25

## Fade delay in seconds
# FadeDelay=3

## Auto-change timers values
## NOTE: it's advised to set *both* values for Min and Max if used
## Colormaps
# ColormapsMin=15
# ColormapsMax=30
## Images
# ImagesMin=15
# ImagesMax=30
## Sequences
# SequencesMin=15
# SequencesMax=30

## Auto-change modes
## Possible values are: shuffle,random,cycle
# ColormapsMode=random
# ImagesMode=random
# SequencesMode=shuffle

## HTTP API port
## Set to 0 to disable
# HttpPort=30543

## Hard/soft timers
# Set to false to prevent timers from using the system clock
# HardTimers=true

[Input]
## Set to true if you have a buggy DSP driver
# AntiPhase=false

## Volume scaling
# VolumeScale=1.0

[Webcam]
## Control horizontal/vertical flipping
# HorizontalFlip=true
# VerticalFlip=true

## Number of webcams
# Webcams=1

## Path to the webcam devices
# Device=/dev/video

## Auto-change webcams
# WebcamsMin=15
# WebcamsMax=30
# WebcamsMode=cycle
```
